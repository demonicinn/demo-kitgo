<x-app-layout>
<div class="content-header">
	<div class="container-fluid">
		<div class="row mb-2 pt-1">
			<div class="col-8">
				<ol class="breadcrumb">
					<li class="breadcrumb-item">
						<a href="{{ route('sub-items') }}">Items</a>
					</li>
					<li class="breadcrumb-item active">
						{{ @$item->make }} - {{ @$item->model }}
					</li>
				</ol>
			</div>
			@if(auth()->user()->role!='user')
			<div class="col-4">
				<ol class="breadcrumb float-right">
					<a class="btn btn-danger btn-sm mr-2" href="javascript:void(0)" data-toggle="modal" data-target="#deleteItem"><i class="fa fa-trash"></i> Delete</a>
					<a class="btn btn-warning btn-sm mr-2" href="{{ route('subitems.edit', $item->id) }}"><i class="fa fa-edit"></i> Edit</a>
				</ol>
			</div>
			@endif
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		
		<!-- Profile Image -->
		<div class="card card-default">
			<div class="card-body box-profile pb-0">
				<div class="row">
					<div class="col-3 col-md-2">
						@if(@$item->pictureurl)
						<img style="width: 164px;" class="profile-user-img img-fluid" src="{{ asset('storage/photos/'.$item->pictureurl) }}">
						@else
						<img style="width: 164px;" class="profile-user-img img-fluid" src="{{ asset('img/noimg.png') }}">
						@endif
					</div>
					<div class="col-9 col-md-10">
						<h3 class="profile-username m-0">{{ @$item->make }} - {{ @$item->model }}</h3>
						<p class="text-muted m-0">{{ @$item->items->name }}</p>
						<p class="badge badge-{{ subItemColor($item->status) }} p-1">{{ subItemStatus($item->status) }}</p>
					</div>
				</div>
			</div>
			
			<div class="card-header p-2">
				<hr/>
				<ul class="nav nav-pills mytabs">
					<li class="nav-item actives"><a class="nav-link dashboard" href="javascript:void(0)" data-tab="dashboard">Dashboard</a></li>
					<li class="nav-item"><a class="nav-link info" href="javascript:void(0)" data-tab="info">Info</a></li>
					{{--<li class="nav-item"><a class="nav-link barcodes" href="javascript:void(0)" data-tab="barcodes">Barcodes</a></li>--}}
					<li class="nav-item"><a class="nav-link reservations" href="javascript:void(0)" data-tab="reservations">Reservations</a></li>
					<li class="nav-item"><a class="nav-link checkouts" href="javascript:void(0)" data-tab="checkouts">Check-outs</a></li>
					<li class="nav-item"><a class="nav-link maps" href="javascript:void(0)" data-tab="maps">Maps</a></li>
					<li class="nav-item"><a class="nav-link comments" href="javascript:void(0)" data-tab="comments">Comments</a></li>
					<li class="nav-item"><a class="nav-link attachments" href="javascript:void(0)" data-tab="attachments">Attachments</a></li>
				</ul>
			</div>
		</div>
		
		
		<div class="cutabs dashboard pl-4 pr-4">
			<div class="row">
				<div class="col-md-8">
					<div class="p-2">BOOKINGS</div>
					<div class="card card-default">
						<div id='calendar'></div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="p-2">History</div>
					<div class="timeline">
						@foreach($item->subitemHistory as $history)
						<div>
							@if(@$history->user->profile_photo_path)
							<img width="30" class="fas img-circle" src="{{ asset('storage/profile/'.$history->user->profile_photo_path) }}" alt="{{ $history->user->name }}">
							@else
							<span class="fas bg-blue">{{ userName($history->user->name) }}</span>
							@endif

							<div class="timeline-item">
								<span class="time"><i class="fas fa-clock"></i> {{ dateTimeFormat($history->created_at) }}</span>
								<h3 class="timeline-header">
									<a href="{{ route('users.show', $history->user->id) }}">{{ $history->user->name }}</a>
								</h3>
								<div class="timeline-body">
								@if($history->type=='1')
									<a href="{{ route('subitems.show', $item->id) }}?tab=comments">
										{!! @$history->notificationtext !!}
									</a>
									<p>{!! getItemsComment($history->linkto) !!}</p>
								@else
									{!! @$history->notificationtext !!}
								@endif
								</div>
							</div>
						</div>
						@endforeach
					</div>
				</div>
			</div>
		</div>
		
		
		
		<div class="cutabs info pl-4 pr-4" style="display:none">				
			<div class="row">
				<div class="col-md-8">
					<div class="p-2">INFORMATION</div>
					<div class="card card-default">
						<div class="table-responsive">
							<table class="table">
								<tbody>
									<tr>
										<th style="width:50%">Name</th>
										<td>{{ @$item->make }} - {{ @$item->model }}</td>
									</tr>
									<tr>
										<th style="width:50%">Serial Number</th>
										<td>{{ @$item->serialnumber }}</td>
									</tr>
									<tr>
										<th>Date of Purchase</th>
										<td>{{ dateFormat($item->date_of_purchase) }}</td>
									</tr>
									
									<tr>
										<th>Pat Date</th>
										<td>{{ dateFormat($item->pat_date) }}</td>
									</tr>
									<tr>
										<th>Pat Due Date</th>
										<td>{{ dateFormat($item->pat_due_date) }}</td>
									</tr>
									@if(@$item->cable_length)
									<tr>
										<th>Cable Length</th>
										<td>{{ $item->cable_length }}</td>
									</tr>
									@endif
									
									
									@if($item->warranty_expiry_period)
									<tr>
										<th>Warranty Expiry Period</th>
										<td>{{ dateFormat($item->warranty_expiry_period) }}</td>
									</tr>
									@endif
									<tr>
										<th>Condition</th>
										<td>{{ $item->condition=='needrepair' ? 'Need Repair' : ucfirst($item->condition) }}</td>
									</tr>
									{{--
									<tr>
										<th>Quantity</th>
										<td>{{ $item->quantity }}</td>
									</tr>
									<tr>
										<th>Available Quantity</th>
										<td>{{ $item->available_qty }}</td>
									</tr>
									--}}
									<tr>
										<th>Note</th>
										<td>{{ $item->notes }}</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				
				<div class="col-md-4">
					<div class="p-2">CODES</div>
					@if($item->barcode_url)
					<img width="150" src="{{ asset('assets/barcodes/'.$item->barcode_url) }}">
					@endif
					<p>{{ @$item->barcode_no }}</p>
				</div>
				
			</div>
		</div>
		
		{{--
		<div class="cutabs barcodes pl-4 pr-4" style="display:none">				
			<div class="row">
				<div class="col-md-12">
					<div class="p-2">QR CODES</div>
					
					<div class="row">
						@foreach($item->subitemBarcode as $barcode)
						<div class="col-md-3">
							{{ $barcode->barcode_no ? QrCode::generate($barcode->barcode_no) : '' }}
							<p>{{ @$barcode->barcode_no }}</p>
						</div>
						@endforeach
					</div>					
				</div>
			</div>
		</div>
		--}}
		
		<div class="cutabs reservations pl-4 pr-4" style="display:none">
			<div class="p-2">Reservations</div>
			<div class="card card-default">
				<div class="table-responsive">
					<table class="table table-hover text-nowrap">
						<thead>
							<tr>
								<th>Name</th>
								<th>From</th>
								<th>To</th>
								<th>Duration</th>
								<th>Contact</th>
							</tr>
						</thead>
						<tbody>
							@foreach($item->projectitem as $res)
							@if(@$res->project)
							<tr>
								<td><a href="{{ route('projects.show', $res->project->id) }}">{{ $res->project->title }}</a></td>
								<td>{{ dateFormat($res->project->pickup_date) }}</td>
								<td>{{ dateFormat($res->project->expected_return_date) }}</td>
								<td>{{ \Carbon\Carbon::parse( $res->project->pickup_date )->diffInDays( $res->project->expected_return_date ) }} days</td>
								<td>
									@if(@$res->user_id)
									<a href="{{ route('users.show', $res->user->id) }}">
										@if(@$res->user->profile_photo_path)
										<img width="38" class="img-circle elevation-2 mr-2" src="{{ asset('storage/profile/'.$res->user->profile_photo_path) }}" alt="{{ $res->user->name }}">
										@else
										<span class="fas bg-blue img-circle p-2 mb-0 mr-2">{{ userName($res->user->name) }}</span>
										@endif
									
										{{ ucfirst(@$res->user->name) }}
									</a>
									@endif
								</td>
							</tr>
							@endif
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
		
		<div class="cutabs checkouts pl-4 pr-4" style="display:none">
			<div class="p-2">Check-outs</div>
			<div class="card card-default">
				<div class="table-responsive">
					<table class="table table-hover text-nowrap">
						<thead>
							<tr>
								<th>Name</th>
								<th>From</th>
								<th>To</th>
								<th>Duration</th>
								<th>Contact</th>
							</tr>
						</thead>
						<tbody>
							@foreach($item->projectitemCheckout as $res)
							@if(@$res->project)
							<tr>
								<td><a href="{{ route('projects.show', $res->project->id) }}">{{ $res->project->title }}</a></td>
								<td>{{ dateFormat($res->project->pickup_date) }}</td>
								<td>{{ dateFormat($res->project->expected_return_date) }}</td>
								<td>{{ \Carbon\Carbon::parse( $res->project->pickup_date )->diffInDays( $res->project->expected_return_date ) }} days</td>
								<td>
									@if(@$res->project->user->name)
									<a href="{{ route('users.show', $res->project->user->id) }}">
									<span class="fas bg-blue">{{ userName($res->project->user->name) }}</span>
									{{ ucfirst(@$res->project->user->name) }}
									</a>
									@endif
									
									@if(@$res->user_id)
									<a href="{{ route('users.show', $res->user->id) }}">
										@if(@$res->user->profile_photo_path)
										<img width="38" class="img-circle elevation-2 mr-2" src="{{ asset('storage/profile/'.$res->user->profile_photo_path) }}" alt="{{ $res->user->name }}">
										@else
										<span class="fas bg-blue img-circle p-2 mb-0 mr-2">{{ userName($res->user->name) }}</span>
										@endif
									
										{{ ucfirst(@$res->user->name) }}
									</a>
									@endif
								</td>
							</tr>
							@endif
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
		
		<div class="cutabs maps pl-4 pr-4" style="display:none">
			<div class="p-2">Maps</div>
			<div class="card card-default">
				<div id="map"></div>
			</div>
		</div>
		
		<div class="cutabs comments pl-4 pr-4" style="display:none">
			@livewire('subitem-comment-action')
		</div>
		
		
		<div class="cutabs attachments pl-4 pr-4" style="display:none">
			<div class="row">
				<div class="col-md-8">
					<div class="p-2">Attachments</div>
					<div class="">
						@if(@$item->pictureurl)
						<img style="width: 164px;" class="profile-user-img img-fluid" src="{{ asset('storage/photos/'.$item->pictureurl) }}">
						@endif
						@if(@$item->receipt_url)
						<img style="width: 164px;" class="profile-user-img img-fluid" src="{{ asset('storage/photos/'.$item->receipt_url) }}">
						@endif
					</div>
					</div>
				<div class="col-md-4">
				{{--
					<div class="p-2">Add New Attachment</div>
				--}}	
				</div>
			</div>
		</div>
		
	</div>
	
	
	<div id="deleteItem" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">Delete</h4>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
					<p>Are you sure to Delete Item?</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<a href="{{ route('subitems.destroy', $item->id) }}" class="btn btn-danger" onclick="event.preventDefault(); document.getElementById('delete-form').submit();">Yes, Delete</a>
					<form id="delete-form" action="{{ route('subitems.destroy', $item->id) }}" method="POST" style="display: none;">
						@csrf
					</form>
				</div>
			</div>
		</div>
	</div>
	
</div>




@push('scripts')

<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key={{ config('services.google.key') }}&libraries=places&sensor=false"></script>
<script>
	
	//document.addEventListener('DOMContentLoaded', function() {
		var calendarEl = document.getElementById('calendar');
		
		var calendar = new FullCalendar.Calendar(calendarEl, {
			headerToolbar: {
				left: 'prev,next today',
				center: 'title',
				right: 'dayGridMonth,timeGridWeek,timeGridDay,listWeek'
			},
			initialView: 'dayGridMonth',
			events: [
				@foreach($item->projectitem as $res)
				{
					title: "{!!$res->project->title!!} - {{@$res->user->name}}",
					start: '{{$res->project->pickup_date}}',
					end: '{{$res->project->expected_return_date}}'
				},
				@endforeach
				@foreach($item->projectbarcode as $res)
				{
					title: 'Checkout',
					start: '{{$res->project->pickup_date}}',
					end: '{{$res->project->expected_return_date}}'
				},
				@endforeach
			]
		});
		
		calendar.render();
	//});
	
	function mymap() {
		const myLatLng = { lat: {{$item->latitude ?? '51.509865'}}, lng: {{$item->longitude ?? '-0.118092'}} };
		const map = new google.maps.Map(document.getElementById("map"), {
			zoom: 16,
			center: myLatLng,
		});
		new google.maps.Marker({
			position: myLatLng,
			map,
			title: "Hello World!",
		});
	}
	mymap();
	
	
	@if(@request()->tab)
		$('.mytabs a.nav-link').parent('.nav-item').removeClass('actives');
		$('.{{request()->tab}}').parent('.nav-item').addClass('actives');
		
		let tab = $('.{{request()->tab}}').attr('data-tab');
		$('.cutabs').hide();
		$('.cutabs.'+tab).show();
	@endif
	
	$('.mytabs a.nav-link').on('click', function(){
		$('.mytabs a.nav-link').parent('.nav-item').removeClass('actives');
		$(this).parent('.nav-item').addClass('actives');
		
		let tab = $(this).attr('data-tab');
		$('.cutabs').hide();
		$('.cutabs.'+tab).show();
		
	});
</script>
@endpush
</x-app-layout>