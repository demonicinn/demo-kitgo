<x-app-layout>
<div class="content-header">
	<div class="container-fluid">
		<div class="row mb-2 pt-1">
			<div class="col-6">
				<ol class="breadcrumb">
					<li class="breadcrumb-item">
						<a href="{{ route('sub-items') }}">Items</a>
					</li>
				</ol>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-8 offset-md-2">
		<div class="card card-default">
			<div class="card-header">
				<h3 class="card-title">Item Quick Checkout</h3>
				<div class="card-tools pull-right">
					<a href="{{ route('sub-items') }}" title="Back" class="btn btn-warning btn-icon"><i class="fa fa-arrow-left"></i></a>
				</div>
			</div>
			
			{!! Form::open(['route' => ['subitems.quickCheckout', $item->id], 'class' => 'form-horizontal']) !!}
				<div class="card-body">					
					
					<div class="form-group {!! ($errors->has('title') ? 'has-error' : '') !!}">
						{!! Form::label('title','Title', ['class' => 'control-label']) !!}
						{!! Form::text('title', auth()->user()->name.' - Quick Checkout', ['class' => 'form-control' . ($errors->has('title') ? ' is-invalid' : '') ]) !!}
						{!! $errors->first('title', '<span class="help-block">:message</span>') !!}
					</div>

					<div class="form-group {!! ($errors->has('pickup_date') ? 'has-error' : '') !!}">
						{!! Form::label('pickup_date','Pickup Date', ['class' => 'control-label']) !!}
						{!! Form::date('pickup_date', null, ['class' => 'form-control' . ($errors->has('pickup_date') ? ' is-invalid' : ''), 'min'=>date('Y-m-d') ]) !!}
						{!! $errors->first('pickup_date', '<span class="help-block">:message</span>') !!}
					</div>

					<div class="form-group {!! ($errors->has('expected_return_date') ? 'has-error' : '') !!}">
						{!! Form::label('expected_return_date','Return Date', ['class' => 'control-label']) !!}
						{!! Form::date('expected_return_date', null, ['class' => 'form-control' . ($errors->has('expected_return_date') ? ' is-invalid' : ''), 'min'=>date('Y-m-d') ]) !!}
						{!! $errors->first('expected_return_date', '<span class="help-block">:message</span>') !!}
					</div>

					<div class="form-group {!! ($errors->has('user') ? 'has-error' : '') !!}">
						{!! Form::label('user','Contact Person', ['class' => 'control-label']) !!}
						<select class="form-control" name="user">
							@foreach(App\Models\Project::getUsers() as $user)
								<option value="{{ $user->id }}">{{ $user->name }} - {{ $user->email }}</option>
							@endforeach
						</select>	
						{!! $errors->first('user', '<span class="help-block">:message</span>') !!}
					</div>


				</div>
				<div class="card-footer">
					<a href="{{ route('sub-items') }}" class="btn btn-default">Cancel</a>
					<button type="submit" class="btn btn-info float-right">Checkout</button>
				</div>
			{!! Form::close() !!}
			
			
		</div>
	</div>
</div>
</x-app-layout>