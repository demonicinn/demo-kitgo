<div class="form-group {!! ($errors->has('item_id') ? 'has-error' : '') !!}">
    {!! Form::label('item_id','Select Sub Category', ['class' => 'control-label']) !!}
	{!! Form::select('item_id', $getlinkeditems, null, ['class' => 'form-control engineerSearch' . ($errors->has('item_id') ? ' is-invalid' : '') ]) !!}	
    {!! $errors->first('item_id', '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {!! ($errors->has('pat_date') ? 'has-error' : '') !!}">
    {!! Form::label('pat_date','Pat Date', ['class' => 'control-label']) !!}
    {!! Form::date('pat_date', null, ['class' => 'form-control' . ($errors->has('pat_date') ? ' is-invalid' : '') ]) !!}
    {!! $errors->first('pat_date', '<span class="help-block">:message</span>') !!}
</div>
<div class="form-group {!! ($errors->has('pat_due_date') ? 'has-error' : '') !!}">
    {!! Form::label('pat_due_date','Pat Due Date', ['class' => 'control-label']) !!}
    {!! Form::date('pat_due_date', null, ['class' => 'form-control' . ($errors->has('pat_due_date') ? ' is-invalid' : '') ]) !!}
    {!! $errors->first('pat_due_date', '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {!! ($errors->has('make') ? 'has-error' : '') !!}">
    {!! Form::label('make','Make', ['class' => 'control-label']) !!}
    {!! Form::text('make', null, ['class' => 'form-control' . ($errors->has('make') ? ' is-invalid' : '') ]) !!}
    {!! $errors->first('make', '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {!! ($errors->has('model') ? 'has-error' : '') !!}">
    {!! Form::label('model','Model', ['class' => 'control-label']) !!}
    {!! Form::text('model', null, ['class' => 'form-control' . ($errors->has('model') ? ' is-invalid' : '') ]) !!}
    {!! $errors->first('model', '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {!! ($errors->has('cable_length') ? 'has-error' : '') !!}">
    {!! Form::label('cable_length','Cable Length', ['class' => 'control-label']) !!}
    {!! Form::text('cable_length', null, ['class' => 'form-control' . ($errors->has('cable_length') ? ' is-invalid' : '') ]) !!}
    {!! $errors->first('cable_length', '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {!! ($errors->has('serialnumber') ? 'has-error' : '') !!}">
    {!! Form::label('serialnumber','Serial no', ['class' => 'control-label']) !!}
    {!! Form::text('serialnumber', null, ['class' => 'form-control' . ($errors->has('serialnumber') ? ' is-invalid' : '') ]) !!}
    {!! $errors->first('serialnumber', '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {!! ($errors->has('pictureurl') ? 'has-error' : '') !!}">
    {!! Form::label('pictureurl','Picture', ['class' => 'control-label']) !!}</br>
    {!! Form::file('pictureurl', null, ['class' => 'form-control' . ($errors->has('pictureurl') ? ' is-invalid' : '') ]) !!}
	@if(@$item->pictureurl)
	</br>
	<a target="_blank" href="{{ asset('storage/photos/'.$item->pictureurl) }}">
		<img width="200" src="{{ asset('storage/photos/'.$item->pictureurl) }}" />
	</a>
	@endif
    {!! $errors->first('pictureurl', '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {!! ($errors->has('date_of_purchase') ? 'has-error' : '') !!}">
    {!! Form::label('date_of_purchase','Date of purchase', ['class' => 'control-label']) !!}
    {!! Form::date('date_of_purchase', null, ['class' => 'form-control' . ($errors->has('date_of_purchase') ? ' is-invalid' : ''), 'max'=>date('Y-m-d') ]) !!}
    {!! $errors->first('date_of_purchase', '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {!! ($errors->has('warranty_expiry_period') ? 'has-error' : '') !!}">
    {!! Form::label('warranty_expiry_period','Warranty Expiry Period', ['class' => 'control-label']) !!}
    {!! Form::date('warranty_expiry_period', null, ['class' => 'form-control' . ($errors->has('warranty_expiry_period') ? ' is-invalid' : '') ]) !!}
    {!! $errors->first('warranty_expiry_period', '<span class="help-block">:message</span>') !!}
</div>

@if(Route::currentRouteName()=='subitems.create')
<div class="form-group {!! ($errors->has('quantity') ? 'has-error' : '') !!}">
    {!! Form::label('quantity','Quantity', ['class' => 'control-label']) !!}
    {!! Form::number('quantity', '1', ['class' => 'form-control' . ($errors->has('quantity') ? ' is-invalid' : ''), 'min'=>'1' ]) !!}
    {!! $errors->first('quantity', '<span class="help-block">:message</span>') !!}
</div>
@endif

@if(Route::currentRouteName()=='subitems.edit')
<div class="form-group {!! ($errors->has('barcode_no') ? 'has-error' : '') !!}">
    {!! Form::label('barcode_no','Barcode', ['class' => 'control-label']) !!}
    {!! Form::text('barcode_no', null, ['class' => 'form-control' . ($errors->has('barcode_no') ? ' is-invalid' : '')]) !!}
    {!! $errors->first('barcode_no', '<span class="help-block">:message</span>') !!}
</div>
@endif

<div class="form-group {!! ($errors->has('receipt_url') ? 'has-error' : '') !!}">
    {!! Form::label('receipt_url','Receipt', ['class' => 'control-label']) !!}</br>
    {!! Form::file('receipt_url', null, ['class' => 'form-control' . ($errors->has('receipt_url') ? ' is-invalid' : '') ]) !!}
	@if(@$item->receipt_url)
	</br>
	<a target="_blank" href="{{ asset('storage/photos/'.$item->receipt_url) }}">
		<img width="200" src="{{ asset('storage/photos/'.$item->receipt_url) }}" />
	</a>
	@endif
    {!! $errors->first('receipt_url', '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {!! ($errors->has('condition') ? 'has-error' : '') !!}">
    {!! Form::label('condition','Select Condition', ['class' => 'control-label']) !!}
	{!! Form::select('condition', $getConditions, null, ['class' => 'form-control' . ($errors->has('condition') ? ' is-invalid' : '') ]) !!}
    {!! $errors->first('condition', '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {!! ($errors->has('notes') ? 'has-error' : '') !!}">
    {!! Form::label('notes','Note', ['class' => 'control-label']) !!}
    {!! Form::text('notes', null, ['class' => 'form-control' . ($errors->has('notes') ? ' is-invalid' : '') ]) !!}
    {!! $errors->first('notes', '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {!! ($errors->has('disposed') ? 'has-error' : '') !!}">
	<input name="disposed" type="hidden" value="0">
	<input name="disposed" type="checkbox" value="1" {{ @($item->disposed=='1') ? 'checked' : '' }}>
	{!! Form::label('disposed','Disposed', ['class' => 'control-label']) !!}
	</br>
    {!! $errors->first('disposed', '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {!! ($errors->has('is_missed') ? 'has-error' : '') !!}">
	<input name="is_missed" type="hidden" value="0">
	<input name="is_missed" type="checkbox" value="1" {{ @($item->is_missed=='1') ? 'checked' : '' }}>
	{!! Form::label('is_missed','Missing', ['class' => 'control-label']) !!}
	</br>
    {!! $errors->first('is_missed', '<span class="help-block">:message</span>') !!}
</div>


@push('scripts')
<style>
	.select2-container .select2-selection--single {
		height: 38px;
	}
</style>
<script>
	$(function () {
		$('.engineerSearch').select2({width: '100%', placeholder: 'Select Sub Category'});
	});
	$('#make').attr('required', 'required');
	$('#model').attr('required', 'required');
	
	
	@if(@$item)
		let check = $('#item_id').find("option:selected").text();
		if(check=='Cable'){
			$('.cas').hide();
			$('.cab').show();
			$('#cable_length').attr('required', 'required');
			$('#cable_length').val('{{@$item->model}}');
			
			$('#make').removeAttr('required');
			$('#model').removeAttr('required');
		}
	@endif
	
	
	$('#item_id').on('change', function(){
		let val = $(this).find("option:selected").text();
		$('.cas').show();
		$('.cab').hide();
		
		$('#cable_length').removeAttr('required');
		
		$('#make').attr('required', 'required');
		$('#model').attr('required', 'required');
		//...
		if(val=='Cable'){
			$('.cas').hide();
			$('.cab').show();
			$('#cable_length').attr('required', 'required');
			
			$('#make').removeAttr('required');
			$('#model').removeAttr('required');
		}
	});
	
	
</script>
@endpush