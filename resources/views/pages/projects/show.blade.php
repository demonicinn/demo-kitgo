<x-app-layout>
<div class="content-header">
	<div class="container-fluid">
		<div class="row mb-2 pt-1">
			<div class="col-6">
				<ol class="breadcrumb">
					<li class="breadcrumb-item">
						<a href="{{ route('projects') }}">Projects</a>
					</li>
					<li class="breadcrumb-item active">
					{{ @$project->title }}
					</li>
				</ol>
			</div>
			@if(auth()->user()->role!='user')
			<div class="col-6">
				<ol class="breadcrumb float-right">
					<a class="btn btn-danger btn-sm mr-2" href="javascript:void(0)" data-toggle="modal" data-target="#deleteProject"><i class="fa fa-trash"></i> Delete</a>
					<a class="btn btn-warning btn-sm mr-2" href="{{ route('projects.edit', $project->id) }}"><i class="fa fa-edit"></i> Edit</a>
				</ol>
			</div>
			@endif
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		
		<!-- Profile Image -->
		<div class="card card-default">
			<div class="card-body box-profile pb-0">
				<div class="row">
					<div class="col-md-12">
						<h3 class="profile-username">{{ @$project->title }}							
							@if($project->status=='2')
							<p class="badge badge-warning mb-0">Awaiting Approval</p>
							@endif
							@if($project->status=='0')
							<p class="badge badge-danger">Not active</p>
							@endif
						</h3>
					</div>
				</div>
			</div>
			
			<div class="card-header p-2">
				<hr/>
				<ul class="nav nav-pills mytabs">
					<li class="nav-item actives"><a class="nav-link info" href="javascript:void(0)" data-tab="info">Info</a></li>
					<li class="nav-item"><a class="nav-link comments" href="javascript:void(0)" data-tab="comments">Comments</a></li>
					<li class="nav-item"><a class="nav-link attachments" href="javascript:void(0)" data-tab="attachments">Attachments</a></li>
				</ul>
			</div>
		</div>
		
		
		<div class="cutabs info pl-4 pr-4">				
			<div class="row">
				<div class="col-md-8">
					<div class="row">
						<div class="col-md-6">
							<div class="p-2">WHEN</div>
							@if(@$project->pickup_date)
							<div class="info-box mb-2">
								Pickup Date
								<strong class="ml-4">{{ dateFormat($project->pickup_date) }}</strong>
							</div>
							@endif
							@if(@$project->shipping_date)
							<div class="info-box mb-2">
								Shipping Date
								<strong class="ml-4">{{ dateFormat($project->shipping_date) }}</strong>
							</div>
							@endif
							@if(@$project->start_date)
							<div class="info-box mb-2">
								Start Date
								<strong class="ml-4">{{ dateFormat($project->start_date) }}</strong>
							</div>
							@endif
							@if(@$project->end_date)
							<div class="info-box mb-2">
								End Date
								<strong class="ml-4">{{ dateFormat($project->end_date) }}</strong>
							</div>
							@endif
							@if(@$project->expected_return_date)
							<div class="info-box mb-2">
								Return Date
								<strong class="ml-4">{{ dateFormat($project->expected_return_date) }}</strong>
							</div>
							@endif
							
							<p class="text-center">Duration: {{ \Carbon\Carbon::parse( $project->pickup_date )->diffInDays( $project->expected_return_date ) }} days</p>
						</div>
						<div class="col-md-6">
							<div class="p-2">CONTACT</div>
							@if(@$project->projectUser)
							<a class="info-box mb-2" href="{{ route('users.show', $project->projectUser->id) }}">
								
								@if(@$project->projectUser->profile_photo_path)
								<img width="106" src="{{ asset('storage/profile/'.$project->projectUser->profile_photo_path) }}" alt="{{ $project->projectUser->name }}">
								@else
								<h1 class="fas bg-blue p-4 mb-0">{{ userName($project->projectUser->name) }}</h1>
								@endif
								
								<div class="ml-2">
									<p class="mb-0">{{ @$project->projectUser->name }}</p>
									<p class="mb-0">{{ @$project->projectUser->email }}</p>
								</div>
							</a>
							@endif
							
							@if(@$project->client_name)
							<div class="p-2">CLIENT DETAILS</div>
							<div class="info-box mb-2">
								<h1 class="fas bg-blue p-4 mb-0">{{ userName($project->client_name) }}</h1>
								<div class="ml-2">
									<p class="mb-0">Name: {{ @$project->client_name }}</p>
									<p class="mb-0">Number: {{ @$project->client_contact_number }}</p>
									<p class="mb-0">Address: {{ @$project->location }}</p>
								</div>
							</div>
							@endif
						</div>
					</div>
					
					@livewire('project-checkout')
					
				</div>
				<div class="col-md-4">
					<div class="p-2">History</div>
					<div class="timeline">
						@foreach($project->projectHistory as $history)
						<div>
							@if(@$history->user)
								@if(@$history->user->profile_photo_path)
								<img width="30" class="fas img-circle" src="{{ asset('storage/profile/'.$history->user->profile_photo_path) }}" alt="{{ $history->user->name }}">
								@else
								<span class="fas bg-blue">{{ userName($history->user->name) }}</span>
								@endif
							@endif
							<div class="timeline-item">
								<span class="time"><i class="fas fa-clock"></i> {{ dateTimeFormat($history->created_at) }}</span>
								<h3 class="timeline-header">
									<a href="{{ route('users.show', $history->user->id) }}">{{ @$history->user->name }}</a>
								</h3>
								<div class="timeline-body">
								@if($history->type=='1')									
									<a href="{{ route('projects.show', $project->id) }}?tab=comments">
										{!! @$history->notificationtext !!}
									</a>
									<p>{!! getProjectComment($history->linkto) !!}</p>
								@elseif($history->type=='2')
								<a href="{{ route('projects.show', $project->id) }}?tab=attachments">
									{!! @$history->notificationtext !!}
								</a>
								@else
									{!! @$history->notificationtext !!}
								@endif
								</div>
							</div>
						</div>
						@endforeach
					</div>
					</div>
			</div>
		</div>
		
		<div class="cutabs comments pl-4 pr-4" style="display:none">
			@livewire('projects-comment-action')
		</div>
		
		<div class="cutabs attachments pl-4 pr-4" style="display:none">
			@livewire('projects-attachments-action')
		</div>
		
	</div>
	
	
	<div id="deleteProject" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">Delete</h4>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
					<p>Are you sure to Delete Project?</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<a href="{{ route('projects.destroy', $project->id) }}" class="btn btn-danger" onclick="event.preventDefault(); document.getElementById('delete-form').submit();">Yes, Delete</a>
					<form id="delete-form" action="{{ route('projects.destroy', $project->id) }}" method="POST" style="display: none;">
						@csrf
					</form>
				</div>
			</div>
		</div>
	</div>
</div>




@push('scripts')
<script>
	@if(@request()->tab)
		$('.mytabs a.nav-link').parent('.nav-item').removeClass('actives');
		$('.{{request()->tab}}').parent('.nav-item').addClass('actives');
		
		let tab = $('.{{request()->tab}}').attr('data-tab');
		$('.cutabs').hide();
		$('.cutabs.'+tab).show();
	@endif
	
	$('.mytabs a.nav-link').on('click', function(){
		$('.mytabs a.nav-link').parent('.nav-item').removeClass('actives');
		$(this).parent('.nav-item').addClass('actives');
		
		let tab = $(this).attr('data-tab');
		$('.cutabs').hide();
		$('.cutabs.'+tab).show();
		
	});
</script>
@endpush
</x-app-layout>