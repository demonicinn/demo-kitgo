<div class="form-group {!! ($errors->has('title') ? 'has-error' : '') !!}">
    {!! Form::label('title','Title', ['class' => 'control-label']) !!}
    {!! Form::text('title', null, ['class' => 'form-control' . ($errors->has('title') ? ' is-invalid' : ''), 'required'=>'required' ]) !!}
    {!! $errors->first('title', '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {!! ($errors->has('client_name') ? 'has-error' : '') !!}">
    {!! Form::label('client_name','Client Name', ['class' => 'control-label']) !!}
    {!! Form::text('client_name', null, ['class' => 'form-control' . ($errors->has('client_name') ? ' is-invalid' : ''), 'required'=>'required' ]) !!}
    {!! $errors->first('client_name', '<span class="help-block">:message</span>') !!}
</div>
<div class="form-group {!! ($errors->has('client_contact_number') ? 'has-error' : '') !!}">
    {!! Form::label('client_contact_number','Client Contact Number', ['class' => 'control-label']) !!}
    {!! Form::text('client_contact_number', null, ['class' => 'form-control' . ($errors->has('client_contact_number') ? ' is-invalid' : ''), 'required'=>'required' ]) !!}
    {!! $errors->first('client_contact_number', '<span class="help-block">:message</span>') !!}
</div>
<div class="form-group {!! ($errors->has('location') ? 'has-error' : '') !!}">
    {!! Form::label('location','Client Location', ['class' => 'control-label']) !!}
    {!! Form::text('location', null, ['class' => 'form-control' . ($errors->has('location') ? ' is-invalid' : ''), 'id'=>'autocomplete', 'required'=>'required' ]) !!}
    {!! $errors->first('location', '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {!! ($errors->has('pickup_date') ? 'has-error' : '') !!}">
    {!! Form::label('pickup_date','Pickup Date', ['class' => 'control-label']) !!}
    {!! Form::date('pickup_date', null, ['class' => 'form-control' . ($errors->has('pickup_date') ? ' is-invalid' : ''), 'required'=>'required', 'min'=>date('Y-m-d') ]) !!}
    {!! $errors->first('pickup_date', '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {!! ($errors->has('shipping_date') ? 'has-error' : '') !!}">
    {!! Form::label('shipping_date','Shipping Date', ['class' => 'control-label']) !!}
    {!! Form::date('shipping_date', null, ['class' => 'form-control' . ($errors->has('shipping_date') ? ' is-invalid' : ''), 'required'=>'required', 'min'=>date('Y-m-d') ]) !!}
    {!! $errors->first('shipping_date', '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {!! ($errors->has('start_date') ? 'has-error' : '') !!}">
    {!! Form::label('start_date','Start Date', ['class' => 'control-label']) !!}
    {!! Form::date('start_date', null, ['class' => 'form-control' . ($errors->has('start_date') ? ' is-invalid' : ''), 'required'=>'required', 'min'=>date('Y-m-d') ]) !!}
    {!! $errors->first('start_date', '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {!! ($errors->has('end_date') ? 'has-error' : '') !!}">
    {!! Form::label('end_date','End Date', ['class' => 'control-label']) !!}
    {!! Form::date('end_date', null, ['class' => 'form-control' . ($errors->has('end_date') ? ' is-invalid' : ''), 'required'=>'required', 'min'=>date('Y-m-d') ]) !!}
    {!! $errors->first('end_date', '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {!! ($errors->has('expected_return_date') ? 'has-error' : '') !!}">
    {!! Form::label('expected_return_date','Return Date', ['class' => 'control-label']) !!}
    {!! Form::date('expected_return_date', null, ['class' => 'form-control' . ($errors->has('expected_return_date') ? ' is-invalid' : ''), 'required'=>'required', 'min'=>date('Y-m-d') ]) !!}
    {!! $errors->first('expected_return_date', '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {!! ($errors->has('user') ? 'has-error' : '') !!}">
	{!! Form::label('user','Contact Person', ['class' => 'control-label']) !!}
	<select class="form-control" name="user" required>
		@foreach(App\Models\Project::getUsers() as $user)
			<option value="{{ $user->id }}" {{@$project->user==$user->id ? 'selected' : ''}}>{{ $user->name }} - {{ $user->email }}</option>
		@endforeach
	</select>	
	{!! $errors->first('user', '<span class="help-block">:message</span>') !!}
</div>



<table class="table" id="products_table">
	<thead>
		<tr>
			<th width="50%">Sub Category</th>
			<th width="25%">Quantity</th>
			<th width="25%">Available Quantity</th>
			<th></th>
		</tr>
	</thead>
	
	<tbody class="item_list">
		@if(@$project->projectitems)
		@foreach ($project->projectitems as $item)
		<tr class="item_{{ $item->id }}">
			<td>
			{{ @$item->subitem->make }} - {{ @$item->subitem->model }}
			</td>
			<td>
			{{ @$item->quantity }}
			</td>
			<td>
				{{ availableItems($item->subitem->item_id) }}
			</td>
			<td>
				@if($item->status=='reserved')
				<a href="javascript:void(0)" class="removeItem" data-id="{{ $item->id }}"><i class="fa fa-trash"></i></a>
				@endif
			</td>
		</tr>
		@endforeach
		@endif
	</tbody>
	
</table>

<div class="col-md-12">
	<button type="button" class="btn btn-info" onClick="addItem()">
		{{ __('+  Add Another Sub Category')}}
	</button>
</div>
	
	
	
@push('scripts')
<style>
	.select2-container .select2-selection--single {
		height: 38px;
	}
</style>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key={{ config('services.google.key') }}&libraries=places&sensor=false"></script>
<script>
	
	function initialize() {
		var input = document.getElementById('autocomplete');
		var options = {
			componentRestrictions: {country: 'uk'}//UK only
		};
		
		var autocomplete = new google.maps.places.Autocomplete(input, options);
		// autocomplete.addListener('place_changed', function() {
			// var place = autocomplete.getPlace();
			// var address = place.formatted_address;
			// $('.location').val(address);
		// });
	}
	initialize();

	let availableSubitems = [];
	
	function addItem(){
		var numItems = $('.item_list .item').length;
		numItems = numItems + 1;
		var postUrl = "{{ route('projects.subitems') }}";
		var expected_return_date = $('input[name=expected_return_date]').val();
		var formData = new FormData();
		formData.append('expected_return_date', expected_return_date);
		
		ajaxRequest(formData, postUrl, function(res) {
			availableSubitems = res;
			let html = '';
			html += '\
				<tr class="item">\
					<td>\
						<div class="form-group">\
							<select class="form-control engineerSearch" name="stockProducts['+numItems+'][name]">\
								<option value="">Select Sub Category</option>';
								$.each( res, function( key, value ) {
									html += '<option value="'+value.id+'" data-qty="'+value.totalQty+'">'+value.name+'</option>';
								});
							
							html += '</select>\
						</div>\
					</td>\
					<td>\
						<div class="form-group">\
							<input type="number" name="stockProducts['+numItems+'][qty]" class="form-control" min="1" value="1">\
						</div>\
					</td>\
					<td>\
						<div class="qty"></div>\
					</td>\
					<td>\
						<a href="javascript:void" class="removeProduct"><i class="fa fa-trash"></i></a>\
					</td>\
				</tr>\
			';			
			
			$('.item_list').append(html);
			$(".engineerSearch").select2({width: '100%'});		
		});
	}
	
	
	$(document).on('change', '.engineerSearch', function () {
		let item = $(this).val();		
		
		var postUrl = "{{ route('projects.subitemsLinked') }}";
		
		var formData = new FormData();
		formData.append('item', item);
		
		ajaxRequest(formData, postUrl, function(res) {
			$.each(res, function( k, ree ) {
				var numItems = $('.item_list .item').length;
				numItems = numItems + 1;
				let html = '';
				
				html += '\
				<tr class="item">\
					<td>\
						<div class="form-group">\
							<select class="form-control engineerSearch" name="stockProducts['+numItems+'][name]">\
								<option value="">Select Sub Category</option>';
								$.each(availableSubitems, function( key, value ) {
									if(value.id==ree.id){
									html += '<option value="'+value.id+'" data-qty="'+value.totalQty+'" selected>'+value.name+'</option>';
									}
									else {
									html += '<option value="'+value.id+'" data-qty="'+value.totalQty+'">'+value.name+'</option>';
									}
								});
							
							html += '</select>\
						</div>\
					</td>\
					<td>\
						<div class="form-group">\
							<input type="number" name="stockProducts['+numItems+'][qty]" class="form-control" min="1" value="1">\
						</div>\
					</td>\
					<td>\
						<div class="qty">'+ree.totalQty+'</div>\
					</td>\
					<td>\
						<a href="javascript:void" class="removeProduct"><i class="fa fa-trash"></i></a>\
					</td>\
				</tr>\
				';
				
				//linked_sub_item_id
				$('.item_list').append(html);
				$(".engineerSearch").select2({width: '100%'});	
			});
		});
	});
	
	
	$(document).on('click', '.removeProduct', function () {
		$(this).parents("tr").remove();
	});
	
	// $(document).on('click', '.engineerSearch', function () {
		// alert('f');
	// });
	
	// $('.engineerSearch').on('engineerSearch:select', function (e) {
		// alert('f');
	// });

	$(document).on('select2:select', '.engineerSearch',  function (e) {
		var qty = $(this).children("option:selected").attr('data-qty');
		$(this).parents('tr.item').find('div.qty').html(qty);
		
		
		//console.log(slide);
	});

	$(document).on('click', '.removeItem', function () {
		let dataid = $(this).attr("data-id");
		var postUrl = "{{ route('projects.subitemsLinked.delete') }}";
		
		var formData = new FormData();
		formData.append('item', dataid);
		
		ajaxRequest(formData, postUrl, function(res) {
			if(res){
				$('.item_list .item_'+dataid).remove();
			}			
		});
	});
	






	
</script>
@endpush