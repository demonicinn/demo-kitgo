<x-app-layout>
	@section('title', 'Projects')
	<div class="row">
		<div class="col-12">
			<div class="card card-default">
				<div class="card-header">
					<h3 class="card-title">Project Checkout</h3>
				</div>
				
				<div id="scandit-barcode-picker"></div>
			</div>
		</div>
	</div>
	
	@push('scripts')
	<script src="https://cdn.jsdelivr.net/npm/scandit-sdk@5.x"></script>
	<script>
	
		let api_key = 'AY7PrAopKpGYAbjarjEFlRI8kljrO4vVZnaQkvdOozH1e+JF6CoqA5gmDg1TNgvZRGFUiTp76LpoLq859V6WFAZQzD95bfDO+HOPlWJQTNQqKjWvwRjQ/3BHSVqrK6up0hhC4JkvOU+YPXCoEfHcchYw0Yhe/3MOLLhEGN5F1adq/S9CuXDOdbSLeRFxHTQ1wOWulJAnNfTBMI9a3MQ349sOJ8m2Df2TA0WM4Odp6A/+HeFLVyhxvmle7XtHuv9lAkNEj4j3GpE/1fiRhPFms5mgeFRlbkOgbx9PvzLAEjBd/+y9YuhwprHV06FdDmUcBUCV4JGI9PI8KYfSyOaXvF7qNtcavaUr7nmm2jJnG+lZcWEiHsc0cCFwcjCWm+J7CT9/ZlTYp3cwvvJ++WYu8A2YtUWU545yu03sFKMe/GrTmAr/y/YD9cQJSBvirLPvbPccs/PE1f2ggf/PLTyOXp6NNIjEANhWeNnMhRiQHp9SCCF8Kl0EnmYhxG5wHfCvsvuhMvZmoyfGxLdUqzwXxsHetbGQsS9Lpz73hYYk9LtmPHTmNfMehQDqwpTklhythUVJfLd5wT6c9dFXokZNzVNo4uDcj/SxiD3SIphDiRzrktQqz4R+PjC/PVlzPzSVQLt8N7Q2ik4tR/OoLp75YqiY6duQLNylrwm2VbdT924D31LNxAIGc6KgnOHApOyRAkt7I1QtFGIhFsMF76/eesZdpWe6pV869CL9+4xWcL+YxHfFIBHgOUPWaNRVRFiN26DN2fo0lueUVfOylILglVFxgkpY2bkekFnhqqx/Xo2yoohVc+9F6FW1';
		
	$(function () {
		ScanditSDK.configure(api_key, {
			engineLocation: "https://cdn.jsdelivr.net/npm/scandit-sdk@5.x/build/",
		})
		.then(() => {
			return ScanditSDK.BarcodePicker.create(document.getElementById("scandit-barcode-picker"), {
				// enable some common symbologies
				playSoundOnScan: true,
				vibrateOnScan: true,
				
				//scanSettings: new ScanditSDK.ScanSettings({ enabledSymbologies: ["ean8", "ean13", "upca", "upce"] }),
			});
		})
		.then((barcodePicker) => {
			alert(barcodePicker)
			// barcodePicker is ready here, show a message every time a barcode is scanned
			barcodePicker.on("scan", (scanResult) => {
				alert(scanResult.barcodes[0].data);
			});
		});
		
		
	});
</script>
@endpush
</x-app-layout>