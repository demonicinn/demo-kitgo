<x-app-layout>
<div class="content-header">
	<div class="container-fluid">
		<div class="row mb-2 pt-1">
			<div class="col-6">
				<ol class="breadcrumb">
					<li class="breadcrumb-item">
						<a href="{{ route('projects') }}">Projects</a>
					</li>
				</ol>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-8 offset-md-2">
		<div class="card card-default">
			<div class="card-header">
				<h3 class="card-title">Project Edit</h3>
				<div class="card-tools pull-right">
					<a href="{{ route('projects.show', $project->id) }}" title="Back" class="btn btn-warning btn-icon"><i class="fa fa-arrow-left"></i></a>
				</div>
			</div>
			
			
			{!! Form::model($project, ['route' => ['projects.update', $project->id], 'method' => 'PATCH', 'class' => 'form-horizontal']) !!}
				<div class="card-body">
					@include('pages.projects.form')
				</div>
				<div class="card-footer">
					<a href="{{ route('projects.show', $project->id) }}" class="btn btn-default">Back</a>
					<button type="submit" class="btn btn-warning float-right">Update</button>
				</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
</x-app-layout>