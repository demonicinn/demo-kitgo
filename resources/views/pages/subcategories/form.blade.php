<div class="form-group {!! ($errors->has('category') ? 'has-error' : '') !!}">
    {!! Form::label('category','Category', ['class' => 'control-label']) !!}
	{!! Form::select('category', $items, null, ['class' => 'form-control' . ($errors->has('category') ? ' is-invalid' : '') ]) !!}
    {!! $errors->first('category', '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {!! ($errors->has('name') ? 'has-error' : '') !!}">
    {!! Form::label('name','Sub Category name', ['class' => 'control-label']) !!}
    {!! Form::text('name', null, ['class' => 'form-control' . ($errors->has('name') ? ' is-invalid' : '') ]) !!}
    {!! $errors->first('name', '<span class="help-block">:message</span>') !!}
</div>
{{--
<div class="form-group {!! ($errors->has('organization_id') ? 'has-error' : '') !!}">
    {!! Form::label('organization_id','Select Organization', ['class' => 'control-label']) !!}
    <select name="organization_id" class="form-control">
		<option value="">Select Organization</option>
		@if(auth()->user()->role=='superadmin')
			@foreach(\App\Models\Organization::all() as $value)
				<option value="{{ $value->id }}" {{ @$item->organisation_id==$value->id ? 'selected' : '' }}>{{ $value->name }}</option>
			@endforeach
		@else
			@foreach(json_decode(auth()->user()->assigned_organizations) as $organization)
				<option value="{{ $organization }}" {{ @$item->organisation_id==$organization ? 'selected' : '' }}>{{ organizationName($organization) }}</option>
			@endforeach
		@endif
	</select>
    {!! $errors->first('organization_id', '<span class="help-block">:message</span>') !!}
</div>
--}}
<div class="form-group {!! ($errors->has('linked_id') ? 'has-error' : '') !!}">
    {!! Form::label('linked_id','Linked Sub Category', ['class' => 'control-label']) !!}
	{!! Form::select('linked_id[]', $subcategory, @$category->linked_id ? json_decode($category->linked_id) : null, ['class' => 'form-control' . ($errors->has('linked_id') ? ' is-invalid' : ''), 'id'=>'subitems', 'multiple' ]) !!}
    {!! $errors->first('linked_id', '<span class="help-block">:message</span>') !!}
</div>

@push('scripts')
<style>
	.select2-container .select2-selection--single {
		height: 38px;
	}
</style>
<script>	
	$("#subitems").select2({width: '100%', placeholder: 'Select Linked Sub Category' });
</script>
@endpush