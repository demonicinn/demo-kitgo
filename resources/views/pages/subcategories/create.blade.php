<x-app-layout>
<div class="content-header">
	<div class="container-fluid">
		<div class="row mb-2 pt-1">
			<div class="col-6">
				<ol class="breadcrumb">
					<li class="breadcrumb-item">
						<a href="{{ route('subCategories') }}">Sub Categories</a>
					</li>
				</ol>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-8 offset-md-2">
		<div class="card card-default">
			<div class="card-header">
				<h3 class="card-title">Sub Category Create</h3>
				<div class="card-tools pull-right">
					<a href="{{ route('subCategories') }}" title="Back" class="btn btn-warning btn-icon"><i class="fa fa-arrow-left"></i></a>
				</div>
			</div>
			
			{!! Form::open(['route' => 'subCategories.store', 'class' => 'form-horizontal']) !!}
				<div class="card-body">
					@include('pages.subcategories.form')
				</div>
				<div class="card-footer">
					<a href="{{ route('subCategories') }}" class="btn btn-default">Back</a>
					<button type="submit" class="btn btn-info float-right">Create</button>
				</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
</x-app-layout>