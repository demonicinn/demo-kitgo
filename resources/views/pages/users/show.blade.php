<x-app-layout>
<div class="content-header">
	<div class="container-fluid">
		<div class="row mb-2 pt-1">
			<div class="col-6">
				<ol class="breadcrumb">
					<li class="breadcrumb-item">
						<a href="{{ route('users') }}">Users</a>
					</li>
					<li class="breadcrumb-item active">
						{{ @$user->name }}
					</li>
				</ol>
			</div>
			<div class="col-6">
				<ol class="breadcrumb float-right">
					<a class="btn btn-danger btn-sm mr-2" href="javascript:void(0)" data-toggle="modal" data-target="#deleteUser"><i class="fa fa-trash"></i> Delete</a>
					<a class="btn btn-warning btn-sm mr-2" href="{{ route('users.edit', $user->id) }}"><i class="fa fa-edit"></i> Edit</a>
				</ol>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		
		<!-- Profile Image -->
		<div class="card card-default">
			<div class="card-body box-profile pb-0">
				<div class="row">
					<div class="col-6 col-md-3">
						
						@if(@$user->profile_photo_path)
						<img src="{{ asset('storage/profile/'.$user->profile_photo_path) }}" alt="{{ $user->name }}">
						@else
						<h1 class="fas bg-blue p-5 mb-0">{{ userName($user->name) }}</h1>
						@endif
					</div>
					<div class="col-6 col-md-9">
						<h3 class="profile-username">{{ @$user->name }}</h3>
						<p class="text-muted"><i class="fas fa-envelope"></i> {{ @$user->email }}</p>
					</div>
				</div>
			</div>
			
			<div class="card-header p-2">
				<hr/>
				<ul class="nav nav-pills mytabs">
					<li class="nav-item actives"><a class="nav-link dashboard" href="javascript:void(0)" data-tab="dashboard">Dashboard</a></li>
					<li class="nav-item"><a class="nav-link info" href="javascript:void(0)" data-tab="info">Info</a></li>
					<li class="nav-item"><a class="nav-link reservations" href="javascript:void(0)" data-tab="reservations">Reservations</a></li>
					<li class="nav-item"><a class="nav-link checkouts" href="javascript:void(0)" data-tab="checkouts">Check-outs</a></li>
				</ul>
			</div>
		</div>
		
		
		<div class="cutabs dashboard pl-4 pr-4">
			<div class="row">
				<div class="col-md-8">
					<div class="p-2">BOOKINGS</div>
					<div class="card card-default">
						<div id='calendar'></div>
					</div>
				</div>
				<div class="col-md-4">
					
				</div>
			</div>
		</div>
		
		<div class="cutabs info pl-4 pr-4" style="display:none">				
			<div class="row">
				<div class="col-md-8">
					<div class="p-2">INFORMATION</div>
					<div class="card card-default">
						<div class="table-responsive">
							<table class="table">
								<tbody>
									<tr>
										<th style="width:50%">Role</th>
										<td>{{ ucfirst(@$user->role) }}</td>
									</tr>
									<tr>
										<th>Organization</th>
										<td>
										@if(@$user->assigned_organizations)
										@foreach(json_decode($user->assigned_organizations) as $organization)
											{{ organizationName($organization) }},
										@endforeach
										@endif	
										</td>
									</tr>										
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					
				</div>
			</div>
		</div>
		
		<div class="cutabs reservations pl-4 pr-4" style="display:none">
			<div class="p-2">Reservations</div>
			<div class="card card-default">
				<div class="table-responsive">
					<table class="table table-hover text-nowrap">
						<thead>
							<tr>
								<th>Name</th>
								<th>From</th>
								<th>To</th>
								<th>Duration</th>
								<th>Contact</th>
							</tr>
						</thead>
						<tbody>
							@foreach($user->projectitem as $res)
							@if(@$res->project)
							<tr>
								<td><a href="{{ route('projects.show', $res->project->id) }}">{{ $res->project->title }}</a></td>
								<td>{{ dateFormat($res->project->pickup_date) }}</td>
								<td>{{ dateFormat($res->project->expected_return_date) }}</td>
								<td>{{ \Carbon\Carbon::parse( $res->project->pickup_date )->diffInDays( $res->project->expected_return_date ) }} days</td>
								<td>
									@if(@$res->user->name)
									<a href="{{ route('users.show', $res->user->id) }}">
										@if(@$res->user->profile_photo_path)
										<img width="50" class="img-circle elevation-2 mr-2" src="{{ asset('storage/profile/'.$res->user->profile_photo_path) }}" alt="{{ $res->user->name }}">
										@else
										<span class="fas bg-blue img-circle p-2 mb-0">{{ userName($res->user->name) }}</span>
										@endif
									
										{{ ucfirst(@$res->user->name) }}
									</a>
									@endif
								</td>
							</tr>
							@endif
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
		
		<div class="cutabs checkouts pl-4 pr-4" style="display:none">
			<div class="p-2">Check-outs</div>
			<div class="card card-default">
				<div class="table-responsive">
					<table class="table table-hover text-nowrap">
						<thead>
							<tr>
								<th>Name</th>
								<th>From</th>
								<th>To</th>
								<th>Duration</th>
								<th>Contact</th>
							</tr>
						</thead>
						<tbody>
							@foreach($user->projectbarcode as $res)
							@if(@$res->project)
							<tr>
								<td><a href="{{ route('projects.show', $res->project->id) }}">{{ $res->project->title }}</a></td>
								<td>{{ dateFormat($res->project->pickup_date) }}</td>
								<td>{{ dateFormat($res->project->expected_return_date) }}</td>
								<td>{{ \Carbon\Carbon::parse( $res->project->pickup_date )->diffInDays( $res->project->expected_return_date ) }} days</td>
								<td>
									@if(@$res->user->name)
									<a href="{{ route('users.show', $res->user->id) }}">
										@if(@$res->user->profile_photo_path)
										<img width="50" class="img-circle elevation-2 mr-2" src="{{ asset('storage/profile/'.$res->user->profile_photo_path) }}" alt="{{ $res->user->name }}">
										@else
										<span class="fas bg-blue img-circle p-2 mb-0">{{ userName($res->user->name) }}</span>
										@endif
										{{ ucfirst(@$res->user->name) }}
									</a>
									@endif
								</td>
							</tr>
							@endif
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>		
		
	</div>
	
	<div id="deleteUser" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">Delete</h4>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
					<p>Are you sure to Delete User?</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<a href="{{ route('users.destroy', $user->id) }}" class="btn btn-danger" onclick="event.preventDefault(); document.getElementById('delete-form').submit();">Yes, Delete</a>
					<form id="delete-form" action="{{ route('users.destroy', $user->id) }}" method="POST" style="display: none;">
						@csrf
					</form>
				</div>
			</div>
		</div>
	</div>
</div>




@push('scripts')

<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAfADVN1iJ2m-ypbDuNVnJ-QM2VnNj_Oj8&libraries=places&sensor=false"></script>
<script>
	
	$('.mytabs a.nav-link').on('click', function(){
		$('.mytabs a.nav-link').parent('.nav-item').removeClass('actives');
		$(this).parent('.nav-item').addClass('actives');
		
		let tab = $(this).attr('data-tab');
		$('.cutabs').hide();
		$('.cutabs.'+tab).show();
		
	});
	
	
	document.addEventListener('DOMContentLoaded', function() {
		var calendarEl = document.getElementById('calendar');
		
		var calendar = new FullCalendar.Calendar(calendarEl, {
			headerToolbar: {
				left: 'prev,next today',
				center: 'title',
				right: 'dayGridMonth,timeGridWeek,timeGridDay,listWeek'
			},
			initialView: 'dayGridMonth',				
			events: [
				@foreach($user->projectitem as $res)
				{
					title: '{{$res->project->title}} - {{@$res->user->name}}',
					start: '{{$res->project->pickup_date}}',
					end: '{{$res->project->expected_return_date}}'
				},
				@endforeach
				@foreach($user->projectbarcode as $res)
				{
					title: 'Checkout',
					start: '{{$res->project->pickup_date}}',
					end: '{{$res->project->expected_return_date}}'
				},
				@endforeach
			]
		});
		
		calendar.render();
	});
	
	
</script>
@endpush
</x-app-layout>