<div class="form-group {!! ($errors->has('name') ? 'has-error' : '') !!}">
    {!! Form::label('name','Name', ['class' => 'control-label']) !!}
    {!! Form::text('name', null, ['class' => 'form-control' . ($errors->has('name') ? ' is-invalid' : '') ]) !!}
    {!! $errors->first('name', '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {!! ($errors->has('email') ? 'has-error' : '') !!}">
    {!! Form::label('email','Email', ['class' => 'control-label']) !!}
    {!! Form::email('email', null, ['class' => 'form-control' . ($errors->has('email') ? ' is-invalid' : '') ]) !!}
    {!! $errors->first('email', '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {!! ($errors->has('password') ? 'has-error' : '') !!}">
    {!! Form::label('password','Password', ['class' => 'control-label']) !!}
	<input name="password" type="password" class="form-control">
    {!! $errors->first('password', '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {!! ($errors->has('image') ? 'has-error' : '') !!}">
	{!! Form::label('image','Profile Image', ['class' => 'control-label']) !!}</br>
	{!! Form::file('image', null, ['class' => 'form-control' . ($errors->has('image') ? ' is-invalid' : '') ]) !!}
	{!! $errors->first('image', '<span class="help-block">:message</span>') !!}
	
	@if(@$user->profile_photo_path)
	</br>
	<img src="{{ asset('storage/profile/'.$user->profile_photo_path) }}" class="profile-user-img img-responsive img-circle mt-2" alt="{{ $user->name }}">
	@endif	
</div>

<div class="form-group {!! ($errors->has('role') ? 'has-error' : '') !!}">
    {!! Form::label('role','Role', ['class' => 'control-label']) !!}
    <select name="role" class="form-control">
		<option value="">-- Select a Role --</option>
		@if(auth()->user()->role == 'admin' || auth()->user()->role == 'superadmin')
		@foreach (App\Models\User::userRoleList() as $key => $value)
		<option value="{{ $key }}" {{ @$user->role==$key || old('role')==$key ? 'selected' : '' }}>{{ $value }}</option>
		@endforeach
		@else
		@foreach (App\Models\User::managerRolelist() as $key => $value)
		<option value="{{ $key }}" {{ @$user->role==$key || old('role')==$key ? 'selected' : '' }}>{{ $value }}</option>
		@endforeach
		@endif
	</select>
    {!! $errors->first('role', '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {!! ($errors->has('organization') ? 'has-error' : '') !!}">
    {!! Form::label('organization','Organization', ['class' => 'control-label']) !!}
    <select name="organization[]" class="form-control organization" multiple>
		@if(auth()->user()->role == 'admin' || auth()->user()->role == 'superadmin')
			@foreach (App\Models\User::userOrganization() as $key => $value)
			<option value="{{ $value->id }}" {{ @in_array($value->id, json_decode($user->assigned_organizations))  ? 'selected' : '' }}>{{ $value->name }}</option>
			@endforeach
		@else 
			@foreach (App\Models\User::userOrganization() as $key => $value)
			<option value="{{ $value->id }}" {{ @in_array($value->id, json_decode($user->assigned_organizations))  ? 'selected' : '' }}>{{ $value->name }}</option>
			@endforeach
		@endif
	</select>
    {!! $errors->first('organization', '<span class="help-block">:message</span>') !!}
</div>


@push('scripts')
<style>
	.select2-container .select2-selection--single {
		height: 38px;
	}
</style>
<script>
	$(".organization").select2({width: '100%', placeholder:'Select a Organization'});	
	
</script>
@endpush