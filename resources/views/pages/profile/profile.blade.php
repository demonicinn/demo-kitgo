<x-app-layout>
	@section('title', 'Profile')
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2 pt-1">
				<div class="col-6">
					<ol class="breadcrumb">
						<li class="breadcrumb-item">
							<a href="{{ route('profile') }}">Profile</a>
						</li>
					</ol>
				</div>
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-8 offset-md-2">
			<div class="card card-default">
				<div class="card-header">
					<h3 class="card-title">Profile</h3>
				</div>
				
				<div class="card-body">
					
					{!! Form::model($user, ['route' => 'profile.update', 'enctype' => 'multipart/form-data']) !!}				
					
					<div class="form-group {!! ($errors->has('name') ? 'has-error' : '') !!}">
						{!! Form::label('name','Name', ['class' => 'control-label']) !!}
						{!! Form::text('name', null, ['class' => 'form-control' . ($errors->has('name') ? ' is-invalid' : '') ]) !!}
						{!! $errors->first('name', '<span class="help-block">:message</span>') !!}
					</div>
					
					<div class="form-group {!! ($errors->has('email') ? 'has-error' : '') !!}">
						{!! Form::label('email','Email', ['class' => 'control-label']) !!}
						{!! Form::email('email', null, ['class' => 'form-control' . ($errors->has('email') ? ' is-invalid' : '') ]) !!}
						{!! $errors->first('email', '<span class="help-block">:message</span>') !!}
					</div>				
					
					<div class="form-group {!! ($errors->has('image') ? 'has-error' : '') !!}">
						{!! Form::label('image','Profile Image', ['class' => 'control-label']) !!}</br>
						{!! Form::file('image', null, ['class' => 'form-control' . ($errors->has('image') ? ' is-invalid' : '') ]) !!}
						{!! $errors->first('image', '<span class="help-block">:message</span>') !!}
						
						@if(@$user->profile_photo_path)
						</br>
						<img src="{{ asset('storage/profile/'.$user->profile_photo_path) }}" class="profile-user-img img-responsive img-circle mt-2" alt="{{ $user->name }}">
						@endif	
					</div>
					
					<a href="{{ route('profile.password') }}" class="btn btn-default">Change Password</a>
					<button class="btn btn-info float-right">Update</button>
					{!! Form::close() !!}
					
				</div>
			</div>
		</div>
	</div>
</x-app-layout>