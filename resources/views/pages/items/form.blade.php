<div class="form-group {!! ($errors->has('name') ? 'has-error' : '') !!}">
    {!! Form::label('name','Category name', ['class' => 'control-label']) !!}
    {!! Form::text('name', null, ['class' => 'form-control' . ($errors->has('name') ? ' is-invalid' : '') ]) !!}
    {!! $errors->first('name', '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {!! ($errors->has('organization_id') ? 'has-error' : '') !!}">
    {!! Form::label('organization_id','Select Organization', ['class' => 'control-label']) !!}
    <select name="organization_id" class="form-control">
		<option value="">Select Organization</option>
		@if(auth()->user()->role=='superadmin')
			@foreach(\App\Models\Organization::all() as $value)
				<option value="{{ $value->id }}" {{ @$item->organisation_id==$value->id ? 'selected' : '' }}>{{ $value->name }}</option>
			@endforeach
		@else
			@foreach(json_decode(auth()->user()->assigned_organizations) as $organization)
				<option value="{{ $organization }}" {{ @$item->organisation_id==$organization ? 'selected' : '' }}>{{ organizationName($organization) }}</option>
			@endforeach
		@endif
	</select>
    {!! $errors->first('organization_id', '<span class="help-block">:message</span>') !!}
</div>












{{--
<div class="form-group {!! ($errors->has('item_type_id') ? 'has-error' : '') !!}">
    {!! Form::label('item_type_id','Type', ['class' => 'control-label']) !!}
    <select name="item_type_id" class="form-control">
		<option value="">-- Select a Item type --</option>
		@foreach (App\Models\ItemType::getItemTypes() as $key => $value)
		<option value="{{ $value['id'] }}" {{ @$item->item_type_id==$value['id'] ? 'selected' : '' }}>{{ $value['name'] }}</option>
		@endforeach
	</select>
    {!! $errors->first('item_type_id', '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {!! ($errors->has('cable_length') ? 'has-error' : '') !!} cab" style="display:none">
    {!! Form::label('cable_length','Cable length', ['class' => 'control-label']) !!}
    {!! Form::text('cable_length', null, ['class' => 'form-control' . ($errors->has('cable_length') ? ' is-invalid' : '') ]) !!}
    {!! $errors->first('cable_length', '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {!! ($errors->has('make') ? 'has-error' : '') !!} cas">
    {!! Form::label('make','Make', ['class' => 'control-label']) !!}
    {!! Form::text('make', null, ['class' => 'form-control' . ($errors->has('make') ? ' is-invalid' : '') ]) !!}
    {!! $errors->first('make', '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {!! ($errors->has('model') ? 'has-error' : '') !!} cas">
    {!! Form::label('model','Model', ['class' => 'control-label']) !!}
    {!! Form::text('model', null, ['class' => 'form-control' . ($errors->has('model') ? ' is-invalid' : '') ]) !!}
    {!! $errors->first('model', '<span class="help-block">:message</span>') !!}
</div>


<div class="form-group {!! ($errors->has('linked_items') ? 'has-error' : '') !!}">
    {!! Form::label('linked_items','Select a Linked Items', ['class' => 'control-label']) !!}
	{!! Form::select('linked_items[]', $getlinkeditems, $linkeditem ?? null, ['class' => 'form-control engineerSearch' . ($errors->has('model') ? ' is-invalid' : ''), 'multiple'=>'multiple' ]) !!}
    {!! $errors->first('linked_items', '<span class="help-block">:message</span>') !!}
</div>


@push('scripts')
<script>
	@if(@$item->item_type_id=='1')
		$('.cab').show();
		$('.cas').hide();
	@endif
	$(function () {
		$('.engineerSearch').select2({width: '100%'});
		$('select[name=item_type_id]').on('change', function(){
			let val = $(this).val();
			
			$('.cas').show();
			$('.cab').hide();
			if(val=='1'){
				$('.cab').show();
				$('.cas').hide();
			}
		});
	});
	
</script>
@endpush
--}}
