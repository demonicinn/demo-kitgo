<x-app-layout>
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2 pt-1">
				<div class="col-sm-6">
					<ol class="breadcrumb">
						<li class="breadcrumb-item">
							<a href="{{ route('dashboard') }}">Dashboard</a>
						</li>
					</ol>
				</div>
				
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						{{--
						@if(auth()->user()->role=='admin')
						<form method="get">
							<div class="input-group-sm">
								<select name="organization" class="form-control ml-2" onchange="javascript:this.form.submit()">
									<option value="">Select Organization</option>
									@foreach(\App\Models\Organization::all() as $value)
									<option value="{{ $value->id }}" {{ @request()->organization==$value->id ? 'selected' : '' }}>{{ $value->name }}</option>
									@endforeach
								</select>
							</div>
						</form>
						@endif
						--}}
					</ol>
				</div>
			</div>
		</div>
	</div>	
	
	<section class="content">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-3">
					<div class="info-box mb-3">
						<span class="info-box-icon bg-info elevation-1"><i class="fas fa-address-book"></i></span>
						<a href="{{ route('users') }}" class="info-box-content">
							<span class="info-box-text">Total Users</span>
							<span class="info-box-number">{{ @$users }}</span>
						</a>
					</div>
				</div>				
				<div class="col-md-3">
					<div class="info-box mb-3">
						<span class="info-box-icon bg-danger elevation-1"><i class="fas fa-cube"></i></span>
						<a href="{{ route('items') }}" class="info-box-content">
							<span class="info-box-text">Total Categories</span>
							<span class="info-box-number">{{ @$items }}</span>
						</a>
					</div>
				</div>				
				<div class="col-md-3">
					<div class="info-box mb-3">
						<span class="info-box-icon bg-success elevation-1"><i class="fas fa-cube"></i></span>
						<a href="{{ route('sub-items') }}" class="info-box-content">
							<span class="info-box-text">Total Items</span>
							<span class="info-box-number">{{ @$subitems }}</span>
						</a>
					</div>
				</div>				
				<div class="col-md-3">
					<div class="info-box mb-3">
						<span class="info-box-icon bg-warning elevation-1"><i class="fas fa-calendar"></i></span>
						<a href="{{ route('projects') }}" class="info-box-content">
							<span class="info-box-text">Total Projects</span>
							<span class="info-box-number">{{ @$projects }}</span>
						</a>
					</div>
				</div>
			</div>
			
			
			<div class="row">
				<div class="col-md-3">
					<div class="info-box mb-3">
						<span class="info-box-icon bg-info elevation-1"><i class="fas fa-list"></i></span>
						<a href="{{ route('users') }}" class="info-box-content">
							<span class="info-box-text">Reserved Assets</span>
							<span class="info-box-number">{{ @$numberOfAssets }}</span>
						</a>
					</div>
				</div>				
				<div class="col-md-3">
					<div class="info-box mb-3">
						<span class="info-box-icon bg-danger elevation-1"><i class="fas fa-address-card"></i></span>
						<a href="{{ route('items') }}" class="info-box-content">
							<span class="info-box-text">Available Assets</span>
							<span class="info-box-number">{{ @$availableAssets }}</span>
						</a>
					</div>
				</div>				
				<div class="col-md-3">
					<div class="info-box mb-3">
						<span class="info-box-icon bg-success elevation-1"><i class="fas fa-wrench"></i></span>
						<a href="{{ route('sub-items') }}?condition=broken" class="info-box-content">
							<span class="info-box-text">Broken Items</span>
							<span class="info-box-number">{{ @$brokenAssets }}</span>
						</a>
					</div>
				</div>
				<div class="col-md-3">
					<div class="info-box mb-3">
						<span class="info-box-icon bg-warning elevation-1"><i class="fas fa-book"></i></span>
						<a href="{{ route('sub-items') }}?disposed=yes" class="info-box-content">
							<span class="info-box-text">Disposed/Missing</span>
							<span class="info-box-number">{{ $disposed }}</span>
						</a>
					</div>
				</div>
			</div>
			
			
			
			<div class="row">
				<div class="col-md-6">
					<div class="card">
						<div class="card-header">
							<h3 class="card-title">Items By Sub Categories</h3>
						</div>
						<div class="card-body">
							<div id="piechart"></div>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="row">						
						<div class="col-lg-6">
							<a href="{{ route('projects') }}" class="small-box bg-success">
								<div class="inner">
									<h3>{{ @$reservations }}</h3>
									<p>Number of Reservations</p>
								</div>
							</a>
						</div>
						<div class="col-lg-6">
							<a href="{{ route('projects') }}" class="small-box bg-info">
								<div class="inner">
									<h3>{{ @$checkin }}</h3>
									<p>Number of Check In</p>
								</div>
							</a>
						</div>
						<div class="col-lg-6">
							<a href="{{ route('projects') }}" class="small-box bg-warning">
								<div class="inner">
									<h3>{{ @$checkout }}</h3>
									<p>Number of Check Outs</p>
								</div>
							</a>
						</div>
						<div class="col-lg-6">
							<a href="{{ route('projects') }}" class="small-box bg-danger">
								<div class="inner">
									<h3>{{ @$overdueItems }}</h3>
									<p>Number of Overdue Items</p>
								</div>
							</a>
						</div>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-12">
					<div class="card">
						<div class="card-header">
							<h3 class="card-title">RESERVATIONS</h3>
						</div>
						<div class="card-body">
							<div id="calendar"></div>
						</div>
					</div>
				</div>
			</div>
			
		</div>
	</section>
	
	@push('scripts')
	
	
	<script type="text/javascript">
		google.charts.load('current', {'packages':['corechart']});
		google.charts.setOnLoadCallback(drawChart);
		
		function drawChart() {
			
			var data = google.visualization.arrayToDataTable([
				['Items', 'Total Subitems'],
				@foreach($allItems as $item)
					['{{$item->name}}', {{countSubItems($item->id)}}],
				@endforeach
			]);
			
			var options = {
				title: 'Items'
			};
			
			var chart = new google.visualization.PieChart(document.getElementById('piechart'));
			
			chart.draw(data, options);
		}
		
		
		document.addEventListener('DOMContentLoaded', function() {
			var calendarEl = document.getElementById('calendar');
			
			var calendar = new FullCalendar.Calendar(calendarEl, {
				headerToolbar: {
					left: 'prev,next today',
					center: 'title',
					right: 'dayGridMonth,timeGridWeek,timeGridDay,listWeek'
				},
				initialView: 'dayGridMonth',
				events: [
					@foreach($allSubItems as $item)
					@foreach($item->projectitem as $res)
					{
						title: "{!!@$res->project->title!!} - {{@$res->user->name}}",
						start: '{{@$res->project->pickup_date}}',
						end: '{{@$res->project->expected_return_date}}'
					},
					@endforeach
					@foreach($item->projectbarcode as $res)
					{
						title: 'Checkout',
						start: '{{@$res->project->pickup_date}}',
						end: '{{@$res->project->expected_return_date}}'
					},
					@endforeach
					@endforeach
				]
			});
			
			calendar.render();
		});
		
	</script>
	@endpush
</x-app-layout>