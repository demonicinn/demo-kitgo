<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- CSRF Token -->
		<meta name="csrf-token" content="{{ csrf_token() }}">
		<title>{{ config('app.name') }}</title>
		
		<link rel="shortcut icon" type="image/x-icon" href="{{ asset('favicon.png') }}">
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
		<link rel="stylesheet" href="{{ asset('plugins/fontawesome-free/css/all.min.css') }}">
		<link rel="stylesheet" href="{{ asset('dist/css/adminlte.min.css') }}">		
		<link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
		<link rel="stylesheet" href="{{ asset('plugins/fullcalendar/main.min.css') }}">
		<link rel="stylesheet" href="{{ asset('css/custom.css') }}">
		
		<script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.7.0/dist/alpine.js" defer></script>		
		
		<script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>
		<script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
		<script src="{{ asset('plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
		<script src="{{ asset('dist/js/adminlte.min.js') }}"></script>
		<script src="{{ asset('plugins/moment/moment.min.js') }}"></script>
		<script src="{{ asset('plugins/fullcalendar/main.min.js') }}"></script>
		<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
		
		@yield('style')
		
		@trixassets
        @livewireStyles
		
		
	</head>
	<body class="hold-transition sidebar-mini">
		<div class="wrapper">
			
			@include('layouts.include.navbar')
			@include('layouts.include.sidebar')
			
			<div class="content-wrapper">
				<div class="content app">
					@include('layouts.alerts')
					
					<div class="container-fluid">
						{{ $slot }}
					</div>
				</div>
			</div>
			
		</div>
		
		
		
		<script>
		function ajaxRequest(formData, postUrl, callback) {
			var res;
			jQuery.ajax({
				headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') },
				url: postUrl,
				type: 'POST',
				data : formData,
				contentType: false,
				cache : false,
				processData: false,
				success: function (response) {
					res = response;
					callback(res);
				}
			});
		}
		</script>
		
		@stack('modals')
		
		@livewireScripts
		@stack('scripts')
	</body>
</html>