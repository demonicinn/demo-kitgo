<nav class="main-header navbar navbar-expand navbar-white navbar-light">
	<ul class="navbar-nav">
		<li class="nav-item">
			<a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
		</li>
		
		<li class="nav-item">
			{!! Form::open(['route' => 'admin.organization']) !!}
			<div class="input-group-sm1">
				<select name="organizations" class="form-control" onchange="this.form.submit()">
					<option value="">Select Organization</option>
					@if(auth()->user()->role=='superadmin')
						@foreach(\App\Models\Organization::all() as $value)
							<option value="{{ $value->id }}" {{ getOrg()==$value->id ? 'selected' : '' }}>{{ $value->name }}</option>
						@endforeach
					@else
						@foreach(json_decode(auth()->user()->assigned_organizations) as $organization)
							<option value="{{ $organization }}" {{ getOrg()==$organization ? 'selected' : '' }}>{{ organizationName($organization) }}</option>
						@endforeach
					@endif
				</select>
			</div>
			{!! Form::close() !!}
		</li>
		
	</ul>
	<ul class="navbar-nav ml-auto">
		
		<li class="nav-item">
            <a class="nav-link" title="profile" href="{{ route('profile') }}">
				@if(auth()->user()->profile_photo_path)
				<img width="35" class="img-circle elevation-2 mr-2" src="{{ asset('storage/profile/'.auth()->user()->profile_photo_path) }}" alt="{{ auth()->user()->name }}">
				@else
				<span class="fas bg-blue img-circle p-2 mb-0 mr-2">{{ userName(auth()->user()->name) }}</span>
				@endif
				<span class="d-none d-sm-inline-block">
				{{ auth()->user()->name }}
				</span>
			</a>
		</li>
		<li class="nav-item">
            <a class="nav-link" title="logout" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="fa fa-power-off"></i></a>
			<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
				@csrf
			</form>
		</li>
		
	</ul>
</nav>