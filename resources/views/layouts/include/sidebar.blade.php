<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
	<!-- Brand Logo -->
	<a href="{{ route('dashboard') }}" class="brand-link">
		<img src="{{ asset('favicon.png') }}" alt="{{ config('app.name') }}" class="brand-image img-circle elevation-3" style="opacity: .8">
		<span class="brand-text font-weight-light">{{ auth()->user()->organizations->name ?? config('app.name') }}</span>
	</a>
	
	<!-- Sidebar -->
	<div class="sidebar">
		<!-- Sidebar Menu -->
		<nav class="mt-2">
			<ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
				
				<li class="nav-item">
					<a href="{{ route('dashboard') }}" class="nav-link {{ @request()->routeIs('dashboard') ? 'active' : '' }}">
						<i class="nav-icon fas fa-chart-line"></i>
						<p>Dashboard</p>
					</a>
				</li>
				
				@if(auth()->user()->role == 'superadmin' || auth()->user()->role == 'admin')
				<li class="nav-item">
					<a href="{{ route('organizations') }}" class="nav-link {{ @request()->segment('1')=='organizations' ? 'active' : '' }}">
						<i class="nav-icon fas fa-building"></i>
						<p>Organizations</p>
					</a>
				</li>
				
				{{--
				<li class="nav-item">
					<a href="{{ route('user-permissions') }}" class="nav-link {{ @request()->segment('1')=='user-permissions' ? 'active' : '' }}">
						<i class="nav-icon fas fa-users"></i>
						<p>User Permissions</p>
					</a>
				</li>
				--}}
				@endif
				
				@if(auth()->user()->role  == 'superadmin' || auth()->user()->role  == 'admin' || auth()->user()->role  == 'manager')
				<li class="nav-item">
					<a href="{{ route('users') }}" class="nav-link {{ @request()->segment('1')=='users' ? 'active' : '' }}">
						<i class="nav-icon fas fa-address-book"></i>
						<p>Users</p>
					</a>
				</li>
				
				<li class="nav-item">
					<a href="{{ route('items') }}" class="nav-link {{ @request()->segment('1')=='categories' ? 'active' : '' }}">
						<i class="nav-icon fas fa-cube"></i>
						<p>Categories</p>
					</a>
				</li>
				
				<li class="nav-item">
					<a href="{{ route('subCategories') }}" class="nav-link {{ @request()->segment('1')=='sub-categories' ? 'active' : '' }}">
						<i class="nav-icon fas fa-cube"></i>
						<p>Sub Categories</p>
					</a>
				</li>
				
				<li class="nav-item">
					<a href="{{ route('sub-items') }}" class="nav-link {{ @request()->segment('1')=='items' ? 'active' : '' }}">
						<i class="nav-icon fas fa-cube"></i>
						<p>Items</p>
					</a>
				</li>
				
				<li class="nav-item">
					<a href="{{ route('projects') }}" class="nav-link {{ @request()->segment('1')=='projects' ? 'active' : '' }}">
						<i class="nav-icon fas fa-calendar"></i>
						<p>Projects</p>
					</a>
				</li>
				@endif
				
				@if(auth()->user()->role == 'user' )
				<li class="nav-item">
					<a href="{{ route('sub-items') }}" class="nav-link {{ @request()->segment('1')=='items' ? 'active' : '' }}">
						<i class="nav-icon fas fa-cube"></i>
						<p>Items</p>
					</a>
				</li>
				<li class="nav-item">
					<a href="{{ route('projects') }}" class="nav-link {{ @request()->segment('1')=='projects' ? 'active' : '' }}">
						<i class="nav-icon fas fa-calendar"></i>
						<p>Projects</p>
					</a>
				</li>
				@endif
				
				
			</ul>
		</nav>
	</div>
</aside>