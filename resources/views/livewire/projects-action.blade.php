<div class="row">
	<div class="col-12">
		
		<div class="card">
			<div class="navbar navbar-expand navbar-white navbar-light">
				<div class="form-inline input-group-sm">
					Per Page: &nbsp;
					<select wire:model="perPage" class="form-control mr-2">
						<option>10</option>
						<option>15</option>
						<option>25</option>
					</select>
					
					<select wire:model="search_type" class="form-control mr-2">
						<option value="">Project Status</option>
						<option value="live">Live</option>
						<option value="future">In Future</option>
						<option value="archived">Archived</option>
					</select>
					
					{{--
					@if(auth()->user()->role=='admin')
					<select wire:model="organization" class="form-control ml-2">
						<option value="">Select Organization</option>
						@foreach(\App\Models\Organization::all() as $value)
						<option value="{{ $value->id }}">{{ $value->name }}</option>
						@endforeach
					</select>
					@endif
					--}}
					
					<div class="mb-2 mt-2 input-group input-group-sm">
						<input wire:model.debounce.1000ms="search" class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
						<div class="input-group-append">
							<button class="btn btn-navbar" type="submit">
								<i class="fas fa-search"></i>
							</button>
						</div>
					</div>
					
					<div class="navbar-nav ml-md-auto">
						<button class="btn btn-{{ $view=='list' ? 'primary' : 'success' }} btn-sm ml-2" wire:click="selectView('list')" type="button">
							<i class="fas fa-list"></i>
						</button>
						<button class="btn btn-{{ $view=='grid' ? 'primary' : 'success' }} btn-sm ml-2" wire:click="selectView('grid')"  type="button">
							<i class="fas fa-th"></i>
						</button>
					</div>
				</div>
			</div>
			
			@if($view=='list')
			<div class="card-body table-responsive p-0">
                <table class="table table-hover text-nowrap">
					<thead>
						<tr>
							<th>
								<a wire:click.prevent="sortBy('title')" href="javascript:void(0)">Title
									@include('includes._sort-icon', ['field' => 'title'])
								</a>
							</th>
							<th>
								<a wire:click.prevent="sortBy('pickup_date')" href="javascript:void(0)">Pickup Date
									@include('includes._sort-icon', ['field' => 'pickup_date'])
								</a>
							</th>
							<th>
								<a wire:click.prevent="sortBy('expected_return_date')" href="javascript:void(0)">Return Date
									@include('includes._sort-icon', ['field' => 'expected_return_date'])
								</a>
							</th>
							<th>
								Duration
							</th>
							<th>
								Contact
							</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($projects as $project)
						<tr>
							<td>
								<div class="row">
									<div class="col-md-9">
										<a href="{{ route('projects.show', $project->id) }}">{{ @$project->title }}</a>
										
										@if($project->status=='2')
										</br>
										<p class="badge badge-warning">Awaiting Approval</p>
										@endif
										@if($project->status=='0')
										</br>
										<p class="badge badge-danger">Not active</p>
										@endif
									</div>
									<div class="col-md-3">
										@if(auth()->user()->role != 'user' && $project->status=='2')
										<div class="input-group-prepend">
											<button type="button" class="btn btn-default btn-sm ml-2 dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
												Action
											</button>
											<ul class="dropdown-menu" style="">
												<li class="dropdown-item"><a href="{{ route('projects.projectStatus', [$project->id, 'approve']) }}">Approve</a></li>
												<li class="dropdown-item"><a href="{{ route('projects.projectStatus', [$project->id, 'reject']) }}">Reject</a></li>
											</ul>
										</div>
										@endif
									</div>
								</div>
							</td>
							<td>{{ dateFormat($project->pickup_date) }}</td>
							<td>{{ dateFormat($project->expected_return_date) }}</td>
							<td>{{ \Carbon\Carbon::parse( $project->pickup_date )->diffInDays( $project->expected_return_date ) }} days</td>
							<td>
								@if(@$project->user)
								<a href="{{ route('users.show', $project->projectUser->id) }}">
									@if(@$project->projectUser->profile_photo_path)
									<img width="38" class="img-circle elevation-2 mr-2" src="{{ asset('storage/profile/'.$project->projectUser->profile_photo_path) }}" alt="{{ $project->projectUser->name }}">
									@else
									<span class="fas bg-blue img-circle p-2 mb-0 mr-2">{{ userName($project->projectUser->name) }}</span>
									@endif
									
								{{ ucfirst(@$project->projectUser->name) }}
								</a>
								@endif
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
			@endif
		</div>
		
		@if($view=='grid')
		<div class="gridView m-3">
			<div class="row d-flex align-items-stretch">
				@foreach ($projects as $project)
				<div class="col-md-4">
					<div class="card">
						<div class="card-body">
							<div class="row">
								<div class="col-12">
									<h4 class="lead">
										<b>{{ @$project->title }}</b>
										@if($project->status=='2')
										<p class="badge badge-warning mb-0">Awaiting Approval</p>
										@endif
										@if($project->status=='0')
										<p class="badge badge-danger mb-0">Not active</p>
										@endif
									</h4>
									
									<ul class="ml-4 mb-0 fa-ul text-muted">
										<li class="small"><span class="fa-li"><i class="fas fa-lg fa-calendar"></i></span> Pickup Date: {{ dateFormat($project->pickup_date) }}</li>
										<li class="small"><span class="fa-li"><i class="fas fa-lg fa-calendar"></i></span> Return Date: {{ dateFormat($project->expected_return_date) }}</li>
										<li class="small"><span class="fa-li"><i class="fas fa-lg fa-clock"></i></span> Duration: {{ \Carbon\Carbon::parse( $project->pickup_date )->diffInDays( $project->expected_return_date ) }} days</li>
										<li class="small"><span class="fa-li"><i class="fas fa-lg fa-user"></i></span> Contact: 
										@if(@$project->user)
										<a href="{{ route('users.show', $project->projectUser->id) }}">
											@if(@$project->projectUser->profile_photo_path)
											<img width="33" class="img-circle elevation-2 mr-2" src="{{ asset('storage/profile/'.$project->projectUser->profile_photo_path) }}" alt="{{ $project->projectUser->name }}">
											@else
											<span class="fas bg-blue img-circle p-2 mb-0 mr-2">{{ userName($project->projectUser->name) }}</span>
											@endif
											{{ ucfirst(@$project->projectUser->name) }}
										</a>
										@endif
										</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="card-footer">
							<div class="text-right">
								
								@if(auth()->user()->role != 'user' && $project->status=='2')
									<button type="button" class="btn btn-default btn-sm ml-2 dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
										Action
									</button>
									<ul class="dropdown-menu" style="">
										<li class="dropdown-item"><a href="{{ route('projects.projectStatus', [$project->id, 'approve']) }}">Approve</a></li>
										<li class="dropdown-item"><a href="{{ route('projects.projectStatus', [$project->id, 'reject']) }}">Reject</a></li>
									</ul>
								@endif
								
								<a href="{{ route('projects.show', $project->id) }}" class="btn btn-sm btn-primary">
									<i class="fas fa-calendar"></i> View Details
								</a>
							</div>
						</div>
					</div>
				</div>
				@endforeach
			</div>
		</div>
		@endif
		
		<div class="pagination">
			{{ $projects->links() }}
		</div>
		
	</div>
</div>
