<div>	
	<div class="row mb-2 pt-1">
		<div class="col-6">
			<div class="p-2">EQUIPMENT {{ @$projects->withSubitems ? $projects->withSubitems->count() : '' }} ITEMS</div>
		</div>
		<div class="col-6">
			@if($checkout)
			<button type="submit" class="btn btn-warning btn-sm pull-right" wire:click="checkout" wire:loading.attr="disabled">
				Check In/Check Out
			</button>
			<button type="submit" class="btn btn-warning mr-2 btn-sm pull-right" wire:click="itemMissed" wire:loading.attr="disabled">
				Marked as Missing
			</button>
			@endif
		</div>
	</div>
	
	@if($projects && $projects->projectitems)
	@foreach($projects->projectitems as $item)
	@if(@$item->subitem)
	<div class="card">
		<div class="card-header p-2">
		
			<div class="row">
				<div class="col-4 col-md-2">
					<a href="{{ route('subitems.show', $item->subitem->id) }}">
						@if(@$item->subitem->pictureurl)
						<img width="100" src="{{ asset('storage/photos/'.$item->subitem->pictureurl) }}">
						@else
						<img width="100" src="{{ asset('img/noimg.png') }}">
						@endif
					</a>
				</div>
				<div class="col-7 col-md-9">
					<ul class="m-0 fa-ul text-muted">
						<li class="small">
							<a href="{{ route('subitems.show', $item->subitem->id) }}">
								<b>{{ @$item->subitem->make }} - {{ @$item->subitem->model }}</b>
							</a>
						</li>
						<li class="small"><p class="badge badge-{{ subItemColor($item->status) }} m-0 p-1">{{ subItemStatus($item->status) }}</p></li>
						<li class="small">
							@if($item->subitem->barcode_url)
							<img width="{{ @$item->subitem->make=='Cable' ? '50' : '15' }}" src="{{ asset('assets/barcodes/'.$item->subitem->barcode_url) }}">
							@endif
							{{ @$item->subitem->barcode_no }}
						</li>
					</ul>
				</div>
				@if(@$item->status!='warehouse_return' && $item->subitem->is_missed=='0' && $projects->status=='1')
				<div class="col-1 col-md-1 text-right">				
					<input type="checkbox" name="checkout[{{$item->id}}]" wire:model="checkout" value="{{$item->id}}">
				</div>
				@endif
			</div>
			
		</div>
	</div>
	@endif
	@endforeach
	@endif
</div>


