<div class="row">
	<div class="col-12">
		
		<div class="card">
			<div class="navbar navbar-expand navbar-white navbar-light">
				<div class="form-inline input-group-sm">
					Per Page: &nbsp;
					<select wire:model="perPage" class="form-control">
						<option>10</option>
						<option>15</option>
						<option>25</option>
					</select>					
					
					<div class="ml-2 input-group input-group-sm">
						<input wire:model="search" class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
						<div class="input-group-append">
							<button class="btn btn-navbar" type="submit">
								<i class="fas fa-search"></i>
							</button>
						</div>
					</div>
				</div>
				
				<div class="navbar-nav ml-auto">
					<a href="#" title="Add" class="btn btn-success btn-sm pull-right"><i class="fa fa-plus"></i> Create</a>
				</div>
			</div>
			
			<div class="card-body table-responsive p-0">
                <table class="table table-hover text-nowrap">
					<thead>
						<tr>
							<th>
								<a wire:click.prevent="sortBy('role')" href="javascript:void(0)">Role
									@include('includes._sort-icon', ['field' => 'role'])
								</a>
							</th>
							<th>
								<a wire:click.prevent="sortBy('route_name')" href="javascript:void(0)">Route Name
									@include('includes._sort-icon', ['field' => 'route_name'])
								</a>
							</th>
							<th>
								<a wire:click.prevent="sortBy('created_at')" href="javascript:void(0)">Created At
									@include('includes._sort-icon', ['field' => 'created_at'])
								</a>
							</th>
							<th>
								Action
							</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($permissions as $permission)
						<tr>
							<td>{{ $permission->role }}</td>
							<td>{{ $permission->route_name }}</td>
							<td>{{ $permission->created_at ? $permission->created_at->format('d/m/Y') : '' }}</td>
							<td>
								<div class="btn-icon-list">
									<a href="javascript:void(0)">
										<i class="fa fa-edit"></i>
									</a>
									
									<a wire:click="deleteShowModal({{ $permission->id }})" href="javascript:void(0)">
										<i class="fa fa-trash"></i>
									</a>
								</div>
							</td>	
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
		
		<div class="pagination">
			{{ $permissions->links() }}
		</div>
		
	</div>

</div>
