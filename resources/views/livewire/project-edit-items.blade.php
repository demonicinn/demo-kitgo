<div>
    @if(@$project->projectitems)
	@foreach ($project->projectitems as $item)
	<tr>
		<td>
		{{ @$item->subitem->make }} - {{ @$item->subitem->model }}
		</td>
		<td>
		{{ @$item->quantity }}
		</td>
		<td></td>
		<td>
			<a wire:click="removeItem('{{$item->id}}')" href="javascript:void(0)"><i class="fa fa-trash"></i></a>
		</td>
	</tr>
	@endforeach
	@endif
</div>
