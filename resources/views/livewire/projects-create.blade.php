<div>
	<div class="card-body">
		<div class="form-group {!! ($errors->has('title') ? 'has-error' : '') !!}">
			{!! Form::label('title','Title', ['class' => 'control-label']) !!}
			<input class="form-control" name="title" type="text" wire:model="title">
			{!! $errors->first('title', '<span class="help-block">:message</span>') !!}
		</div>
		
		<div class="form-group {!! ($errors->has('client_name') ? 'has-error' : '') !!}">
		{!! Form::label('client_name','Client Name', ['class' => 'control-label']) !!}
			<input class="form-control" name="client_name" type="text" wire:model="client_name">
			{!! $errors->first('client_name', '<span class="help-block">:message</span>') !!}
		</div>
		
		<div class="form-group {!! ($errors->has('client_contact_number') ? 'has-error' : '') !!}">
		{!! Form::label('client_contact_number','Client Contact Number', ['class' => 'control-label']) !!}
			<input class="form-control" name="client_contact_number" type="text" wire:model="client_contact_number">
			{!! $errors->first('client_contact_number', '<span class="help-block">:message</span>') !!}
		</div>
		
		<div class="form-group {!! ($errors->has('location') ? 'has-error' : '') !!}">
		{!! Form::label('location','Client Location', ['class' => 'control-label']) !!}
			<input class="form-control" name="location" type="text" wire:model="location" id="autocomplete">
			{!! $errors->first('location', '<span class="help-block">:message</span>') !!}
		</div>
		
		<div class="form-group {!! ($errors->has('pickup_date') ? 'has-error' : '') !!}">
			{!! Form::label('pickup_date','Pickup Date', ['class' => 'control-label']) !!}
			<input class="form-control" name="pickup_date" type="date" wire:model="pickup_date" wire:change="checkItems()" min="{{date('Y-m-d')}}">
			{!! $errors->first('pickup_date', '<span class="help-block">:message</span>') !!}
		</div>
		
		<div class="form-group {!! ($errors->has('shipping_date') ? 'has-error' : '') !!}">
			{!! Form::label('shipping_date','Shipping Date', ['class' => 'control-label']) !!}
			<input class="form-control" name="shipping_date" type="date" wire:model="shipping_date" min="{{date('Y-m-d')}}">
			{!! $errors->first('shipping_date', '<span class="help-block">:message</span>') !!}
		</div>

		<div class="form-group {!! ($errors->has('start_date') ? 'has-error' : '') !!}">
			{!! Form::label('start_date','Start Date', ['class' => 'control-label']) !!}
			<input class="form-control" name="start_date" type="date" wire:model="start_date" min="{{date('Y-m-d')}}">
			{!! $errors->first('start_date', '<span class="help-block">:message</span>') !!}
		</div>

		<div class="form-group {!! ($errors->has('end_date') ? 'has-error' : '') !!}">
			{!! Form::label('end_date','End Date', ['class' => 'control-label']) !!}
			<input class="form-control" name="end_date" type="date" wire:model="end_date" min="{{date('Y-m-d')}}">
			{!! $errors->first('end_date', '<span class="help-block">:message</span>') !!}
		</div>

		<div class="form-group {!! ($errors->has('expected_return_date') ? 'has-error' : '') !!}">
			{!! Form::label('expected_return_date','Return Date', ['class' => 'control-label']) !!}
			<input class="form-control" name="expected_return_date" type="date" wire:model="expected_return_date" min="{{date('Y-m-d')}}">
			{!! $errors->first('expected_return_date', '<span class="help-block">:message</span>') !!}
		</div>
		
		<div class="form-group {!! ($errors->has('user') ? 'has-error' : '') !!}">
			{!! Form::label('user','Contact Person', ['class' => 'control-label']) !!}
			<select class="form-control" name="user" wire:model="user">
				@foreach(App\Models\Project::getUsers() as $user)
					<option value="{{ $user->id }}">{{ $user->name }} - {{ $user->email }}</option>
				@endforeach
			</select>	
			{!! $errors->first('user', '<span class="help-block">:message</span>') !!}
		</div>
		
		<table class="table" id="products_table">
			<thead>
				<tr>
					<th width="50%">Items</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				@if($projectitems)
				@foreach ($projectitems as $item)
				<tr>
					<td>
					{{ @$item->subitem->make }} - {{ @$item->subitem->model }}
					</td>
				</tr>
				@endforeach
				@endif
				
				@if(@$stockProducts)
				@foreach ($stockProducts as $index => $subids)
				<tr>
					<td>
						<div class="form-group">
							<select class="form-control select_dp_{{$index}}" name="stockProducts[{{$index}}][id]" data-name="stockProducts.{{$index}}.id" wire:model="stockProducts.{{$index}}.id" wire:change="checkProduct({{$index}})">
								<option value="">Select Item</option>
								@foreach($getsubitems as $item)
									<option value="{{ $item->id }}">{{ @$item->make }} - {{ @$item->model }}</option>
								@endforeach
							</select>
						</div>
					</td>
					<td>
						<a href="#" wire:click.prevent="removeProduct({{$index}})"><i class="fa fa-trash"></i></a>
					</td>
				</tr>
				
				<script>
					$('.select_dp_{{$index}}').select2({width: '100%'});
					$('.1_select_dp_{{$index}}').on('change', function (e) {
						let name = $(this).attr('data-name');
						var data = $('.select_dp_{{$index}}').select2("val");
						//@this.emitTo('foo', 'sa');
						//@this.set('foo', () => {})
						@this.set(name, data);
					});
				</script>
				@endforeach
				@endif
			</tbody>
		</table>
		
		
		<div class="col-md-12">
			<button type="button" class="btn btn-info" wire:click.prevent="addProduct" wire:loading.attr="disabled">
				{{ __('+  Add Another Item')}}
			</button>
		</div>
	</div>
	

	<div class="card-footer">
		@if(@$project_id)
		<a href="{{ route('projects.show', $project_id) }}" class="btn btn-default">Back</a>
		<button type="submit" class="btn btn-warning float-right" wire:click="update" wire:loading.attr="disabled">Update</button>		
		@else
		<a href="{{ route('projects') }}" class="btn btn-default">Back</a>
		<button type="submit" class="btn btn-info float-right" wire:click="create" wire:loading.attr="disabled">Create</button>
		@endif
	</div>
	
	
	
	
	@push('scripts')
	<style>
		.select2-container .select2-selection--single {
			height: 38px;
		}
	</style>
	<style>
	.dropbtn {
	  background-color: #4CAF50;
	  color: white;
	  padding: 16px;
	  font-size: 16px;
	  border: none;
	  cursor: pointer;
	}

	.dropbtn:hover, .dropbtn:focus {
	  background-color: #3e8e41;
	}

	#myInput {
	  box-sizing: border-box;
	  background-image: url('searchicon.png');
	  background-position: 14px 12px;
	  background-repeat: no-repeat;
	  font-size: 16px;
	  padding: 14px 20px 12px 45px;
	  border: none;
	  border-bottom: 1px solid #ddd;
	}

	#myInput:focus {outline: 3px solid #ddd;}

	.dropdown {
	  position: relative;
	  display: inline-block;
	}

	.dropdown-content {
	  position: absolute;
	  background-color: #f6f6f6;
	  min-width: 230px;
	  overflow: auto;
	  border: 1px solid #ddd;
	  z-index: 1;
	}

	.dropdown-content a {
	  color: black;
	  padding: 12px 16px;
	  text-decoration: none;
	  display: block;
	}

	.dropdown a:hover {background-color: #ddd;}

	.show {display: block;}
	</style>
	<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key={{ config('services.google.key') }}&libraries=places&sensor=false"></script>
	<script>
	
		google.maps.event.addDomListener(window, 'load', initialize);
	
		function initialize() {
			var input = document.getElementById('autocomplete');
			var options = {
				componentRestrictions: {country: 'uk'}//UK only
			};
			
			var autocomplete = new google.maps.places.Autocomplete(input, options);
			autocomplete.addListener('place_changed', function() {
				var place = autocomplete.getPlace();
				var address = place.formatted_address;
				@this.set('location', address);
			});
		}
		
		
		
		function filterFunction() {
  var input, filter, ul, li, a, i;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  div = document.getElementById("myDropdown");
  a = div.getElementsByTagName("a");
  for (i = 0; i < a.length; i++) {
    txtValue = a[i].textContent || a[i].innerText;
    if (txtValue.toUpperCase().indexOf(filter) > -1) {
      a[i].style.display = "";
    } else {
      a[i].style.display = "none";
    }
  }
}

	$('.filter').on('keyup', function(){
	
	
	});
	</script>
	@endpush
</div>
