<div class="row">
	<div class="col-12">
		
		<div class="card">
			<div class="navbar navbar-expand navbar-white navbar-light">
				<div class="form-inline input-group-sm">
					Per Page: &nbsp;
					<select wire:model="perPage" class="form-control mr-2">
						<option>10</option>
						<option>15</option>
						<option>25</option>
					</select>
					
					{{--
					@if(auth()->user()->role=='admin')
					<select wire:model="organizations" class="form-control mr-2">
						<option value="">Select Organization</option>
						@foreach(\App\Models\Organization::all() as $value)
						<option value="{{ $value->id }}">{{ $value->name }}</option>
						@endforeach
					</select>
					@endif
					--}}
					
					<div class="mb-2 mt-2 input-group input-group-sm">
						<input wire:model.debounce.1000ms="search" class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
						<div class="input-group-append">
							<button class="btn btn-navbar" type="submit">
								<i class="fas fa-search"></i>
							</button>
						</div>
					</div>
					
					<div class="navbar-nav ml-md-auto">
						<button class="btn btn-{{ $view=='list' ? 'primary' : 'success' }} btn-sm ml-2" wire:click="selectView('list')" type="button">
							<i class="fas fa-list"></i>
						</button>
						<button class="btn btn-{{ $view=='grid' ? 'primary' : 'success' }} btn-sm ml-2" wire:click="selectView('grid')"  type="button">
							<i class="fas fa-th"></i>
						</button>
					</div>					
					
				</div>
				
			</div>
			
			@if($view=='list')
			<div class="card-body table-responsive p-0">
                <table class="table table-hover text-nowrap">
					<thead>
						<tr>
							<th>
								<a wire:click.prevent="sortBy('name')" href="javascript:void(0)">Name
									@include('includes._sort-icon', ['field' => 'name'])
								</a>
							</th>
							<th>
								<a wire:click.prevent="sortBy('email')" href="javascript:void(0)">Email
									@include('includes._sort-icon', ['field' => 'email'])
								</a>
							</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($users as $user)
						<tr>
							<td>
								<a href="{{ route('users.show', $user->id) }}">
									@if(@$user->profile_photo_path)
									<img width="38" class="img-circle elevation-2 mr-2" src="{{ asset('storage/profile/'.$user->profile_photo_path) }}" alt="{{ $user->name }}">
									@else
									<span class="fas bg-blue img-circle p-2 mb-0 mr-2">{{ userName($user->name) }}</span>
									@endif
									{{ @$user->name }}
								</a>
							</td>
							<td>{{ @$user->email }}</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
			@endif
		</div>
		
		@if($view=='grid')
		<div class="gridView">
			<div class="p-3">
				<div class="row d-flex align-items-stretch">
					@foreach ($users as $user)
					<div class="col-md-4">
						<div class="card">
							<div class="card-body">
								<div class="row">
									<div class="col-8">
										<h2 class="lead"><b>{{ @$user->name }}</b></h2>
										<ul class="ml-4 mb-0 fa-ul text-muted">
											<li class="small"><span class="fa-li"><i class="fas fa-lg fa-envelope"></i></span> Email #: {{ @$user->email }}</li>
										</ul>
									</div>
									<div class="col-4 text-center">
										@if(@$user->profile_photo_path)
										<img width="90" src="{{ asset('storage/profile/'.$user->profile_photo_path) }}" alt="{{ $user->name }}">
										@else
										<h2 class="fas bg-blue p-4 mb-0">{{ userName($user->name) }}</h2>
										@endif
									</div>
								</div>
							</div>
							<div class="card-footer">
								<div class="text-right">
									<a href="{{ route('users.show', $user->id) }}" class="btn btn-sm btn-primary">
										<i class="fas fa-user"></i> View Profile
									</a>
								</div>
							</div>
						</div>
					</div>
					@endforeach
				</div>
			</div>
		</div>
		@endif
		
		<div class="pagination">
			{{ $users->links() }}
		</div>
		
	</div>
</div>
