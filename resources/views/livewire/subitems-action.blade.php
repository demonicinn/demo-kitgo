<div class="row">
	<div class="col-12">
		
		<div class="card">
			<div class="navbar navbar-expand navbar-white navbar-light">
				<div class="form-inline input-group-sm">
					Per Page: &nbsp;
					<select wire:model="perPage" class="form-control ml-2">
						<option>10</option>
						<option>15</option>
						<option>25</option>
					</select>
					
					<select wire:model="condition" class="form-control mb-2 mt-2 ml-2">
						<option value="">Select Condition</option>
						@foreach(\App\Models\Subitem::getConditions() as $k => $value)
						<option value="{{ $k }}">{{ $value }}</option>
						@endforeach
					</select>
					
					<select wire:model="category" wire:change="getSubCategories()" class="form-control ml-2">
						<option value="">Select Category</option>
						@foreach($items as $value)
						<option value="{{ $value->id }}">{{ $value->name }}</option>
						@endforeach
					</select>
					
					<select wire:model="subcategory" class="form-control ml-2">
						<option value="">Select Sub Category</option>
						@foreach($subcategoryList as $value)
						<option value="{{ $value->id }}">{{ $value->name }}</option>
						@endforeach
					</select>
					
					<div class="mb-2 mt-2 ml-2 input-group input-group-sm">
						<input wire:model.debounce.1000ms="search" class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
						<div class="input-group-append">
							<button class="btn btn-navbar" type="submit">
								<i class="fas fa-search"></i>
							</button>
						</div>
					</div>
					
					<div class="navbar-nav ml-md-auto">
						<button class="btn btn-{{ $view=='list' ? 'primary' : 'success' }} btn-sm ml-2" wire:click="selectView('list')" type="button">
							<i class="fas fa-list"></i>
						</button>
						<button class="btn btn-{{ $view=='grid' ? 'primary' : 'success' }} btn-sm ml-2" wire:click="selectView('grid')"  type="button">
							<i class="fas fa-th"></i>
						</button>
					</div>
					
					<div class="mb-2 mt-2 ml-2">
						<input class="mt-1 pl-1 pl-1" wire:model="disposed" value="1" type="checkbox">
						<span>Show Disposed/Missing</span>
					</div>
				</div>
				
			</div>
			
			@if($view=='list')
			<div class="card-body table-responsive p-0">
                <table class="table table-hover text-nowrap">
					<thead>
						<tr>
							<th>
								<a wire:click.prevent="sortBy('name')" href="javascript:void(0)">Name
									@include('includes._sort-icon', ['field' => 'name'])
								</a>
							</th>
							<th>
								<a wire:click.prevent="sortBy('condition')" href="javascript:void(0)">Condition
									@include('includes._sort-icon', ['field' => 'condition'])
								</a>
							</th>
							<th>
								<a wire:click.prevent="sortBy('item_id')" href="javascript:void(0)">Sub Category
									@include('includes._sort-icon', ['field' => 'item_id'])
								</a>
							</th>
							@if(auth()->user()->role!='user')
							<th>Print</th>
							@endif
							<th></th>
						</tr>
					</thead>
					<tbody>
						@foreach ($subitems as $item)
						<tr>
							<td>
								<div class="row">
									<div class="col-5 col-md-3">
										<a href="{{ route('subitems.show', $item->id) }}">
											@if(@$item->pictureurl)
											<img width="50" src="{{ asset('storage/photos/'.$item->pictureurl) }}">
											@else
											<img width="50" src="{{ asset('img/noimg.png') }}">
											@endif
										</a>
									</div>
									<div class="col-7 col-md-9">
										<ul class="m-0 fa-ul text-muted">
											<li class="small">
												<a href="{{ route('subitems.show', $item->id) }}">
													<b>{{ @$item->make }} - {{ @$item->model }}</b>
												</a>
											</li>
											<li class="small"><p class="badge badge-{{ subItemColor($item->status) }} m-0 p-1">{{ subItemStatus($item->status) }}</p></li>
											<li class="small">
												@if($item->barcode_url)
												<img width="{{ @$item->make=='Cable' ? '50' : '15' }}" src="{{ asset('assets/barcodes/'.$item->barcode_url) }}">
												@endif
												{{ @$item->barcode_no }}
											</li>
										</ul>
									</div>
								</div>
							</td>
							<td>{{ $item->condition=='needrepair' ? 'Need Repair' : ucfirst($item->condition) }}</td>
							<td>{{ @subcategory($item->item_id) }}</td>
							@if(auth()->user()->role!='user')
							<td>
								<button type="button" data-id="print_{{$item->id}}" class="btn btn-primary print">Print</button>
								<div style="display:none">
									<div id="print_{{$item->id}}">
										<img src="{{ asset('assets/barcodes/'.$item->barcode_url)  }}">
									</div>
								</div>
							</td>
							@endif
							<td>
								<div class="input-group-prepend">
									<button type="button" class="btn btn-default btn-sm ml-2 dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
										Action
									</button>
									<ul class="dropdown-menu">
										@if($item->status=='available')
										<li class="dropdown-item"><a href="{{ route('subitems.quickCheckout', $item->id) }}">Quick Checkout</a></li>
										@endif
										@if(auth()->user()->role!='user')
										<li class="dropdown-item"><a href="javascript:void(0)" wire:click="organizationModal({{$item->id}})" data-toggle="modal" data-target="#modalOrganizationVisible" title="Change Organization">Change Organization</a></li>
										@endif
									</ul>
								</div>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
		@endif
		
		@if($view=='grid')
		<div class="gridView">
			<div class="p-3">
				<div class="row d-flex align-items-stretch">
					@foreach ($subitems as $item)
					<div class="col-md-4">
						<div class="card">
							<div class="card-body">
								<div class="row">
									<div class="col-7">
										<h2 class="lead"><b>{{ @$item->make }} - {{ @$item->model }}</b></h2>
										<ul class="ml-2 mb-0 fa-ul text-muted">
											<li class="small">Condition: {{ $item->condition=='needrepair' ? 'Need Repair' : ucfirst($item->condition) }}</li>
											<li class="small">Category: {{ @subcategory($item->item_id) }}</li>
											<li class="small">Status: 
												<p class="badge badge-{{ subItemColor($item->status) }} p-1">{{ subItemStatus($item->status) }}</p>
											</li>
										</ul>
									</div>
									<div class="col-5 text-center">
										@if(@$item->pictureurl)
										<img width="100" src="{{ asset('storage/photos/'.$item->pictureurl) }}">
										@else
										<img width="100" src="{{ asset('img/noimg.png') }}">
										@endif
									</div>
								</div>
							</div>
							<div class="card-footer">
								<div class="text-right">
									
									
									<button type="button" class="btn btn-default btn-sm ml-2 dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
										Action
									</button>
									<ul class="dropdown-menu">
										@if($item->status=='available')
										<li class="dropdown-item"><a href="{{ route('subitems.quickCheckout', $item->id) }}">Quick Checkout</a></li>
										@endif
										<li class="dropdown-item"><a href="javascript:void(0)" wire:click="organizationModal({{$item->id}})" data-toggle="modal" data-target="#modalOrganizationVisible" title="Change Organization">Change Organization</a></li>
									</ul>
									
								
									<button type="button" data-id="print_{{$item->id}}" class="btn btn-sm btn-primary print">Print</button>
									
									<a href="{{ route('subitems.show', $item->id) }}" class="btn btn-sm btn-primary">
										<i class="fas fa-cube"></i> View Details
									</a>
								</div>
							</div>
						</div>
					</div>
					@endforeach
				</div>
			</div>
		</div>
		@endif
		
		<div class="pagination">
			{{ $subitems->links() }}
		</div>
		
		
		<div wire:ignore.self class="modal fade" id="modalOrganizationVisible" tabindex="-1" role="dialog" aria-labelledby="modalOrganizationVisible" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">{{ __('Change Organization') }}</h4>
					</div>
					<div class="modal-body">
						<div class="form-group {!! ($errors->has('organizationId') ? 'has-error' : '') !!}">
							{!! Form::label('organizationId','Select Organization', ['class' => 'control-label']) !!}
							<select wire:model="organizationId" class="form-control">
								<option value="">Select Organization</option>
								@if(auth()->user()->assigned_organizations)
								@foreach(json_decode(auth()->user()->assigned_organizations) as $organization)
									<option value="{{ $organization }}" {{ $oldOrganization==$organization ? 'selected' : '' }}>{{ organizationName($organization) }}</option>
								@endforeach
								@endif
							</select>
							{!! $errors->first('organizationId', '<span class="help-block">:message</span>') !!}
						</div>						
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
						<button type="submit" class="btn btn-success" wire:click="changeOrganization" wire:loading.attr="disabled">Change</button>
					</div>
				</div>
			</div>
		</div>
	
	</div>
	
	
	@push('scripts')
	<script>		
		$('.print').on('click', function(){
			var id = $(this).attr('data-id');
			
			printElement(id);
		});
		
		function printElement(id) {
			var divToPrint = document.getElementById(id);
			//console.log(domClone);
			var winPrint = window.open('', 'Print-Window');
			winPrint.document.open();
			winPrint.document.write('<html><body onload="window.print()">'+divToPrint.innerHTML+'</body></html>');
			winPrint.document.close();

			setTimeout(function(){winPrint.close();},10);
		}
	</script>
	@endpush
	
</div>
