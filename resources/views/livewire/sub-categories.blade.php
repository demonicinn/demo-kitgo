<div class="row">
	<div class="col-12">
		
		<div class="card">
			<div class="navbar navbar-expand navbar-white navbar-light">
				<div class="form-inline input-group-sm">
					Per Page: &nbsp;
					<select wire:model="perPage" class="form-control mr-2">
						<option>10</option>
						<option>15</option>
						<option>25</option>
					</select>					
					
					<div class="mb-2 mt-2 input-group input-group-sm">
						<input wire:model.debounce.1000ms="search" class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
						<div class="input-group-append">
							<button class="btn btn-navbar" type="submit">
								<i class="fas fa-search"></i>
							</button>
						</div>
					</div>					

				</div>
				
			</div>
			
			<div class="card-body table-responsive p-0">
                <table class="table table-hover text-nowrap">
					<thead>
						<tr>
							<th>
								<a wire:click.prevent="sortBy('name')" href="javascript:void(0)">Name
									@include('includes._sort-icon', ['field' => 'name'])
								</a>
							</th>							
							<th>
								<a wire:click.prevent="sortBy('category')" href="javascript:void(0)">Category
									@include('includes._sort-icon', ['field' => 'category'])
								</a>
							</th>
							<th>
								<a wire:click.prevent="sortBy('created_at')" href="javascript:void(0)">Created At
									@include('includes._sort-icon', ['field' => 'created_at'])
								</a>
							</th>
							<th>
								Action
							</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($items as $item)
						<tr>
							<td>{{ @$item->name }}</td>
							<td>{{ @$item->items->name }}</td>
							<td>{{ dateFormat($item->created_at) }}</td>
							<td>
								<div class="btn-icon-list">
									<a href="{{ route('subCategories.edit', $item->id) }}">
										<i class="fa fa-edit"></i>
									</a>
									<a wire:click="deleteShowModal({{ $item->id }})" data-toggle="modal" data-target="#modalDelete" href="javascript:void(0)">
										<i class="fa fa-trash"></i>
									</a>
								</div>
							</td>	
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
		
		<div class="pagination">
			{{ $items->links() }}
		</div>
		
	</div>
	
	
	
	<div wire:ignore.self class="modal fade" id="modalDelete" tabindex="-1" role="dialog" aria-labelledby="modalDelete" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">{{ __('Delete Sub Category') }}</h4>
				</div>
				<div class="modal-body">
					{{ __('Are you sure you want to delete this Sub Category?') }}
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancle</button>
					<button type="submit" class="btn btn-warning" wire:click="delete" wire:loading.attr="disabled">Confirm Delete</button>
				</div>
			</div>
		</div>
	</div>
	
	@push('scripts')
	<script>
		window.livewire.on('modalHideDelete', () => {
            $('#modalDelete').modal('hide');
        });
	</script>
	@endpush
	
</div>
