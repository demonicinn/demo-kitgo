<div>
    <div class="row">
		<div class="col-md-8">
			<div class="p-2">Attachments</div>			
			<div class="timeline">
				@foreach($project->projectAttachments as $attachment)
				<div>
					<span class="fas bg-blue">{{ userName($attachment->user->name) }}</span>
					<div class="timeline-item">
						<span class="time"><i class="fas fa-clock"></i> {{ dateTimeFormat($attachment->created_at) }}</span>
						<h3 class="timeline-header">
							<a href="{{ route('users.show', $attachment->user->id) }}">{{ @$attachment->user->name }}</a>
						</h3>
						<div class="timeline-body">
						<img width="150" src="{{ asset('storage/attachment/'.$attachment->attachment) }}"/>
						</div>
					</div>
				</div>
				@endforeach
			</div>			
		</div>
		
		
		<div class="col-md-4">
			<div class="p-2">Add New Attachment</div>
			
			<div class="form-group">
				<input wire:model="attachment" type="file"></textarea>
			</div>
			@if($attachment)
			<div class="form-group">
				<button type="button" class="btn btn-primary" wire:click="addAttachment" wire:loading.attr="disabled">Upload Attachment</button>
			</div>
			@endif
			
		</div>
	</div>
</div>
