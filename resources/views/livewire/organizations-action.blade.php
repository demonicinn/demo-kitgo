<div>
	
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2 pt-1">
				<div class="col-6">
					<ol class="breadcrumb">
						<li class="breadcrumb-item">
							<a href="{{ route('organizations') }}">Organizations</a>
						</li>
					</ol>
				</div>
				<div class="col-6">
					<ol class="breadcrumb float-right">
						<a href="javascript:void(0)" wire:click="createShowModal()" data-toggle="modal" data-target="#modalFormVisible" title="Create" class="btn btn-success btn-sm"><i class="fa fa-plus"></i> Create</a>
					</ol>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-12">
			
			<div class="card">
				<div class="navbar navbar-expand navbar-white navbar-light">
					<div class="form-inline input-group-sm">
						Per Page: &nbsp;
						<select wire:model="perPage" class="form-control mr-2">
							<option>10</option>
							<option>15</option>
							<option>25</option>
						</select>
						
						
						<div class="mb-2 mt-2 input-group input-group-sm">
							<input wire:model.debounce.1000ms="search" class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
							<div class="input-group-append">
								<button class="btn btn-navbar" type="submit">
									<i class="fas fa-search"></i>
								</button>
							</div>
						</div>
					</div>
				</div>
				
				<div class="card-body table-responsive p-0">
					<table class="table table-hover text-nowrap">
						<thead>
							<tr>
								<th>
									<a wire:click.prevent="sortBy('name')" href="javascript:void(0)">Name
										@include('includes._sort-icon', ['field' => 'name'])
									</a>
								</th>
								<th>
									<a wire:click.prevent="sortBy('address')" href="javascript:void(0)">Address
										@include('includes._sort-icon', ['field' => 'address'])
									</a>
								</th>
								<th>
									<a wire:click.prevent="sortBy('created_at')" href="javascript:void(0)">Created At
										@include('includes._sort-icon', ['field' => 'created_at'])
									</a>
								</th>
								<th>
									Action
								</th>
							</tr>
						</thead>
						<tbody>
							@foreach ($organizations as $organization)
							<tr>
								<td>{{ @$organization->name }}</td>
								<td>{{ @$organization->address }}</td>
								<td>{{ $organization->created_at ? $organization->created_at->format('d/m/Y') : '' }}</td>
								<td>
									<div class="btn-icon-list">
										<a wire:click="updateShowModal({{ $organization->id }})" data-toggle="modal" data-target="#modalFormVisible" href="javascript:void(0)">
											<i class="fa fa-edit"></i>
										</a>
										
										<a wire:click="deleteShowModal({{ $organization->id }})" data-toggle="modal" data-target="#modalDelete" href="javascript:void(0)">
											<i class="fa fa-trash"></i>
										</a>
									</div>
								</td>	
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
			
			<div class="pagination">
				{{ $organizations->links() }}
			</div>
			
		</div>
		
	</div>
	
	
	<div wire:ignore.self class="modal fade" id="modalFormVisible" tabindex="-1" role="dialog" aria-labelledby="modalFormVisible" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">{{ __('Create or Update Organization') }}</h4>
				</div>
				<div class="modal-body">
					<div class="form-group {!! ($errors->has('name') ? 'has-error' : '') !!}">
						{!! Form::label('name','Name', ['class' => 'control-label']) !!}
						<input type="text" wire:model="name" class="form-control">
						{!! $errors->first('name', '<span class="help-block">:message</span>') !!}
					</div>
					<div class="form-group {!! ($errors->has('address') ? 'has-error' : '') !!}">
						{!! Form::label('address','Address', ['class' => 'control-label']) !!}
						<input wire:ignore type="text" wire:model="address" class="form-control" id="autocomplete">
						{!! $errors->first('address', '<span class="help-block">:message</span>') !!}
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
					
					@if ($modelId)
						<button type="submit" class="btn btn-success" wire:click="update" wire:loading.attr="disabled">Update</button>
					@else
						<button type="submit" class="btn btn-success" wire:click="create" wire:loading.attr="disabled">Create</button>
					@endif
				</div>
			</div>
		</div>
	</div>
	
	
	<div wire:ignore.self class="modal fade" id="modalDelete" tabindex="-1" role="dialog" aria-labelledby="modalDelete" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">{{ __('Delete Organization') }}</h4>
				</div>
				<div class="modal-body">
					{{ __('Are you sure you want to delete this organization?') }}
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancle</button>
					<button type="submit" class="btn btn-warning" wire:click="delete" wire:loading.attr="disabled">Confirm Delete</button>
				</div>
			</div>
		</div>
	</div>
	
	@push('scripts')
	<style>
	.pac-container.pac-logo {
		z-index: 9999;
	}
	</style>
	<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key={{ config('services.google.key') }}&libraries=places&sensor=false"></script>
	<script>
		window.livewire.on('modalHide', () => {
            $('#modalFormVisible').modal('hide');
        });
		window.livewire.on('modalHideDelete', () => {
            $('#modalDelete').modal('hide');
        });
		
		
		google.maps.event.addDomListener(window, 'load', initialize);
	
		function initialize() {
			var input = document.getElementById('autocomplete');
			var options = {
				componentRestrictions: {country: 'uk'}//UK only
			};

			var autocomplete = new google.maps.places.Autocomplete(input, options);
			autocomplete.addListener('place_changed', function() {
				var place = autocomplete.getPlace();
				// console.log(place.geometry);
				
				var latitude = place.geometry['location'].lat();
				var longitude = place.geometry['location'].lng();
				var address = place.formatted_address;
				
				@this.set('latitude', latitude);
				@this.set('longitude', longitude);
				@this.set('address', address);
				
			});
		}
	</script>
	@endpush
</div>
