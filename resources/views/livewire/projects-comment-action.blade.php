<div>
    <div class="row">
		<div class="col-md-8">
			<div class="p-2">Comments</div>			
			<div class="timeline">
				@foreach($project->projectComments as $comment)
				<div>
					@if(@$comment->user->profile_photo_path)
					<img width="30" class="fas img-circle" src="{{ asset('storage/profile/'.$comment->user->profile_photo_path) }}" alt="{{ $comment->user->name }}">
					@else
					<span class="fas bg-blue">{{ userName($comment->user->name) }}</span>
					@endif
					
					<div class="timeline-item">
						<span class="time"><i class="fas fa-clock"></i> {{ dateTimeFormat($comment->created_at) }}</span>
						<h3 class="timeline-header">
							<a href="{{ route('users.show', $comment->user->id) }}">{{ $comment->user->name }}</a>
						</h3>
						<div class="timeline-body">
						{!! @$comment->comment !!}
						</div>
					</div>
				</div>
				@endforeach
			</div>
		</div>
		
		
		<div class="col-md-4">
			<div class="p-2">Add New Comments</div>
			
			<div class="form-group">
				<textarea wire:model="comment" class="form-control" rows="1" placeholder="Write a comment"></textarea>
			</div>
			@if($comment)
			<div class="form-group">
				<button type="button" class="btn btn-primary" wire:click="addComment" wire:loading.attr="disabled">Add Comment</button>
			</div>
			@endif
			
		</div>
	</div>
</div>
