<x-app-layout>
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2 pt-1">
				<div class="col-3">
					<ol class="breadcrumb">
						<li class="breadcrumb-item">
							<a href="{{ route('projects') }}">Projects</a>
						</li>
					</ol>
				</div>
				<div class="col-9">
					<ol class="breadcrumb float-right">
						@if(auth()->user()->role!='user')
						<a href="#" data-toggle="modal" data-target="#import" title="Import" class="btn btn-warning btn-sm mr-2"><i class="fa fa-plus"></i> Import CSV</a>
						<a href="{{ route('projects.export') }}" title="Export" class="btn btn-warning btn-sm mr-2"><i class="fa fa-plus"></i> Export CSV</a>
						@endif
						<a href="{{ route('projects.create') }}" title="Add" class="btn btn-success btn-sm"><i class="fa fa-plus"></i> Create</a>
					</ol>
				</div>
			</div>
		</div>
	</div>
	
    @livewire('projects-action')
	
	<div id="import" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">Import</h4>
				</div>
				{!! Form::open(['route' => 'projects.import', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data']) !!}
				<div class="modal-body">
					<div class="form-group {!! ($errors->has('file') ? 'has-error' : '') !!}">
						{!! Form::label('file','Select CSV file', ['class' => 'control-label']) !!}</br>
						<input name="file" type="file" accept=".csv">
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-success">Submit</button>
				</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
	
</x-app-layout>