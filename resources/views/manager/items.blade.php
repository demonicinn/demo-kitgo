<x-app-layout>
<div class="content-header">
	<div class="container-fluid">
		<div class="row mb-2 pt-1">
			<div class="col-6">
				<ol class="breadcrumb">
					<li class="breadcrumb-item">
						<a href="{{ route('items') }}">Categories</a>
					</li>
				</ol>
			</div>
			<div class="col-6">
				<ol class="breadcrumb float-right">
					<a href="{{ route('categories.create') }}" title="Add" class="btn btn-success btn-sm"><i class="fa fa-plus"></i> Create</a>
				</ol>
			</div>
		</div>
	</div>
</div>

    @livewire('items-action')
</x-app-layout>