<x-app-layout>
<div class="content-header">
	<div class="container-fluid">
		<div class="row mb-2 pt-1">
			<div class="col-2">
				<ol class="breadcrumb">
					<li class="breadcrumb-item">
						<a href="{{ route('sub-items') }}">Items</a>
					</li>
				</ol>
			</div>
			@if(auth()->user()->role!='user')
			<div class="col-10">
				<ol class="breadcrumb float-right">
					<a href="#" data-toggle="modal" data-target="#import" title="Add" class="btn btn-warning btn-sm mr-2"><i class="fa fa-upload"></i> Import CSV</a>
					<a href="{{ route('subitems.export') }}" title="Add" class="btn btn-warning btn-sm mr-2"><i class="fa fa-download"></i> Export CSV</a>
				
					<a href="{{ route('subitems.create') }}" title="Add" class="btn btn-success btn-sm"><i class="fa fa-plus"></i> Create</a>
				</ol>
			</div>
			@endif
		</div>
	</div>
</div>
	
    @livewire('subitems-action')	
	
	<div id="import" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">Import</h4>
				</div>
				{!! Form::open(['route' => 'subitems.import', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data']) !!}
				<div class="modal-body">
					<div class="form-group {!! ($errors->has('file') ? 'has-error' : '') !!}">
						{!! Form::label('file','Select CSV file', ['class' => 'control-label']) !!}</br>
						<input name="file" type="file" accept=".csv">
					</div>
					{{--
					<a href="{{ asset('files/subitems.csv') }}" target="_blank">Click here to Download Sample</a>
					--}}
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-success">Submit</button>
				</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</x-app-layout>