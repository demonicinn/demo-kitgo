<?php

namespace App\Console\Commands;

use App\Models\Project;
use App\Models\ProjectItem;
use App\Models\ProjectHistory;
use App\Models\SubItem;
use App\Models\SubitemBarcode;
use App\Models\ProjectBarcode;

use Illuminate\Console\Command;
use Mail;

class ReturnCron extends Command
{
	
	/**
	* The name and signature of the console command.
	*
	* @var string
	*/
	protected $signature = 'return:cron';

	/**
	* The console command description.
	*
	* @var string
	*/
	protected $description = 'Project Items Return';
	
	/**
	* Execute the console command.
	*
	* @return mixed
	*/
	public function handle(){
		
		$date = date('Y-m-d');
		
		
		//on job
		$onJobProjects = Project::whereDate('start_date', '<=', $date)->whereDate('end_date', '>=', $date)->get();
		if(@$onJobProjects){
			foreach($onJobProjects as $project){
				foreach($project->items as $item){
				
					if($item->status=='intransit'){
						$item->status = 'onjob';
						$item->save();
						
						$subitems = SubItem::find($item->item_id);
						$subitems->status = 'onjob';
						$subitems->save();
						
						$newMess = $subitems->make.' - '. $subitems->model .' On Job';
						//...
						$history = new ProjectHistory;
						$history->project_id = $project->id;
						$history->barcode = $subitems->barcode_no;
						$history->notificationtext = $newMess;
						$history->created_by = $project->created_by;
						$history->updated_by = $project->updated_by;
						$history->save();
					}
				}
				
				$project->project_status = 'onjob';
				$project->save();
			}
		}
		
		//in transit
		$projects = Project::whereDate('shipping_date', '<=', $date)->whereDate('start_date', '>=', $date)->get();
		if(@$projects){
			foreach($projects as $project){
				foreach($project->items as $item){
					if($item->status=='kit_packing'){
						$item->status = 'intransit';
						$item->save();
						
						$subitems = SubItem::find($item->item_id);
						$subitems->status = 'intransit';
						$subitems->save();
						
						$newMess = $subitems->make.' - '. $subitems->model .' In Transit';
						//...
						$history = new ProjectHistory;
						$history->project_id = $project->id;
						$history->barcode = $subitems->barcode_no;
						$history->notificationtext = $newMess;
						$history->created_by = $project->created_by;
						$history->updated_by = $project->updated_by;
						$history->save();
					}
				}
				
				$project->project_status = 'intransit';
				$project->save();
			}
		}
		
		
		//overduereturn
		$overduereturn = Project::whereDate('expected_return_date', '<', $date)->get();
		if(@$overduereturn){
			foreach($overduereturn as $project){
				foreach($project->items as $item){
					if($item->status != 'warehouse_return'){
						$item->status = 'overduereturn';
						$item->save();
						
						$subitems = SubItem::find($item->item_id);
						$subitems->status = 'overduereturn';
						$subitems->save();
						
						$newMess = $subitems->make.' - '. $subitems->model .' Overdue Returned';
						//...
						$history = new ProjectHistory;
						$history->project_id = $project->id;
						$history->barcode = $subitems->barcode_no;
						$history->notificationtext = $newMess;
						$history->created_by = $project->created_by;
						$history->updated_by = $project->updated_by;
						$history->save();
					}
				}
				
				$project->project_status = 'overduereturn';
				$project->save();
			}
		}
		
		
		
		
		
		
		echo 'Done';
		
	}
	
	
}