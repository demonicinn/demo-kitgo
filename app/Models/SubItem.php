<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Auth;
class Subitem extends Model
{
    use HasFactory;
	
    protected $fillable = [
        'pictureurl',
        'serialnumber',
        'quantity',
        'date_of_purchase',
        'warranty_expiry_period',
        'receipt_url',
        'condition',
        'status',
        'notes',
        'available_qty',
        'item_id',
        'organisation_id',
        'created_by',
        'updated_by'
    ];
    

    public static function getConditions(){
        return ['excellent'=>'Excellent','good'=>'Good','ok'=>'Ok','needrepair'=>'Need Repair','broken'=>'Broken'];
    }
    
    public static function getStatus(){
        return ['available'=>'Available','awaitingbooking'=>'Awaiting booking','onjob'=>'On Job','intransit'=>'In Transit'];
    }
    
    public static function getlinkedsubitems(){
        return Subitem::all()->where('organisation_id', '=', Auth::user()->organization_id);
    }
	
	public function subcategory() {
        return $this->belongsTo(SubCategories::class, 'item_id');
    }
	
	public function projectitem() {
        return $this->hasMany(ProjectItem::class, 'item_id')->where('status', 'reserved')->orderBy('id', 'desc');
    }
	
	public function projectitemCheckout() {
        return $this->hasMany(ProjectItem::class, 'item_id')->where('status', '!=', 'reserved')->orderBy('id', 'desc');
    }
	
	public function subitemBarcode() {
        return $this->hasMany(SubitemBarcode::class, 'sub_item_id')->orderBy('id', 'desc');
    }
	
	public function subitemBarcodeAvailable() {
        return $this->hasMany(SubitemBarcode::class, 'sub_item_id')->where('status', 'available')->orderBy('id', 'desc');
    }
	
	public function projectbarcode() {
        return $this->hasMany(ProjectBarcode::class, 'sub_item_id')->orderBy('id', 'desc');
    }
	
	public function subitemComments() {
        return $this->hasMany(SubitemComments::class, 'sub_item_id')->orderBy('id', 'desc');
    }
	
	public function subitemHistory() {
        return $this->hasMany(SubitemHistory::class, 'sub_item_id')->orderBy('id', 'desc');
    }
	
	public static function availableItems() {
		return Subitem::all()->where('organisation_id', '=', Auth::user()->organization_id)->where('status', 'available')->where('disposed', '0')->where('is_missed', '0');
    }
	
}
