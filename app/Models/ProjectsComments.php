<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProjectsComments extends Model
{
    use HasFactory;
	
	protected $fillable = [
        'project_id', 'comment', 'created_by', 'updated_by'
    ];
	
	public function user()
    {
        return $this->belongsTo(User::class, 'created_by');
    }
}
