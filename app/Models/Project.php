<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Auth;

class Project extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'pickup_date',
        'shipping_date',
        'start_date',
        'end_date',
        'expected_return_date',
        'status',
        'organisation_id',
        'created_by',
        'updated_by',
		'project_status'
    ];

    /**
     * @return relationship 
     */
    public function items()
    {
        return $this->morphMany(ProjectItem::class, 'itemable');
    }
	
	public function withSubitems()
    {
		return $this->belongsToMany(Subitem::class, ProjectItem::class, 'project_id', 'item_id')->withPivot('status');
	}
	
	public function projectitems()
    {
		return $this->hasMany(ProjectItem::class, 'project_id');
    }
	
	
    /**
     * static function for Modal popup 
     */
    public static function getItems()
    {
        return Item::all()->where('organisation_id', '=', Auth::user()->organization_id);
    }
	
	public static function getSubItems()
    {
		return Subitem::all()->where('organisation_id', '=', Auth::user()->organization_id);
    }
	
	public static function getUsers()
    {
		$organization_id = Auth::user()->organization_id;
		return User::whereJsonContains('assigned_organizations', ["{$organization_id}"])->get();
    }
	
	public function projectBarcodes()
    {
		return $this->hasMany(ProjectBarcode::class, 'project_id')->orderBy('id', 'desc');
    }
	
	public function user()
    {
        return $this->belongsTo(User::class, 'created_by');
    }
	
	public function projectComments()
    {
		return $this->hasMany(ProjectsComments::class, 'project_id')->orderBy('id', 'desc');
    }
	
	public function projectAttachments()
    {
		return $this->hasMany(ProjectsAttachments::class, 'project_id')->orderBy('id', 'desc');
    }
	
	
	public function projectHistory()
    {
		return $this->hasMany(ProjectHistory::class, 'project_id')->orderBy('id', 'desc');
    }
	
	
	public function projectUser()
    {
        return $this->belongsTo(User::class, 'user');
    }
	
	
	
}
