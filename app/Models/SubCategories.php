<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Auth;

class SubCategories extends Model
{
    use HasFactory;
	
	protected $fillable = [
        'category',
        'name',
        'linked_id',
        'organisation_id',
        'created_by',
        'updated_by',
    ];
	
	public function items() {
        return $this->belongsTo(Item::class, 'category');
    }
	
	public function subitems() {
        return $this->hasMany(Subitem::class, 'item_id')
			->where('organisation_id', '=', Auth::user()->organization_id);
    }
	
	public function subitemsAvailable() {
        return $this->hasMany(Subitem::class, 'item_id')
			->where('status', 'available')
			->where('is_missed', '0')
			->where('disposed', '0')
			->where('organisation_id', '=', Auth::user()->organization_id);
    }
	
	public function organisation(){
        return $this->belongsTo(Organization::class, 'organisation_id');
    }
}
