<?php

namespace App\Http\Livewire;

use Livewire\Component;

use App\Models\Project;
use App\Models\SubitemHistory;
use App\Models\ProjectItem;
use App\Models\Item;
use App\Models\Linkedsubitem;
use App\Models\Subitem;
use App\Models\ProjectHistory;

class ProjectsCreate extends Component
{
	public $title;
	public $project_id;
	public $client_name;
	public $client_contact_number;
	public $location;
    public $shipping_date = "";
    public $pickup_date = "";
    public $start_date = "";
    public $end_date = "";
    public $expected_return_date = "";
	public $stockProducts = [];
	public $user = "";	
	public $projectitems = [];
	public $getsubitems = [];	
	
	
	
	public function mount() {
		if(request()->segment(3)=='edit'){
			$id = request()->segment(2);
			$data = Project::find($id);
			
			$this->project_id = $id;
			$this->title = $data->title;
			$this->client_name = $data->client_name;
			$this->client_contact_number = $data->client_contact_number;
			$this->location = $data->location;
			$this->pickup_date = $data->pickup_date;
			$this->shipping_date = $data->shipping_date;
			$this->start_date = $data->start_date;
			$this->end_date = $data->end_date;
			$this->expected_return_date = $data->expected_return_date;
			$this->user = $data->user;
			
			//..
			$projectitem = ProjectItem::where('project_id', $id)->get();
			$this->projectitems = $projectitem;
			
			// if($projectitem){
				// foreach($projectitem as $k => $item){
					// if($k==0){
						// $this->stockProducts = [
							// ['id' => $item->item_id, 'quantity' => 1, 'avl_quantity' => '', 'is_delete' => 'no']
						// ];
					// }
					// if($k>0){
						// $this->stockProducts[] = ['id' => $item->item_id, 'quantity' => 1, 'avl_quantity' => '', 'is_delete' => 'no'];
					// }
				// }
			// }
		}
		else {
			// $this->stockProducts = [
				// ['id' => '', 'quantity' => 1, 'avl_quantity' => '', 'is_delete' => 'no']
			// ];		
		}
		
		$this->getsubitems = Subitem::availableItems();
		
    }
	
	public function checkItems(){
		$date = $this->pickup_date;
		$subitems = Subitem::availableItems();
		$newArray = array();
		//...
		$projects = Project::whereDate('expected_return_date', '<=', $date)
			->where('organisation_id', auth()->user()->organization_id)
			->where('status', '1')
			//->where('project_status', '!=', 'warehouse_return')
			->get();
			//dd($projects);
		foreach($projects as $project){
			foreach($project->projectitems as $item){
				
				$subitm = $item->subitem;
				if($item->status!='warehouse_return' && $subitm->is_missed==0 && $subitm->disposed==0){
					$newArray[] = $item->subitem;
				}
			}
		}
		//dd($newArray);
		$merged = $subitems->merge($newArray);
		$this->getsubitems = $merged;		
	}
	
	/**
     * Add product Item. 
     * @return object
     */
    public function addProduct()
    {
        $this->stockProducts[] = ['id' => '', 'quantity' => 1, 'avl_quantity' => '', 'is_delete' => 'no'];
		//dd($this->stockProducts);
    }
	
	/**
     * check product linked. 
     * @return object
     */
	public function checkProduct($index)
    {
		$product = $this->stockProducts[$index];
		$subitem = Subitem::find($product['id']);
		$this->stockProducts[$index] = ['id' =>$product['id'], 'quantity' => 1, 'avl_quantity' => '', 'is_delete' => 'no'];
		
        $linkeditem = Linkedsubitem::where('sub_item_id', $product['id'])->get();
		
		foreach($linkeditem as $linked){
			$this->stockProducts[] = ['id' => $linked->linked_sub_item_id, 'quantity' => 1, 'avl_quantity' => '', 'is_delete' => 'no'];
		}
	}
	
	public function removeProduct($index)
    {
        unset($this->stockProducts[$index]);
        $this->stockProducts = array_values($this->stockProducts);
    }
	
	
	
	public function rules()
    {
		return [
            'title' =>'required',
			'client_name' =>'required',
			'client_contact_number' =>'required',
			'location' =>'required',
			'user' =>'required',
            'pickup_date' =>'required|date',
            'shipping_date' => 'required|date|after_or_equal:pickup_date',
            'start_date' => 'required|date|after_or_equal:shipping_date',
            'end_date' => 'required|date|after_or_equal:start_date',
            'expected_return_date' => 'required|date|after_or_equal:end_date',
        ];
    }
	
	public function create()
    {
        $this->validate();
       
		$store = new Project;
		$store->title = $this->title;
		$store->client_name = $this->client_name;
		$store->client_contact_number = $this->client_contact_number;
		$store->location = $this->location;
		$store->pickup_date = $this->pickup_date;
		$store->shipping_date = $this->shipping_date;
		$store->start_date = $this->start_date;
		$store->end_date = $this->end_date;
		$store->expected_return_date = $this->expected_return_date;
		$store->created_by = auth()->user()->id;
		$store->updated_by = auth()->user()->id;
		$store->organisation_id = auth()->user()->organization_id;
		$store->user = $this->user;
		$store->status = auth()->user()->role == 'user' ? '2' : '1';
		$store->project_status = 'reserved';
		$store->save();
		
		$phis = new ProjectHistory;
		$phis->project_id = $store->id;
		$phis->created_by = auth()->user()->id;
		$phis->updated_by = auth()->user()->id;
		$phis->notificationtext = 'Project Created';
		$phis->save();
		
		//...
		$perform = Project::find($store->id);
		
		if(@$this->stockProducts){
			foreach($this->stockProducts as $item){
				if(@$item){
				//---
				Subitem::find($item['id'])->update(['status'=>'reserved']);
				
				//..
				$perform->items()->create([
					'quantity' =>  '1',
					'item_id'  => $item['id'],
					'user_id'  => auth()->user()->id,
					'project_id' => $perform->id,
					'status' => 'reserved'
				])->user()->associate(auth()->user()->id)->save();
				
				$his = new SubitemHistory;
				$his->sub_item_id = $item['id'];
				$his->created_by = auth()->user()->id;
				$his->updated_by = auth()->user()->id;
				$his->notificationtext = 'Item Reserved';
				$his->save();
				}
			}
		}
		
		request()->session()->flash('success', "Project Created Successfully");
		return redirect()->route("projects");
    }
	
	public function update()
	{	
		$this->validate();
		$project = Project::find($this->project_id);
		$project->title = $this->title;
		$project->client_name = $this->client_name;
		$project->client_contact_number = $this->client_contact_number;
		$project->location = $this->location;
		$project->pickup_date = $this->pickup_date;
		$project->shipping_date = $this->shipping_date;
		$project->start_date = $this->start_date;
		$project->end_date = $this->end_date;
		$project->expected_return_date = $this->expected_return_date;
		$project->created_by = auth()->user()->id;
		$project->updated_by = auth()->user()->id;
		$project->organisation_id = auth()->user()->organization_id;
		$project->user = $this->user;
		$project->save();
		
		//...
		$perform = Project::find($this->project_id);
		
		$phis = new ProjectHistory;
		$phis->project_id = $perform->id;
		$phis->created_by = auth()->user()->id;
		$phis->updated_by = auth()->user()->id;
		$phis->notificationtext = 'Project updated';
		$phis->save();
		
		if(@$this->stockProducts){
			foreach($this->stockProducts as $item){
				$projectitem = ProjectItem::where('project_id', $this->project_id)
					->where('item_id', $item['id'])
					->first();
					
				if(@$projectitem){
					$projectitem->quantity = '1';
					$projectitem->save();
				}
				else {
					Subitem::find($item['id'])->update(['status'=>'reserved']);
					$perform->items()->create([
						'quantity' =>  '1',
						'item_id'  => $item['id'],
						'user_id'  => auth()->user()->id,
						'project_id' => $perform->id,
						'status' => 'reserved'
					])->user()->associate(auth()->user()->id)->save();
				}
			}
		}
		
		request()->session()->flash('success', "Project Updated Successfully");
		return redirect()->route("projects.show", $this->project_id);
	}
	
	
	
    public function render()
    {
        return view('livewire.projects-create');
    }
}
