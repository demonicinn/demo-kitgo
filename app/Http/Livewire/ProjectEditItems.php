<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Project;
use App\Models\ProjectItem;

class ProjectEditItems extends Component
{
	public $project = [];
	
	public function mount(){
		$this->project = Project::find(request()->segment('2'));
	}
	
	public function removeItem($id){
		//$item = ProjectItem::find($id);
		
		dd($id);
		
	}
	
    public function render()
    {
        return view('livewire.project-edit-items');
    }
	
	
}
