<?php

namespace App\Http\Livewire;

use Livewire\Component;

use Livewire\WithPagination;
use Auth;

use App\Models\Project;
use App\Models\ProjectItem;
use App\Models\Item;
use App\Models\Linkeditem;
use App\Models\Subitem;
use App\Models\ProjectHistory;

class ProjectsAction extends Component
{
	use WithPagination;
	
	public $perPage = 10;
    public $sortField='id';
    public $sortAsc = false;
    public $search = '';
    public $search_type = '';
	public $view = 'list';
	
	public function sortBy($field)
    {
        if ($this->sortField === $field) {
            $this->sortAsc = ! $this->sortAsc;
        } else {
            $this->sortAsc = true;
			}

        $this->sortField = $field;
    }

	public function selectView($view){
		$this->view = $view;
	}

    public function render()
    {
		$search = $this->search;
		$search_type = $this->search_type;
		
		$user = auth()->user();
		$organization_id = $user->organization_id;
		$organization = json_decode($user->assigned_organizations);
		
		$project = Project::with('items')
			->where(function ($query) use ($search) {
				$query->where('title', 'like', '%'.$search.'%')
				->orWhere('pickup_date', 'like', '%'.$search.'%')
				->orWhere('shipping_date', 'like', '%'.$search.'%')
				->orWhere('start_date', 'like', '%'.$search.'%')
				->orWhere('end_date', 'like', '%'.$search.'%')
				->orWhere('expected_return_date', 'like', '%'.$search.'%')
				->orWhere('status', 'like', '%'.$search.'%');
			})
			->where(function ($query) use ($search_type) {
				if($search_type){
					$date = date('Y-m-d');
					if($search_type=='future'){
						$query->whereDate('pickup_date', '>', $date);
					}
					if($search_type=='live'){
						$query->whereDate('pickup_date', '<=', $date);
						$query->whereDate('expected_return_date', '>=', $date);
					}
					if($search_type=='archived'){
						$query->whereDate('expected_return_date', '<', $date);
					}
				};
			});
			
			$project = $project->where(function ($query) use ($organization_id, $organization) {
				if($organization_id){
					$query->where('organisation_id', $organization_id);
				}
				else {
					if(@$organization){
						$query->whereIn('organisation_id', $organization);
					}
				}
			});
			
			$project = $project->orderBy($this->sortField, $this->sortAsc ? 'asc' : 'desc')
			->paginate($this->perPage);
		
		
        return view('livewire.projects-action', [
            'projects' => $project,
        ]);
    }
	
}
