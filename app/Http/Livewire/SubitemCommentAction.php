<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Subitem;
use App\Models\SubitemComments;
use App\Models\SubitemHistory;

class SubitemCommentAction extends Component
{
	public $comment;
	public $item;

	public function mount() {
		$this->item = Subitem::find(request()->segment(2));
    }
	
	public function rules()
    {
        return [
            'comment' =>'required',
        ];
    }
	
	/**
     * Add comment. 
     * @return object
     */
    public function addComment()
    {
		$this->validate();
        $store = new SubitemComments;
		$store->sub_item_id = $this->item->id;
		$store->comment = $this->comment;
		$store->created_by = auth()->user()->id;
		$store->updated_by = auth()->user()->id;
		$store->save();
		
		$his = new SubitemHistory;
		$his->sub_item_id = $this->item->id;
		$his->created_by = auth()->user()->id;
		$his->updated_by = auth()->user()->id;
		$his->type = '1';
		$his->linkto = $store->id;
		$his->notificationtext = auth()->user()->name.' added new comment';
		$his->save();
		
		//...
        redirect()->to('/items/'.$this->item->id.'?tab=comments');	
    }
	
    public function render()
    {
        return view('livewire.subitem-comment-action');
    }
}
