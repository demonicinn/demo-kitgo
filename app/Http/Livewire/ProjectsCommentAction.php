<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Project;
use App\Models\ProjectsComments;
use App\Models\ProjectHistory;

class ProjectsCommentAction extends Component
{
	public $comment;
	public $project;
	
	
	public function mount() {
		$this->project = Project::find(request()->segment(2));
    }
	
	public function rules()
    {
        return [
            'comment' =>'required',
        ];
    }
	
	/**
     * Add comment. 
     * @return object
     */
    public function addComment()
    {
		$this->validate();
        $store = new ProjectsComments;
		$store->project_id = $this->project->id;
		$store->comment = $this->comment;
		$store->created_by = auth()->user()->id;
		$store->updated_by = auth()->user()->id;
		$store->save();
		
		//...
		$phis = new ProjectHistory;
		$phis->project_id = $this->project->id;
		$phis->created_by = auth()->user()->id;
		$phis->updated_by = auth()->user()->id;
		$phis->type = '1';
		$phis->linkto = $store->id;
		$phis->notificationtext = auth()->user()->name.' added new comment';
		$phis->save();
		
		//...
        redirect()->to('/projects/'.$this->project->id.'?tab=comments');
    }
	
    public function render()
    {
        return view('livewire.projects-comment-action');
    }
}
