<?php

namespace App\Http\Livewire;

use App\Models\SubCategories as SubCategoriesModel;
use Auth;

use Livewire\Component;
use Livewire\WithPagination;

class SubCategories extends Component
{
	use WithPagination;
	
	public $perPage = 10;
    public $sortField='name';
    public $sortAsc = true;
    public $search = '';
	
	public $modalConfirmDeleteVisible;
    public $modelId;
	
	
	//...
	public function sortBy($field)
    {
        if ($this->sortField === $field) {
            $this->sortAsc = ! $this->sortAsc;
        } else {
            $this->sortAsc = true;
        }

        $this->sortField = $field;
    }
	
	public function deleteShowModal($id) {
        $this->modelId = $id;
    }
	
	public function delete() {
        SubCategoriesModel::destroy($this->modelId);
        $this->modalConfirmDeleteVisible = false;
        $this->resetPage();
        redirect()->to('/sub-categories');
    }
	
    public function render()
    {
		$search = $this->search;
		
		$user = auth()->user();
		$organization_id = $user->organization_id;
		$organization = json_decode($user->assigned_organizations);
		
		$items = SubCategoriesModel::select('sub_categories.*')
			->leftjoin('items', 'items.id', '=', 'sub_categories.category')
			->where(function ($query) use ($search) {
				$query->where('sub_categories.name', 'like', '%'.$search.'%');
				$query->orWhere('items.name', 'like', '%'.$search.'%');
			})
			->where(function ($query) use ($organization_id, $organization) {
				if($organization_id){
					$query->where('items.organisation_id', $organization_id);
				}
				// else {
					// if(@$organization && auth()->user()->role!='superadmin'){
						// $query->whereIn('sub_categories.organisation_id', $organization);
					// }
				// }
			})
			->orderBy($this->sortField, $this->sortAsc ? 'asc' : 'desc')
			->paginate($this->perPage);
			
        return view('livewire.sub-categories', [
            'items' => $items,
        ]);
    }
}
