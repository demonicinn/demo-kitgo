<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Project;

use App\Models\ProjectHistory;
use App\Models\projectItem;
use App\Models\SubitemBarcode;
use App\Models\Subitem;

class ProjectCheckout extends Component
{
	public $checkout = [];
	public $projects;
	
	
	public function mount() {
		$projectId = request()->segment(2);
		$this->projects = Project::find($projectId);
	}
	
	public function itemMissed(){
		if(@$this->checkout){
			$newMess = '';
			$message = '';
			
			foreach($this->checkout as $checkout){
				$message = 'Item Missed';
				
				$subitem = Subitem::find($checkout);
				$projectItem = ProjectItem::where('project_id', $this->projects->id)
				->where('item_id', $subitem->id)->first();				
				
				$projectItem->status = 'item_missed';
				//..
				$subitem->status = 'item_missed';
				$subitem->is_missed = '1';
				
				$projectItem->save();
				//...subitem
				$subitem->save();
			
				$newMess = $subitem->make.' - '. $subitem->model .' '.$message;
				//...
				$history = new ProjectHistory;
				$history->project_id = $this->projects->id;
				$history->barcode = $subitem->barcode_no;
				$history->notificationtext = $newMess;
				$history->created_by = auth()->user()->id;
				$history->updated_by = auth()->user()->id;
				$history->save();
			}
			
			if($newMess){
				request()->session()->flash('success', $newMess);
			}
			return redirect()->route("projects.show", $this->projects->id);
		}
	}
	
	
	
	public function checkout(){
		
		if(@$this->checkout){
			$newMess = '';
			$message = '';
			
			foreach($this->checkout as $check){				
				$projectItem = ProjectItem::find($check);
				if(@$projectItem){
					$subitem = Subitem::find($projectItem->item_id);
				
					$message = 'Kit Packed';
					$status = $projectItem->status;
					
					if(@$status=='reserved'){
						$projectItem->status = 'kit_packing';
						//..
						$subitem->status = 'kit_packing';
						Project::find($this->projects->id)->update(['project_status'=>'kit_packing']);
					}
					
					if(@$status=='kit_packing' || $status=='intransit' || $status=='onjob'){
						$message = 'On Location Kit Packed';
						$projectItem->status = 'on_location_kit_pack';
						
						//..
						$subitem->status = 'on_location_kit_pack';
						Project::find($this->projects->id)->update(['project_status'=>'on_location_kit_pack']);
					}
					
					if(@$status=='on_location_kit_pack'){
						$message = 'Warehouse Returned';
						$projectItem->status = 'warehouse_return';
						
						//..
						$subitem->status = 'available';
						Project::find($this->projects->id)->update(['project_status'=>'warehouse_return']);
					}
					
					$projectItem->save();
					//...subitem
					$subitem->save();
				
					$newMess = $subitem->make.' - '. $subitem->model .' has been '.$message;
					//...
					$history = new ProjectHistory;
					$history->project_id = $this->projects->id;
					$history->barcode = $subitem->barcode_no;
					$history->notificationtext = $newMess;
					$history->created_by = auth()->user()->id;
					$history->updated_by = auth()->user()->id;
					$history->save();
				}
			}
			
			if($newMess){
				request()->session()->flash('success', $newMess);
			}
			return redirect()->route("projects.show", $this->projects->id);
		}
	}
	
    public function render()
    {
        return view('livewire.project-checkout');
    }
}
