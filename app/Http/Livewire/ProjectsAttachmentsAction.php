<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Project;
use App\Models\ProjectsAttachments;
use App\Models\ProjectHistory;

use Livewire\WithFileUploads;

class ProjectsAttachmentsAction extends Component
{
	use WithFileUploads;
	public $attachment;
	public $project;
	
	public function mount() {
		$this->project = Project::find(request()->segment(2));
    }
	
	public function rules()
    {
        return [
            'attachment' =>'required|image',
        ];
    }
	
	/**
     * Add attachment. 
     * @return object
     */
    public function addAttachment()
    {
		$this->validate();
		
		if ($this->attachment) {
            $picture =time() . rand().'.png';
            $this->attachment->storeAs('public/attachment', $picture);
        }
		
        $store = new ProjectsAttachments;
		$store->project_id = $this->project->id;
		$store->attachment = $picture;
		$store->created_by = auth()->user()->id;
		$store->updated_by = auth()->user()->id;
		$store->save();
		
		//...
		$phis = new ProjectHistory;
		$phis->project_id = $this->project->id;
		$phis->created_by = auth()->user()->id;
		$phis->updated_by = auth()->user()->id;
		$phis->type = '2';
		$phis->linkto = $store->id;
		$phis->notificationtext = auth()->user()->name.' added new attachment';
		$phis->save();
		
		//...
        redirect()->to('/projects/'.$this->project->id.'?tab=attachments');
    }
	
	
    public function render()
    {
        return view('livewire.projects-attachments-action');
    }
}
