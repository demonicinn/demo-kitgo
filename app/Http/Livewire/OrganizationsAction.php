<?php

namespace App\Http\Livewire;

use Livewire\Component;

use App\Models\Organization;

use Livewire\WithPagination;
use Auth;

class OrganizationsAction extends Component
{
	use WithPagination;
	
	public $perPage = 10;
    public $sortField='name';
    public $sortAsc = true;
    public $search = '';
	public $model = Organization::class;
	
	
	public $modalFormVisible;
    public $modalConfirmDeleteVisible;
    public $modelId;
    
    public $name;
    public $address;
    public $latitude;
    public $longitude;
	
	public function mount() {
		if(auth()->user()->role=='manager' || auth()->user()->role=='user'){
			request()->session()->flash('error', "Permission Denied");
			return redirect()->route('dashboard');
		}
	}
	
	
	/**
     * The data for the model mapped
     * in this component.
     *
     * @return void
     */
    public function modelData()
    {
        return [          
            'name' => $this->name,
            'address' => $this->address,
            'latitude' => $this->latitude,
            'longitude' => $this->longitude,
			'created_by' => auth()->user()->id,
			'updated_by' => auth()->user()->id
        ];
    }
    
    /**
     * The validation rules
     *
     * @return void
     */
    public function rules()
    {
        if($this->modelId){
			return [
				'name' => 'required|unique:organizations,name,'.$this->modelId,
				'address' => 'required'
			];
		}
		return [
            'name' => 'required|unique:organizations,name',
            'address' => 'required'
        ];
    }
	
	/**
	* The create function.
     *
     * @return void
     */
    public function create()
    {
        $this->validate();
        Organization::create($this->modelData());
        //$this->modalFormVisible = false;
        redirect()->to('/organizations');
    }

    /**
     * Shows the create modal
     *
     * @return void
     */
    public function createShowModal()
    {
		$this->modelId = '';
        $this->resetValidation();
        $this->reset();
        $this->modalFormVisible = true;
    }
	
	/**
     * Shows the delete confirmation modal.
     *
     * @param  mixed $id
     * @return void
     */
    public function deleteShowModal($id)
    {
        $this->modelId = $id;
    }   

    /**
     * The delete function.
     *
     * @return void
     */
    public function delete()
    {
        Organization::destroy($this->modelId);
        $this->resetPage();
		$this->reset();
		$this->emit('modalHideDelete');
    }
	
	
	/**
     * Loads the model data
     * of this component.
     *
     * @return void
     */
    public function loadModel()
    {
		$this->resetPage();
        $data = Organization::find($this->modelId);
        // Assign the variables here
        $this->name = $data->name;
        $this->address = $data->address;
    }

    /**
     * The update function
     *
     * @return void
     */
    public function update()
    {
        $this->validate();
        Organization::find($this->modelId)->update($this->modelData());
		$this->reset();
        $this->emit('modalHide');
    }
	
	/**
     * Shows the form modal
     * in update mode.
     *
     * @param  mixed $id
     * @return void
     */
    public function updateShowModal($id)
    {
		$this->resetValidation();
		$this->reset();
        $this->modalFormVisible = true;
        $this->modelId = $id;
        $this->loadModel();
    }
	
	public function sortBy($field)
    {
        if ($this->sortField === $field) {
            $this->sortAsc = ! $this->sortAsc;
        } else {
            $this->sortAsc = true;
        }

        $this->sortField = $field;
    }
	

    public function render()
    {
		$search = $this->search;
        return view('livewire.organizations-action', [
            'organizations' => Organization::where(function ($query) use ($search) {
					$query->where('name', 'like', '%'.$search.'%')
					->orWhere('address', 'like', '%'.$search.'%');
				})
                ->orderBy($this->sortField, $this->sortAsc ? 'asc' : 'desc')
                ->paginate($this->perPage),
        ]);
    }
}
