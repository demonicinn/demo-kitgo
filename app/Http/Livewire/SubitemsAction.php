<?php

namespace App\Http\Livewire;

use App\Models\Item;
use App\Models\Subitem;
use App\Models\Linkedsubitem;
use App\Models\SubitemBarcode;
use App\Models\SubitemHistory;
use App\Models\SubCategories;
use Auth;

use Livewire\Component;
use Livewire\WithPagination;

use Intervention\Image\Facades\Image as Image;
use Milon\Barcode\DNS1D;
use Livewire\WithFileUploads;

class SubitemsAction extends Component
{
	use WithPagination;
	use WithFileUploads;
	
	public $view = 'list';
	public $perPage = 10;
    public $sortField='make';
    public $sortAsc = true;
    public $search = '';
    public $condition = '';
    public $category = '';
    public $disposed = false;
	
    public $modelId = '';
    public $oldOrganization = '';
    public $organizationId = '';
    public $subcategory = '';
    public $subcategoryList = [];
    public $items = [];
    public $modalFormVisible = false;
	
	
	public function mount()
    {
        if(@request()->condition){
			$this->condition = request()->condition;
		}
		if(@request()->disposed=='yes'){
			$this->disposed = true;
		}
		
		$user = auth()->user();
		$organization = json_decode($user->assigned_organizations);
		
        $this->items = Item::where(function ($query) use ($organization){
				if(@$organization && auth()->user()->role!='superadmin'){
					$query->whereIn('organisation_id', $organization);
				}
			})
			->get();
    }
	
	public function getSubCategories(){
		$this->subcategoryList = SubCategories::where('category', $this->category)->get();
	}
	
	
	public function organizationModal($id)
    {
		$subitem = Subitem::find($id);
		
		$this->modelId = $id;
		$this->oldOrganization = $subitem->organisation_id;
        $this->modalFormVisible = true;
    }
	
	public function changeOrganization(){
		$subitem = Subitem::find($this->modelId);
		$subitem->organisation_id = $this->organizationId;
		$subitem->save();
		
		request()->session()->flash('success', 'Item Organization Chnaged');
		redirect()->to('/items');
	}

	public function selectView($view){
		$this->view = $view;
	}

	public function sortBy($field)
    {
        if ($this->sortField === $field) {
            $this->sortAsc = ! $this->sortAsc;
        } else {
            $this->sortAsc = true;
        }
        $this->sortField = $field;
    }

    public function render()
    {
		$search = $this->search;
		$condition = $this->condition;
		$subcategory = $this->subcategory;
		$disposed = $this->disposed ? '1' : '0';
		
		$user = auth()->user();
		$organization_id = $user->organization_id;
		$organization = json_decode($user->assigned_organizations);
		
		$subitem = Subitem::select('subitems.*');
			$subitem = $subitem->leftjoin('sub_categories', 'sub_categories.id', '=', 'subitems.item_id');			
			
			$subitem = $subitem->where(function ($query) use ($organization_id, $organization) {
				if($organization_id){
					$query->where('subitems.organisation_id', $organization_id);
				}
				else {
					if(@$organization){
						$query->whereIn('subitems.organisation_id', $organization);
					}
				}
			});
			
			$subitem = $subitem->where(function ($query) use ($search) {
				$query->where('subitems.serialnumber', 'like', '%'.$search.'%')
				->orWhere('subitems.make', 'like', '%'.$search.'%')
				->orWhere('subitems.model', 'like', '%'.$search.'%')
				->orWhere('subitems.barcode_no', 'like', '%'.$search.'%')
				->orWhere('subitems.condition', 'like', '%'.$search.'%')
				->orWhere('sub_categories.name', 'like', '%'.$search.'%');
			});
			
			if($condition){
				$subitem = $subitem->where('subitems.condition', $condition);
			}
			
			if($subcategory){
				$subitem = $subitem->where('subitems.item_id', $subcategory);
			}
			
			$subitem = $subitem->where(function ($query) use ($disposed) {
				if($disposed=='1'){
					$query->where('subitems.disposed', $disposed)
					->orWhere('subitems.is_missed', $disposed);
				}
				else {
					$query->where('subitems.disposed', $disposed)
					->where('subitems.is_missed', $disposed);
				}
			});
			
			$subitem = $subitem->orderBy('subitems.'.$this->sortField, $this->sortAsc ? 'asc' : 'desc')
			->paginate($this->perPage);
		
        return view('livewire.subitems-action', [
            'subitems' => $subitem,
        ]);
    }
}
