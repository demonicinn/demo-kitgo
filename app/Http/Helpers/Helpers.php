<?php

	function projectbarcodeCount($subitem, $project){
		return App\Models\ProjectBarcode::where('sub_item_id', $subitem)
			->where('project_id', $project)
			->count();
	}
	
	function userName($name){
		$firstTwoCharacters = substr($name, 0, 2);
		return strtoupper($firstTwoCharacters);
	}
	
	function availableItems($item_id){
		return App\Models\Subitem::where('item_id', $item_id)
			->where('status', 'available')
			->where('is_missed', '0')
			->where('disposed', '0')
			->where('organisation_id', '=', auth()->user()->organization_id)
			->count();
	}
	
	function itemsStatus($status){
		if($status=='awaitingbooking'){
			return 'Awaiting Booking';
		}
		
		return 'available';
	}
	
	function countSubItems($id){
		$user = auth()->user();
		$organization_id = $user->organization_id;
		$date = date('Y-m-d');
		
		$organization = json_decode($user->assigned_organizations);
		
		return App\Models\SubItem::where('item_id', $id)
			->where(function ($query) use ($organization_id, $organization) {
				if($organization_id){
					$query->where('organisation_id', $organization_id);
				}
				else {
					if(@$organization){
						$query->whereIn('organisation_id', $organization);
					}
				}
			})
			->count();
	}
	
	function subcategory($id){
		$cat = App\Models\SubCategories::where('id', $id)->first();
		return $cat->name;
	}
	
	function organizationName($organization){
		$data = App\Models\Organization::find($organization);
		return $data->name;
	}
	
	function uniqueNumber(){
		$permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyz'.time();
		return substr(str_shuffle($permitted_chars), 0, 8);
	}
	
	function subItemStatus($status){
		switch($status){
			case 'available':
				return 'Available';
			break;
			case 'reserved':
				return 'Reserved';
			break;
			case 'awaitingbooking':
				return 'Awaiting Pickup';
			break;
			case 'awatingsignoff':
				return 'Awating Sign Off';
			break;
			case 'awaitingcollection':
				return 'Awaiting Collection';
			break;
			case 'overduereturn':
				return 'Overdue Returned';
			break;
			case 'itemmissing':
				return 'Item Missing';
			break;
			case 'itemdamaged':
				return 'Item Damaged';
			break;
			case 'on_location_kit_pack':
				return 'On Location Kit Packed';
			break;
			case 'warehouse_return':
				return 'Warehouse Returned';
			break;
			case 'new':
				return 'Upcoming';
			break;
			case 'kit_packing':
				return 'Kit Packed';
			break;
			case 'intransit':
				return 'In Transit';
			break;
			case 'onjob':
				return 'On Job';
			break;
			case 'item_missed':
				return 'Item Missing';
			break;
			case 'needrepair':
				return 'Need Repair';
			break;
			case 'broken':
				return 'Broken';
			break;
			
			default:
			return 'Available';
		}
	}
	
	function subItemColor($status){
		switch($status){
			case 'available':
				return 'success';
				break;
			case 'new':
				return 'success';
			break;
			case 'reserved':
				return 'warning';
			break;
			case 'awaitingbooking':
				return 'warning';
			break;
			case 'awatingsignoff':
				return 'warning';
			break;
			case 'awaitingcollection':
				return 'warning';
			break;
			case 'overduereturn':
				return 'danger';
			break;
			case 'itemmissing':
				return 'danger';
			break;
			case 'itemdamaged':
				return 'danger';
			break;			
			case 'on_location_kit_pack':
				return 'warning';
			break;
			case 'warehouse_return':
				return 'warning';
			break;
			case 'kit_packing':
				return 'warning';
			break;
			case 'intransit':
				return 'warning';
			break;
			case 'onjob':
				return 'warning';
			break;
			case 'item_missed':
				return 'danger';
			break;
			case 'needrepair':
				return 'danger';
			break;
			case 'broken':
				return 'danger';
			break;
			
			default:
			return 'success';
		}
	}
	
	
	function subItemColorCode($status){
		switch($status){
			case 'available':
				return '#28a745';
			break;
			case 'new':
				return '#28a745';
			break;
			case 'reserved':
				return '#4f4f4f';
			break;
			case 'awaitingbooking':
				return '#4f4f4f';
			break;
			case 'awatingsignoff':
				return '#4f4f4f';
			break;
			case 'awaitingcollection':
				return '#4f4f4f';
			break;
			case 'overduereturn':
				return '#dc3545';
			break;
			case 'itemmissing':
				return '#dc3545';
			break;
			case 'itemdamaged':
				return '#dc3545';
			break;			
			case 'on_location_kit_pack':
				return '#4f4f4f';
			break;
			case 'warehouse_return':
				return '#4f4f4f';
			break;
			case 'kit_packing':
				return '#4f4f4f';
			break;
			case 'intransit':
				return '#4f4f4f';
			break;
			case 'onjob':
				return '#4f4f4f';
			break;
			case 'item_missed':
				return '#dc3545';
			break;
			case 'needrepair':
				return '#dc3545';
			break;
			case 'broken':
				return '#dc3545';
			break;
			
			default:
			return '#28a745';
		}
	}
	
	
	function dateFormat($date){
		return \Carbon\Carbon::parse($date)->format('M j, Y');
	}
	
	function dateTimeFormat($date){
		return \Carbon\Carbon::parse($date)->format('M j, Y, g:i a');
	}
	
	function getOrg(){
		return auth()->user()->organization_id;
		return \Cookie::get('organizations');
	}
	
	function getProjectComment($linkto){
		$data = App\Models\ProjectsComments::find($linkto);
		if(@$data){
			return $data->comment;
		}
		return '';
	}
	
	function getItemsComment($linkto){
		$data = App\Models\SubitemComments::find($linkto);
		if(@$data){
			return $data->comment;
		}
		return '';
	}
	