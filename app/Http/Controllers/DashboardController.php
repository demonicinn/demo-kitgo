<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\User;
use App\Models\Subitem;
use App\Models\Project;
use App\Models\Item;
use App\Models\Organization;
use App\Models\SubCategories;



class DashboardController extends Controller
{
    public function index(Request $request){
	
		$user = auth()->user();
		$organization_id = $user->organization_id;
		$date = date('Y-m-d');
		
		$organization = json_decode($user->assigned_organizations);
		
		$users = User::where(function ($query) use ($organization_id, $organization) {
			if($organization_id){
				$query->whereJsonContains('assigned_organizations', ["{$organization_id}"]);
			}
			else {
				if(@$organization){
					foreach($organization as $org){
						$query->orWhereJsonContains('assigned_organizations', $org); 
					}
				}
			}
		})
		->count();
		
		$subitems = Subitem::where(function ($query) use ($organization_id, $organization) {
			if($organization_id){
				$query->where('organisation_id', $organization_id);
			}
			else {
				if(@$organization){
					$query->whereIn('organisation_id', $organization);
				}
			}
		})
		->where('disposed', '0')
		->count();
		
		$projects = Project::where(function ($query) use ($organization_id, $organization) {
			if($organization_id){
				$query->where('organisation_id', $organization_id);
			}
			else {
				if(@$organization){
					$query->whereIn('organisation_id', $organization);
				}
			}
		})
		->count();
		
		$items = Item::where(function ($query) use ($organization_id) {
			if($organization_id){
				//$query->where('organisation_id', $organization_id);
			}
		})
		->count();
		
		$allItems = SubCategories::where(function ($query) use ($organization_id) {
			if($organization_id){
				//$query->where('organisation_id', $organization_id);
			}
		})
		->get();
		
		$allSubItems = Subitem::where(function ($query) use ($organization_id, $organization) {
			if($organization_id){
				$query->where('organisation_id', $organization_id);
			}
			else {
				if(@$organization){
					$query->whereIn('organisation_id', $organization);
				}
			}
		})
		->get();
	
		$numberOfAssets = Subitem::where(function ($query) use ($organization_id, $organization) {
			if($organization_id){
				$query->where('organisation_id', $organization_id);
			}
			else {
				if(@$organization){
					$query->whereIn('organisation_id', $organization);
				}
			}
		})
		->where('status', '!=', 'available')
		->where('disposed', '0')
		->count();
	
		$disposed = Subitem::where(function ($query) use ($organization_id, $organization) {
			if($organization_id){
				$query->where('organisation_id', $organization_id);
			}
			else {
				if(@$organization){
					$query->whereIn('organisation_id', $organization);
				}
			}
		})
		->where(function ($query){
			$query->where('disposed', '1');
			$query->orWhere('is_missed', '1');
		})
		->count();
		
		$availableAssets = Subitem::where(function ($query) use ($organization_id, $organization) {
			if($organization_id){
				$query->where('organisation_id', $organization_id);
			}
			else {
				if(@$organization){
					$query->whereIn('organisation_id', $organization);
				}
			}
		})
		->where('status', 'available')
		->count();
		
		$brokenAssets = Subitem::where(function ($query) use ($organization_id, $organization) {
			if($organization_id){
				$query->where('organisation_id', $organization_id);
			}
			else {
				if(@$organization){
					$query->whereIn('organisation_id', $organization);
				}
			}
		})
		->where('condition', 'broken')
		->count();
		
		
		//..
		$reservations = Subitem::where(function ($query) use ($organization_id, $organization) {
			if($organization_id){
				$query->where('organisation_id', $organization_id);
			}
			else {
				if(@$organization){
					$query->whereIn('organisation_id', $organization);
				}
			}
		})
		->where('status', 'reserved')
		->count();
		
		$checkin = Subitem::where(function ($query) use ($organization_id, $organization) {
			if($organization_id){
				$query->where('organisation_id', $organization_id);
			}
			else {
				if(@$organization){
					$query->whereIn('organisation_id', $organization);
				}
			}
		})
		->where(function ($query){
			$query->where('status', 'on_location_kit_pack');
		})
		->count();

		$checkout = Subitem::where(function ($query) use ($organization_id, $organization) {
			if($organization_id){
				$query->where('organisation_id', $organization_id);
			}
			else {
				if(@$organization){
					$query->whereIn('organisation_id', $organization);
				}
			}
		})		
		->where(function ($query){
			$query->where('status', 'kit_packing');
		})
		->count();
		
		$overdueItems = Project::where(function ($query) use ($organization_id, $organization) {
			if($organization_id){
				$query->where('organisation_id', $organization_id);
			}
			else {
				if(@$organization){
					$query->whereIn('organisation_id', $organization);
				}
			}
		})
		->where('project_status', '!=', 'warehouse_return')
		->where('expected_return_date', '<', $date)
		->count();
		
		return view('dashboard', compact('users', 'subitems', 'projects', 'items', 'allItems', 'allSubItems', 'checkin', 'reservations', 'numberOfAssets', 'availableAssets', 'brokenAssets', 'disposed', 'checkout', 'overdueItems'));
	
	
	}
	
	
	
	
}
