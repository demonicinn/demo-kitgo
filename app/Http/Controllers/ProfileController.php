<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Intervention\Image\ImageManagerStatic as Image;
use Cookie;
use App\Models\User;
use App\Models\Organization;
use Hash;

class ProfileController extends Controller
{
    public function index()
    {
		$user = auth()->user();
        return view('pages.profile.profile', compact('user'));
    }

    public function update(Request $request)
	{
		$user = auth()->user();
		$request->validate([
			'name' => 'required|max:50',
			'email' => 'required|max:100|unique:users,email,'.$user->id,
			'image' => 'mimetypes:image/jpeg,image/png,image/jpg|max:1024',
        ]);

		$user->name = $request->name;
		$user->email = $request->email;
		
		if ($request->file('image')) {
			$path = 'storage/profile/';
			if(!is_dir($path)) {
				mkdir($path, 0775, true);
				chown($path, exec('whoami'));
			}
			if(@is_file('storage/profile/'.$user->profile_photo_path)) {
				unlink('storage/profile/'.$user->profile_photo_path);
			}
			
			$image = 'pic-'. $user->id . time() .'.png';
			Image::make($request->file('image'))->resize(154, 137)->save($path.$image);
			$user->profile_photo_path = $image;
		}
		
		$user->save();

		$request->session()->flash('success', "Profile updated successfully");
        return redirect()->route('profile');
	}





	public function password()
    {
        return view('pages.profile.password');
    }

    public function passwordUpdate(Request $request)
	{		
		$request->validate([
            'current-password' => 'required',
            'new-password' => 'required|string|min:6|same:new-password',
            'new-password_confirmed' => 'required|same:new-password',
        ]);

		if (!(Hash::check($request->get('current-password'), auth()->user()->password))) {
            // The passwords matches
			$request->session()->flash('danger', "Your current password does not matches with the password you provided. Please try again.");
			return redirect()->back();
        }
 
        if(strcmp($request->get('current-password'), $request->get('new-password')) == 0){
            //Current password and new password are same
			$request->session()->flash('warning', "New Password cannot be same as your current password. Please choose a different password.");
			return redirect()->back();
        }        
 
        //Change Password
        $user = auth()->user();
        $user->password = Hash::make($request->get('new-password'));
        $user->save();
 
		$request->session()->flash('success', "Password changed successfully");
        return redirect()->back();
	}	
	
	public function setOrganization(Request $request){
		//if(auth()->user()->role=='superadmin'){
			
			$mess = 'Organization filter cleared';
			
			$user = User::find(auth()->user()->id);
			
			if($request->organizations){
				$organizations = Organization::find($request->organizations);				
				if(@$organizations){
					$mess = 'Organization filter set as '.$organizations->name;
					$user->organization_id = $request->organizations;
				}
			}
			else {
				$user->organization_id = null;			
			}
			$user->save();
			
			$request->session()->flash('success', $mess);
			return redirect()->back();
		//}
		//$request->session()->flash('error', 'Invalid Request');
		//return redirect()->route('dashboard');
	}
	
}
