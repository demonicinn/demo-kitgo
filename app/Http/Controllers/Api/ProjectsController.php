<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Project;
use App\Models\ProjectHistory;
use Auth;

class ProjectsController extends Controller
{
    public function index(Request $request)
	{
		$user = auth()->user();
		$organization_id = $user->organization_id;
		$date = date('Y-m-d');
		$organization = json_decode($user->assigned_organizations);
		
		$projects = Project::with('withSubitems')
			->where(function ($query) use ($organization_id, $organization) {
				if($organization){
					if(@$organization){
						$query->whereIn('organisation_id', $organization);
					}
				}
				else {
					$query->where('organisation_id', $organization_id);
				}
			})
			->where('status', '1')
			->where('project_status', '!=', 'warehouse_return')
			->get();
		
		
		if($projects){
			foreach($projects as $project){
				$project->project_status = 'Live';
				if($project->pickup_date > $date){
					$project->project_status = 'In Future';
				}
				if($project->expected_return_date < $date){
					$project->project_status = 'Archived';
				}
				if(($project->pickup_date >= $date) && ($project->expected_return_date <= $date)){
					$project->project_status = 'Live';
				}
				
				foreach($project->withSubitems as $items){
					
					$items->pictureurl = $items->pictureurl ? asset('storage/photos/'.$items->pictureurl) : asset('img/noimg.png');
					$items->receipt_url = $items->receipt_url ? asset('storage/photos/'.$items->receipt_url) : '';
					$items->barcode_url = $items->barcode_url ? asset('assets/barcodes/'.$items->barcode_url) : '';
					$items->status_color = subItemColorCode($items->pivot->status);
					$items->status_text = subItemStatus($items->pivot->status);
					
					//$items->subitem_barcode_available = $items->subitemBarcodeAvailable;
					//$items->project_barcode = $items->projectbarcode;
				}				
				
			}
			return response($projects, 200);
		}
		return response(['message'=>'Data not found', "code"=>422], 422);
	}
	
	
	//tracking number	
	public function trackingNumber(Request $request)
	{
		$request->validate([
			'project_id' => 'required',
			'tracking_number' => 'required'
		]);
		
		$projects = Project::find($request->project_id);
		
		$trackMess = 'Project Tracking # assigned - '.$request->tracking_number;
		if(@$projects->tracking_number){
			$trackMess = 'Project Tracking # assigned - '.$request->tracking_number;
		}
		
		
		$projects->tracking_number = $request->tracking_number;
		$projects->updated_by = auth()->user()->id;
		$projects->save();
		
		//...
		$history = new ProjectHistory;
		$history->project_id = $request->project_id;
		$history->notificationtext = $trackMess;
		$history->created_by = auth()->user()->id;
		$history->updated_by = auth()->user()->id;
		$history->save();
		
		return response(['message'=>$trackMess], 200);
	}
	
	//Outgoing shipping crate	
	public function outgoingShippingCrate(Request $request)
	{
		$request->validate([
			'project_id' => 'required',
			'outgoing_shipping_crate' => 'required'
		]);
		
		$projects = Project::find($request->project_id);
		
		$trackMess = 'Outgoing Tracking # assigned - '.$request->outgoing_shipping_crate;
		if(@$projects->outgoing_shipping_crate){
			$trackMess = 'Outgoing Tracking # assigned - '.$request->outgoing_shipping_crate;
		}
		
		
		$projects->outgoing_shipping_crate = $request->outgoing_shipping_crate;
		$projects->updated_by = auth()->user()->id;
		$projects->save();
		
		//...
		$history = new ProjectHistory;
		$history->project_id = $request->project_id;
		$history->notificationtext = $trackMess;
		$history->created_by = auth()->user()->id;
		$history->updated_by = auth()->user()->id;
		$history->save();
		
		return response(['message'=>$trackMess], 200);
	}
	
	//incoming Shipping crate
	public function incomingShippingCrate(Request $request)
	{
		$request->validate([
			'project_id' => 'required',
			'incoming_shipping_crate' => 'required'
		]);
		
		$projects = Project::find($request->project_id);
		
		$trackMess = 'Incoming Tracking # assigned - '.$request->incoming_shipping_crate;
		if(@$projects->incoming_shipping_crate){
			$trackMess = 'Incoming Tracking # assigned - '.$request->incoming_shipping_crate;
		}
		
		
		$projects->incoming_shipping_crate = $request->incoming_shipping_crate;
		$projects->updated_by = auth()->user()->id;
		$projects->save();
		
		//...
		$history = new ProjectHistory;
		$history->project_id = $request->project_id;
		$history->notificationtext = $trackMess;
		$history->created_by = auth()->user()->id;
		$history->updated_by = auth()->user()->id;
		$history->save();
		
		return response(['message'=>$trackMess], 200);
	}
	
}
