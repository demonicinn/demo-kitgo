<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Project;
use App\Models\ProjectItem;
use App\Models\ProjectHistory;
use App\Models\ProjectBarcode;
use App\Models\SubitemBarcode;
use App\Models\Subitem;
use Carbon\Carbon;

class CheckoutController extends Controller
{
    //store	
	public function store(Request $request)
	{
		$request->validate([
			'project_id' => 'required',
			'barcode_number' => 'required',
			'latitude' => 'required',
			'longitude' => 'required',
		]);
		
		$subitem = Subitem::where('barcode_no', $request->barcode_number)
					->where('organisation_id', auth()->user()->organization_id);
					->first();
		
		if(@$subitem){
			$projectItem = ProjectItem::where('project_id', $request->project_id)
				->where('item_id', $subitem->id)
				->first();
				
			if(!$projectItem){
				$projectItem = ProjectItem::where('project_id', $request->project_id)
					->where('subcategory', $subitem->item_id)
					->where('status', 'reserved')
					->first();
					
					
					
					
				// if($projectItem->status!='reserved'){
					// return response(['message'=>"No such item is linked with this project."], 404);
				// }
			}
		
			if(@$projectItem){
			
				if($projectItem->status=='warehouse_return'){
					return response(['message'=>"Project Already Completed."], 404);
				}
				
				/* $lastUpdate = ProjectItem::where('project_id', $request->project_id)
					->where('item_id', $subitem->id)
					->where('status', '!=', 'reserved')
					->where('updated_at', '>=', Carbon::now()->subHour())
					->first();
				if(@$lastUpdate){
					return response(['message'=>"Item Already Scaned."], 404);
				} */
				
				$message = 'Kit Packed';
				$status = $projectItem->status;
				
				if(@$status=='reserved'){
					$projectItem->status = 'kit_packing';
					//..
					$subitem->status = 'kit_packing';
					Project::find($request->project_id)->update(['project_status'=>'kit_packing']);
				}
				
				if(@$status=='kit_packing' || $status=='onjob' || $status=='intransit'){
					$message = 'On Location Kit Packed';
					$projectItem->status = 'on_location_kit_pack';
					
					//..
					$subitem->status = 'on_location_kit_pack';
					Project::find($request->project_id)->update(['project_status'=>'on_location_kit_pack']);
				}
				
				if(@$status=='on_location_kit_pack'){
					$message = 'Warehouse Returned';
					$projectItem->status = 'warehouse_return';
					
					//..
					$subitem->status = 'available';
					Project::find($request->project_id)->update(['project_status'=>'warehouse_return']);
				}
				
				if($projectItem->item_id != $subitem->id){
					Subitem::find($projectItem->item_id)->update(['status'=>'available']);
					//...
					$projectItem->item_id = $subitem->id;
				}
				
				
				$projectItem->save();
				
				//...subitem
				$subitem->latitude = $request->latitude;
				$subitem->longitude = $request->longitude;
				$subitem->save();
				
				
				$newMess = $subitem->make.' - '. $subitem->model .' has been '.$message;
				//...
				$history = new ProjectHistory;
				$history->project_id = $request->project_id;
				$history->barcode = $request->barcode_number;
				$history->notificationtext = $newMess;
				$history->created_by = auth()->user()->id;
				$history->updated_by = auth()->user()->id;
				$history->save();
		
				return response(['message'=>$newMess], 200);
			}
			return response(['message'=>"No such item is linked with this project."], 404);
		}
		
		return response(['message'=>"No such item was found."], 404);		
	}
}





