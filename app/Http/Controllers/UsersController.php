<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Intervention\Image\ImageManagerStatic as Image;
use Hash;

use App\Models\User;

class UsersController extends Controller
{
	
	//show	
	public function show(Request $request, User $user){
		if(auth()->user()->role=='user'){
			$request->session()->flash('warning', "Permission Denied");
			return redirect()->route("dashboard");
		}
		return view('pages.users.show', compact('user'));
	}
	
    //...
	public function create(Request $request)
	{
		if(auth()->user()->role=='user'){
			$request->session()->flash('warning', "Permission Denied");
			return redirect()->route("dashboard");
		}
		return view('pages.users.create');
	}
	
	//...
	public function store(Request $request)
	{
		$request->validate([
			'role' => 'required',
			'name' => 'required',
			'password' => 'required|string|min:8',
			'organization' => 'required',
			'email' => 'required|unique:users,email',
			'image' => 'mimetypes:image/jpeg,image/png,image/jpg|max:1024',
		]);
		
		$user = new User;
		
		if ($request->file('image')) {
			$path = 'storage/profile/';
			if(!is_dir($path)) {
				mkdir($path, 0775, true);
				chown($path, exec('whoami'));
			}
			
			$image = 'pic-'. auth()->user()->id . time() .'.png';
			Image::make($request->file('image'))->resize(154, 137)->save($path.$image);
			$user->profile_photo_path = $image;
		}
		
		$user->role = $request->role;
		$user->name = $request->name;
		$user->password = Hash::make($request->password);
		$user->assigned_organizations = json_encode($request->organization);
		$user->organization_id = @$request->organization[0];
		$user->email = $request->email;
		$user->created_by = auth()->user()->id;
		$user->updated_by = auth()->user()->id;
		$user->save();
		
		$request->session()->flash('success', "User Created Successfully");
		return redirect()->route("users");
	}
	
	
	
	//...
	public function edit(Request $request, User $user)
	{
		if(auth()->user()->role=='user'){
			$request->session()->flash('warning', "Permission Denied");
			return redirect()->route("dashboard");
		}
		return view('pages.users.edit', compact('user'));
	}
	
	//...
	public function update(Request $request, User $user)
	{
		$request->validate([
			'role' => 'required',
			'name' => 'required',
			'organization' => 'required',
			'email' => 'required|unique:users,email,'.$user->id,
			'image' => 'mimetypes:image/jpeg,image/png,image/jpg|max:1024',
		]);
		
		
        if ($request->file('image')) {
			$path = 'storage/profile/';
			if(!is_dir($path)) {
				mkdir($path, 0775, true);
				chown($path, exec('whoami'));
			}
			if(@is_file('storage/profile/'.$user->profile_photo_path)) {
				unlink('storage/profile/'.$user->profile_photo_path);
			}
			
			$image = 'pic-'. $user->id . time() .'.png';
			Image::make($request->file('image'))->resize(154, 137)->save($path.$image);
			$user->profile_photo_path = $image;
		}
		
		$user->role = $request->role;
		$user->name = $request->name;
		if(@$request->password){
			$user->password = Hash::make($request->password);
		}
		$user->assigned_organizations = json_encode($request->organization);
		$user->email = $request->email;
		$user->save();
		
		$request->session()->flash('success', "User Updated Successfully");
		return redirect()->route("users.show", $user->id);
	}
	
	
	//destroy
	public function destroy(Request $request, User $user){		
		
		$user->delete();
	
		$request->session()->flash('success', "User Deleted");
		return redirect()->route('users');
	}
}
