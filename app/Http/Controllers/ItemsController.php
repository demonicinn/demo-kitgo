<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Item;
use App\Models\Linkeditem;

class ItemsController extends Controller
{
    //...
	public function create(Request $request)
	{
		$getlinkeditems = Item::getlinkeditems()->pluck('name', 'id');
		return view('pages.items.create', compact('getlinkeditems'));
	}
	
	//...
	public function store(Request $request)
	{
		$request->validate([
			'name' => 'required|unique:items,name',
			'organization_id' => 'required',
		]);
		
		$store = new Item;
		$store->name = $request->name;
		$store->organisation_id = $request->organization_id;
		$store->created_by = auth()->user()->id;
		$store->updated_by = auth()->user()->id;
		$store->status = 'available';
		$store->save();
		
		//...
		if(@$request->linked_items){
			foreach ($request->linked_items as $linkedItems) {
				$linkStore = new Linkeditem;
				$linkStore->item_id = $store->id;
				$linkStore->linked_item_id = $linkedItems;
				$linkStore->created_by = auth()->user()->id;
				$linkStore->updated_by = auth()->user()->id;
				$linkStore->save();
			}
		}
		
		$request->session()->flash('success', "Category Created Successfully");
		return redirect()->route("items");
	}
	
	//...
	public function edit(Request $request, Item $item)
	{
		$getlinkeditems = $item->getlinkeditems()->where('id', '!=', $item->id)->pluck('name', 'id');
		$linkeditem = Linkeditem::where('item_id', $item->id)->pluck('id')->toArray();
		
		return view('pages.items.edit', compact('item', 'getlinkeditems', 'linkeditem'));
	}
	
	//...
	public function update(Request $request, Item $item)
	{
		$request->validate([
			'name' => 'required|unique:items,name,'.$item->id,
			'organization_id' => 'required',
		]);
		
		$item->name = $request->name;
		$item->organisation_id = $request->organization_id;
		$item->created_by = auth()->user()->id;
		$item->updated_by = auth()->user()->id;
		$item->save();
		
		//...
		if(@$request->linked_items){
			foreach ($request->linked_items as $linkedItems) {
				$linkCheck = Linkeditem::where('linked_item_id', $linkedItems)
					->where('item_id', $item->id)->first();
				
				$linkStore = new Linkeditem;
				if(@$linkCheck){
					$linkStore->id = $linkCheck->id;
					$linkStore->exists = true;
				}
				$linkStore->item_id = $item->id;
				$linkStore->linked_item_id = $linkedItems;
				$linkStore->created_by = auth()->user()->id;
				$linkStore->updated_by = auth()->user()->id;
				$linkStore->save();
			}
		}
		
		$request->session()->flash('success', "Category Updated Successfully");
		return redirect()->route("items");
	}
}
