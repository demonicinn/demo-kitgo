<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Project;
use App\Models\ProjectHistory;
use App\Models\ProjectItem;
use App\Models\Subitem;
use App\Models\SubitemHistory;
use App\Models\Linkedsubitem;
use App\Models\SubCategories;
use DB;
use Illuminate\Support\Facades\Storage;
use Auth;

class ProjectsController extends Controller
{
	//show	
	public function show(Request $request, Project $project){
		return view('pages.projects.show', compact('project'));
	}
	
	public function subitems(Request $request)
	{
		$subcategories = SubCategories::all();
		
		$newarray = [];
		foreach($subcategories as $itm){
		
			$subitems = Subitem::where('status','available')
				->where('item_id', $itm->id)
				->where('disposed', '0')
				->where('is_missed', '0')
				->where('organisation_id', Auth::user()->organization_id)
				->count();
				
			if($subitems > 0){
				$array = [];
				$array['id'] = $itm->id;
				$array['name'] = $itm->name;
				$array['totalQty'] = $subitems;
				$newarray[] = $array;
			}
		}
		return $newarray;
	}
	
	public function subitemsLinkedDelete(Request $request){
		$projectitem = ProjectItem::find($request->item);
		
		if(@$projectitem){
			$subitem = Subitem::find($projectitem->item_id);
			$subitem->status = 'available';
			$subitem->save();
			
			$projectitem->delete();
			return true;
		}
		return false;
	}
	
	
	public function subitemsLinked(Request $request){
		//return Linkedsubitem::where('sub_item_id', $request->item)->pluck('linked_sub_item_id')->toArray();
		
		$subcategories = SubCategories::find($request->item);
		
		$subcategories_linked = SubCategories::whereIn('id', json_decode($subcategories->linked_id))->get();
		
		$newarray = [];
		foreach($subcategories_linked as $itm){
		
			$subitems = Subitem::where('status','available')
				->where('item_id', $itm->id)
				->where('disposed', '0')
				->where('is_missed', '0')
				->where('organisation_id', Auth::user()->organization_id)
				->count();
				
			if($subitems > 0){
				$array = [];
				$array['id'] = $itm->id;
				$array['name'] = $itm->name;
				$array['totalQty'] = $subitems;
				$array['linked_with'] = $request->item;
				$newarray[] = $array;
			}
		}
		return $newarray;
	}
	
    //...
	public function create(Request $request)
	{
		//self::subitems($request);
		
		if(!auth()->user()->organization_id){
			$request->session()->flash('error', "Please select an organisation to proceed with these features.");
			return redirect()->back();
		}
		
		$getsubitems = Subitem::availableItems();
		return view('pages.projects.create', compact('getsubitems'));
	}
	
	//...
	public function store(Request $request)
	{
		$request->validate([
			'title' =>'required',
			'client_name' =>'required',
			'client_contact_number' =>'required',
			'location' =>'required',
			'user' =>'required',
            'pickup_date' =>'required|date',
            'shipping_date' => 'required|date|after_or_equal:pickup_date',
            'start_date' => 'required|date|after_or_equal:shipping_date',
            'end_date' => 'required|date|after_or_equal:start_date',
            'expected_return_date' => 'required|date|after_or_equal:end_date',
		]);

		$store = new Project;
		$store->title = $request->title;
		$store->client_name = $request->client_name;
		$store->client_contact_number = $request->client_contact_number;
		$store->location = $request->location;
		$store->pickup_date = $request->pickup_date;
		$store->shipping_date = $request->shipping_date;
		$store->start_date = $request->start_date;
		$store->end_date = $request->end_date;
		$store->expected_return_date = $request->expected_return_date;
		$store->created_by = auth()->user()->id;
		$store->updated_by = auth()->user()->id;
		$store->organisation_id = auth()->user()->organization_id;
		$store->user = $request->user;
		$store->status = auth()->user()->role == 'user' ? '2' : '1';
		$store->project_status = 'reserved';
		$store->save();
		
		$phis = new ProjectHistory;
		$phis->project_id = $store->id;
		$phis->created_by = auth()->user()->id;
		$phis->updated_by = auth()->user()->id;
		$phis->notificationtext = 'Project Created';
		$phis->save();
		
		//...
		$perform = Project::find($store->id);
		if(@$request->stockProducts){
			foreach($request->stockProducts as $item){
				if(@$item){
					//---
					$subcat = SubCategories::find($item['name']);
					
					foreach($subcat->subitemsAvailable as $i => $subitms){
						if($i<$item['qty']){
							Subitem::find($subitms->id)->update(['status'=>'reserved']);
						
							//..
							$perform->items()->create([
								'quantity' =>  '1',
								'item_id'  => $subitms->id,
								'user_id'  => auth()->user()->id,
								'project_id' => $perform->id,
								'status' => 'reserved',
								'subcategory' => $subitms->item_id,
							])->user()->associate(auth()->user()->id)->save();
							
							$his = new SubitemHistory;
							$his->sub_item_id = $subitms->id;
							$his->created_by = auth()->user()->id;
							$his->updated_by = auth()->user()->id;
							$his->notificationtext = 'Item Reserved';
							$his->save();
					
						}
					}
				}
			}
		}
		
		$request->session()->flash('success', "Project Created Successfully");
		return redirect()->route("projects");
	}
	
	//...
	public function edit(Request $request, Project $project)
	{
		if(!auth()->user()->organization_id){
			$request->session()->flash('error', "Please select an organisation to proceed with these features.");
			return redirect()->back();
		}
		return view('pages.projects.edit', compact('project'));
	}
	
	//...
	public function update(Request $request, Project $project)
	{
		$request->validate([
			'title' =>'required',
			'client_name' =>'required',
			'client_contact_number' =>'required',
			'location' =>'required',
			'user' =>'required',
            'pickup_date' =>'required|date',
            'shipping_date' => 'required|date|after_or_equal:pickup_date',
            'start_date' => 'required|date|after_or_equal:shipping_date',
            'end_date' => 'required|date|after_or_equal:start_date',
            'expected_return_date' => 'required|date|after_or_equal:end_date',
		]);		
		
		$project->title = $request->title;
		$project->client_name = $request->client_name;
		$project->client_contact_number = $request->client_contact_number;
		$project->location = $request->location;
		$project->pickup_date = $request->pickup_date;
		$project->shipping_date = $request->shipping_date;
		$project->start_date = $request->start_date;
		$project->end_date = $request->end_date;
		$project->expected_return_date = $request->expected_return_date;
		$project->created_by = auth()->user()->id;
		$project->updated_by = auth()->user()->id;
		$project->organisation_id = auth()->user()->organization_id;
		$project->user = $request->user;
		$project->save();		
		
		$phis = new ProjectHistory;
		$phis->project_id = $project->id;
		$phis->created_by = auth()->user()->id;
		$phis->updated_by = auth()->user()->id;
		$phis->notificationtext = 'Project updated';
		$phis->save();
		
		//...
		$perform = Project::find($project->id);
		if(@$request->stockProducts){
			foreach($request->stockProducts as $item){
				if(@$item){
					//---
					$subcat = SubCategories::find($item['name']);
					if(@$subcat && $subcat->subitemsAvailable){
						foreach($subcat->subitemsAvailable as $i => $subitms){
							if($i<$item['qty']){
								Subitem::find($subitms->id)->update(['status'=>'reserved']);
							
								//..
								$perform->items()->create([
									'quantity' =>  '1',
									'item_id'  => $subitms->id,
									'user_id'  => auth()->user()->id,
									'project_id' => $perform->id,
									'status' => 'reserved',
									'subcategory' => $subitms->item_id,
								])->user()->associate(auth()->user()->id)->save();
								
								$his = new SubitemHistory;
								$his->sub_item_id = $subitms->id;
								$his->created_by = auth()->user()->id;
								$his->updated_by = auth()->user()->id;
								$his->notificationtext = 'Item Reserved';
								$his->save();
								
							}
						}
					}
				}
			}
		}
		
		$request->session()->flash('success', "Project Updated Successfully");
		return redirect()->route("projects.show", $project->id);
	}
	
	
	
	//...
	public function checkout(Request $request, Project $project)
	{
		return view('pages.projects.checkout', compact('project'));
	}
	
	
	
	
	
	
	///...
	public function export(){
	
		$filename = 'projects_' . auth()->user()->id . date('Y_m_d_h_i');
		
		$headers = array(
			"Content-type" => "text/csv",
			"Content-Disposition" => "attachment; filename=".$filename.".csv",
			"Pragma" => "no-cache",
			"Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
			"Expires" => "0"
		);
		
		$columns = array('Title', 'Pickup date', 'Shipping date', 'Start date', 'End date', 'Return date', 'Contact Person');
		
		$projects = Project::where('organisation_id', '=', auth()->user()->organization_id)->get();
		
		if(auth()->user()->role=='admin'){
			$projects = Project::get();
		}		
		
		$callback = function() use ($projects, $columns) {
			$file = fopen('php://output', 'w');
			fputcsv($file, $columns);
			
			foreach($projects as $item){
				
				$datas = array();
				
				$datas['Title'] = @$item->title;
				$datas['Pickup date'] = @$item->pickup_date;
				$datas['Shipping date'] = @$item->shipping_date;
				$datas['Start date'] = @$item->start_date;
				$datas['End date'] = @$item->end_date;
				$datas['Return date'] = @$item->expected_return_date;
				$datas['Contact Person'] = @$item->projectUser->name;
				
				fputcsv($file, $datas);
			}
			fclose($file);
		};
		
		return response()->stream($callback, 200, $headers);
	}
	
	public function import(Request $request)
	{
		$request->validate([
			'file' => 'required|file',
		]);
		
		if ($request->file('file')) {
			
			$file = $request->file('file');
			
			$path = 'storage/uploads/csv/';
			if(!is_dir($path)) {
				mkdir($path, 0775, true);
				chown($path, exec('whoami'));
			}
			
			$filename = 'projects-'.md5(uniqid().time()).'.'.$file->getClientOriginalExtension();
			// Upload file
			$file->move($path, $filename);
			
			// Import CSV to Database
			$filepath = ('public/uploads/csv/'.$filename);
			
			// Reading file
			$data = Storage::get($filepath);
			
			$csv = array_map(function($row) {
				$data = str_getcsv($row);
				//insert players
				$format = "Y-m-d";
				if(@$data && @$data[1] && (date($format, strtotime($data[1])) == date($data[1]))) {
					
					$store = new Project;
					$store->title = $data[0];
					$store->pickup_date = $data[1];
					$store->shipping_date = $data[2];
					$store->start_date = $data[3];
					$store->end_date = $data[4];
					$store->expected_return_date = $data[5];
					$store->created_by = auth()->user()->id;
					$store->updated_by = auth()->user()->id;
					$store->organisation_id = auth()->user()->organization_id;
					$store->user = auth()->user()->id;
					$store->status = '1';
					$store->save();
					
					$phis = new ProjectHistory;
					$phis->project_id = $store->id;
					$phis->created_by = auth()->user()->id;
					$phis->updated_by = auth()->user()->id;
					$phis->notificationtext = 'Project Created';
					$phis->save();
		
				}
			}, explode("\n", $data)); 
			
			$request->session()->flash('success', "Csv Uploaded");
			return redirect()->route('projects');
		}
		$request->session()->flash('error', "Csv File Missing");
		return redirect()->route('projects');
	
	}
	
	
	
	
	public function projectStatus(Request $request, Project $project, $status){
		if(auth()->user()->role!='user'){
			
			$project->status = $status == 'approve' ? '1' : '0';
			$project->save();			
			
			$phis = new ProjectHistory;
			$phis->project_id = $project->id;
			$phis->created_by = auth()->user()->id;
			$phis->updated_by = auth()->user()->id;
			$phis->notificationtext = "Project ".$status;
			$phis->save();	
			
			if($status=='reject'){
				if($project->withSubitems){
					foreach($project->withSubitems as $items){
						$items->status = 'available';
						$items->save();
					}
				}
			}
			
			$request->session()->flash('success', "Project ".$status);
			return redirect()->route('projects');
		}
		$request->session()->flash('error', "Invalid Request");
		return redirect()->route('projects');
	}
	
	
	//destroy
	public function destroy(Request $request, Project $project){
		
		foreach($project->withSubitems as $item){
			$item->status = 'available';
			$item->save();
		}
		
		$project->delete();
	
		$request->session()->flash('success', "Project Deleted");
		return redirect()->route('projects');
	}
	
	
	
	
	
}
