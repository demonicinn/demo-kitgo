<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

use App\Models\Item;
use App\Models\SubCategories;


class SubCategoriesController extends Controller
{
    //...
	public function index(Request $request)
	{
		return view('pages.subcategories.index');
	}
	
	//...
	public function create(Request $request)
	{
		$user = auth()->user();
		$organization = json_decode($user->assigned_organizations);
		
		$items = Item::where('status', 'available')
			->where(function ($query) use ($organization){
				if(@$organization && auth()->user()->role!='superadmin'){
					$query->whereIn('organisation_id', $organization);
				}
			})
			->pluck('name', 'id');
		
		$subcategory = SubCategories::where(function ($query) use ($organization){
				if(@$organization && auth()->user()->role!='superadmin'){
					$query->whereIn('organisation_id', $organization);
				}
			})
			->pluck('name', 'id');		
		
		return view('pages.subcategories.create', compact('items', 'subcategory'));
	}
	
	//...
	public function store(Request $request)
	{
		$request->validate([
			'category' => 'required',
			//'organization_id' => 'required',
			'name' =>  [
				'required',
				Rule::unique('sub_categories')->where('name', $request->name)->where('category', $request->category)
			]
		]);
		
		$store = new SubCategories;
		$store->organisation_id = $request->organization_id;
		$store->category = $request->category;
		$store->name = $request->name;
		$store->linked_id = json_encode($request->linked_id);
		$store->created_by = auth()->user()->id;
		$store->updated_by = auth()->user()->id;
		$store->save();
		
		$request->session()->flash('success', "Sub Category Created Successfully");
		return redirect()->route("subCategories");
	}
	
	//...
	public function edit(Request $request, SubCategories $category)
	{
		$user = auth()->user();
		$organization = json_decode($user->assigned_organizations);
		
		$items = Item::where('status', 'available')
			->where(function ($query) use ($organization){
				if(@$organization && auth()->user()->role!='superadmin'){
					$query->whereIn('organisation_id', $organization);
				}
			})
			->pluck('name', 'id');
		
		$subcategory = SubCategories::where(function ($query) use ($organization){
				if(@$organization && auth()->user()->role!='superadmin'){
					$query->whereIn('organisation_id', $organization);
				}
			})
			->pluck('name', 'id');	
		
		return view('pages.subcategories.edit', compact('category', 'items', 'subcategory'));
	}
	
	//...
	public function update(Request $request, SubCategories $category)
	{
		$request->validate([
			'category' => 'required',
			//'organization_id' => 'required',
			'name' =>  [
				'required',
				Rule::unique('sub_categories')->where('name', $request->name)->where('category', $request->category)->ignore($category->id)
			]
		]);
		
		$category->organisation_id = $request->organization_id;
		$category->category = $request->category;
		$category->name = $request->name;
		$category->linked_id = json_encode($request->linked_id);
		$category->updated_by = auth()->user()->id;
		$category->save();		
		
		$request->session()->flash('success', "Sub Category Updated Successfully");
		return redirect()->route("subCategories");
	}
}
