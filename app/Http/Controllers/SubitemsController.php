<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Item;
use App\Models\Subitem;
use App\Models\SubitemHistory;
use App\Models\SubitemBarcode;
use App\Models\Linkedsubitem;
use QrCode;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;
use Milon\Barcode\DNS1D;

use App\Models\Project;
use App\Models\ProjectHistory;
use App\Models\ProjectItem;
use App\Models\SubCategories;
use DB;
use Auth;

class SubitemsController extends Controller
{
    //show	
	public function show(Request $request, Subitem $item)
	{	
		return view('pages.subitems.show', compact('item'));
	}

	//...
	public function create(Request $request)
	{
		if(!auth()->user()->organization_id){
			$request->session()->flash('error', "Please select an organisation to proceed with these features.");
			return redirect()->back();
		}
		
		$organization_id = Auth::user()->organization_id;
		$getlinkeditems = SubCategories::select('sub_categories.*')
			->leftjoin('items', 'items.id', '=', 'sub_categories.category')			
			->where(function ($query) use ($organization_id) {
				if($organization_id){
					$query->where('items.organisation_id', $organization_id);
				}
			})
			->pluck('name', 'id');
		
		
		$getConditions = Subitem::getConditions();
		//$getlinkedsubitems = Subitem::getlinkedsubitems()->pluck('serialnumber', 'id')->toArray();
		
		$getlinkedsubitems = Subitem::select(DB::raw("CONCAT(subitems.make,' - ',subitems.model) as name"),'subitems.id as id')
			->where('subitems.organisation_id', '=', Auth::user()->organization_id)
			->pluck('name', 'id');
			
	
		return view('pages.subitems.create', compact('getlinkeditems', 'getConditions', 'getlinkedsubitems'));
	}
	
	//...
	public function store(Request $request)
	{
		$request->validate([
			//'serialnumber' => 'required',
            'pictureurl' => 'mimetypes:image/jpeg,image/png,image/jpg,image/webp|max:2048',
            'receipt_url' => 'mimetypes:image/jpeg,image/png,image/jpg,image/webp|max:2048',
            //'pat_date' => 'required',
            //'pat_due_date' => 'required',
            'condition' => 'required',
            'item_id' => 'required',
            'quantity' => 'required',
		]);		
		
		$path = ('storage/photos/');
		if(!is_dir($path)) {
			mkdir($path, 0775, true);
			chown($path, exec('whoami'));
		}
		
		$picture = '';
		if ($request->file('pictureurl')) {
			$image = 'picture-'. auth()->user()->id .'-'. md5(uniqid() . time()) .'.webp';
			Image::make($request->file('pictureurl'))->encode('webp', 90)->resize(400, 400)->save($path.$image);
			//image
			$picture = $image;
		}
		
		$receipt_url = '';
		if (@$request->file('receipt_url')) {
			$image = 'receipt-'. auth()->user()->id .'-'. md5(uniqid() . time()) .'.webp';
			Image::make($request->file('receipt_url'))->encode('webp', 90)->resize(400, 400)->save($path.$image);
			//image
			$receipt_url = $image;
		}		
		
		for($i=1; $i<=$request->quantity; $i++){
		
		$barcode = uniqueNumber();
			
			if(@$request->cable_length){
				$dns = new DNS1D();
				$barcodeImage = $dns->getBarcodePNG($barcode, 'C128');
				$path = public_path('assets/barcodes/' . $barcode . '.png');
				Image::make(file_get_contents('data:image/jpeg;base64, ' . $barcodeImage))->save($path);
			}
			else {
				QrCode::size(200)
					->format('png')
					->generate($barcode, public_path('assets/barcodes/' . $barcode . '.png'));
			}
				
			//$barcodeImage = QrCode::generate($barcode);
			//Image::make(($barcodeImage))->save($barcodespath . $barcode . '.png');
			
			$store = new Subitem;
			$store->serialnumber = $request->serialnumber;
			$store->pat_date = $request->pat_date;
			$store->pat_due_date = $request->pat_due_date;
			$store->pictureurl = $picture;
			$store->date_of_purchase = $request->date_of_purchase;
			$store->warranty_expiry_period = $request->warranty_expiry_period;
			$store->quantity = '1';
			
			/* if(@$request->cable_length){
				$store->make = 'Cable';
				$store->model = $request->cable_length;
			}
			else {
				$store->make = $request->make;
				$store->model = $request->model;
			} */
			
			//$store->available_qty = $request->quantity;
			
			$store->make = $request->make;
			$store->model = $request->model;
			$store->cable_length = $request->cable_length;
			
			$store->receipt_url = $receipt_url;
			$store->barcode_no = $barcode;
			$store->barcode_url = $barcode. '.png';
			$store->condition = $request->condition;
			$store->notes = $request->notes;
			$store->item_id = $request->item_id;
			$store->created_by = auth()->user()->id;
			$store->updated_by = auth()->user()->id;
			$store->organisation_id = auth()->user()->organization_id;
			$store->latitude = auth()->user()->organizations->latitude;
			$store->longitude = auth()->user()->organizations->longitude;
			
			if($request->condition=='needrepair'){
				$store->status = 'needrepair';
			}
			elseif($request->condition=='broken'){
				$store->status = 'broken';
			}
			else {
				$store->status = 'available';
			}
			
			if(@$request->disposed=='1'){
				$store->disposed = '1';
			}
			else {
				$store->disposed = '0';
			}
			
			if(@$request->is_missed=='1'){
				$store->is_missed = '1';
				$store->status = 'item_missed';
			}
			else {
				$store->is_missed = '0';
			}
			
			$store->save();
			
			$his = new SubitemHistory;
			$his->sub_item_id = $store->id;
			$his->created_by = auth()->user()->id;
			$his->updated_by = auth()->user()->id;
			$his->notificationtext = 'Created New Item';
			$his->save();
			
			//...linked subitems
			if(@$request->linkedsubitems){
				foreach ($request->linkedsubitems as $linkedItems) {
					$linkStore = new Linkedsubitem;
					$linkStore->sub_item_id = $store->id;
					$linkStore->linked_sub_item_id = $linkedItems;
					$linkStore->created_by = auth()->user()->id;
					$linkStore->updated_by = auth()->user()->id;
					$linkStore->save();
				}
			}
		
		}
		//...barcodes
		/* if($request->quantity>0){
			for($i=1; $i<=$request->quantity; $i++){
			$barcode = time() . $i . rand();
				
				//save barcode
				$storeBarcode = new SubitemBarcode;
				$storeBarcode->sub_item_id = $store->id;
				$storeBarcode->barcode_no = $barcode;
				$storeBarcode->barcode_url = $barcode . ".jpg";
                $storeBarcode->status = 'available';
				$storeBarcode->save();
			}
		} */
		
		$request->session()->flash('success', "Item Created Successfully");
		return redirect()->route("sub-items");
	}
	
	
	//...
	public function edit(Request $request, Subitem $item)
	{
		if(!auth()->user()->organization_id){
			$request->session()->flash('error', "Please select an organisation to proceed with these features.");
			return redirect()->back();
		}
		
		$organization_id = Auth::user()->organization_id;
		$getlinkeditems = SubCategories::select('sub_categories.*')
			->leftjoin('items', 'items.id', '=', 'sub_categories.category')			
			->where(function ($query) use ($organization_id) {
				if($organization_id){
					$query->where('items.organisation_id', $organization_id);
				}
			})
			->pluck('name', 'id');
			
		$getConditions = Subitem::getConditions();
		//$getlinkedsubitems = $item->getlinkedsubitems()->pluck('serialnumber', 'id')->toArray();
		
		$getlinkedsubitems = Subitem::select(DB::raw("CONCAT(subitems.make,' - ',subitems.model) as name"),'subitems.id as id')
			->where('subitems.organisation_id', '=', Auth::user()->organization_id)
			->pluck('name', 'id');
			
		$linkeditem = Linkedsubitem::where('sub_item_id', $item->id)->pluck('linked_sub_item_id')->toArray();
		
		return view('pages.subitems.edit', compact('getlinkeditems', 'getConditions', 'getlinkedsubitems', 'item', 'linkeditem'));
	}
	
	//...
	public function update(Request $request, Subitem $item)
	{
		$cond = $item->condition;
		$disposed = $item->disposed;
		$is_missed = $item->is_missed;
		
		$request->validate([
			//'serialnumber' => 'required',
            'pictureurl' => 'mimetypes:image/jpeg,image/png,image/jpg,image/webp|max:2048',
            'receipt_url' => 'mimetypes:image/jpeg,image/png,image/jpg,image/webp|max:2048',
            //'pat_date' => 'required',
            //'pat_due_date' => 'required',
            'condition' => 'required',
            'item_id' => 'required',
            'barcode_no' => 'required',
		]);
		
		$path = ('storage/photos/');
		if ($request->file('pictureurl')) {
			$image = 'picture-'. auth()->user()->id .'-'. md5(uniqid() . time()) .'.webp';
			Image::make($request->file('pictureurl'))->encode('webp', 90)->resize(400, 400)->save($path.$image);
			//image
			$item->pictureurl = $image;
		}
		
		if (@$request->file('receipt_url')) {
			$image = 'receipt-'. auth()->user()->id .'-'. md5(uniqid() . time()) .'.webp';
			Image::make($request->file('receipt_url'))->encode('webp', 90)->resize(400, 400)->save($path.$image);
			//image
			$item->receipt_url = $image;
		}
		
		if(!$item->barcode_url || $item->barcode_no!=$request->barcode_no){
			
			//$barcode = uniqueNumber();
			$barcode = $request->barcode_no;
			$item->barcode_no = $barcode;			
			$item->barcode_url = $barcode. '.png';			
			
			if(@$request->cable_length){
				$dns = new DNS1D();
				$barcodeImage = $dns->getBarcodePNG($barcode, 'C128');
				$path = public_path('assets/barcodes/' . $barcode . '.png');
				Image::make(file_get_contents('data:image/jpeg;base64, ' . $barcodeImage))->save($path);
			}
			else {
				QrCode::size(200)
					->format('png')
					->generate($barcode, public_path('assets/barcodes/' . $barcode . '.png'));
			}
		}
		
		/* if(@$request->cable_length){
			$item->make = 'Cable';
			$item->model = $request->cable_length;
		}
		else {
			$item->make = $request->make;
			$item->model = $request->model;
		} */
		
		$item->make = $request->make;
		$item->model = $request->model;
		$item->cable_length = $request->cable_length;
		$item->pat_date = $request->pat_date;
		$item->pat_due_date = $request->pat_due_date;
		$item->serialnumber = $request->serialnumber;
		$item->date_of_purchase = $request->date_of_purchase;
		$item->warranty_expiry_period = $request->warranty_expiry_period;
		$item->condition = $request->condition;
		$item->notes = $request->notes;
		$item->item_id = $request->item_id;
		$item->created_by = auth()->user()->id;
		$item->updated_by = auth()->user()->id;
		$item->organisation_id = auth()->user()->organization_id;
		
		
		
		if(@$request->disposed=='1'){
			$item->disposed = '1';
		}
		else {
			$item->disposed = '0';
		}
		
		if(@$request->is_missed=='1'){
			$item->is_missed = '1';
			$item->status = 'item_missed';
		}
		else {
			$item->is_missed = '0';
			$item->status = 'available';
		}
		
		if($request->condition=='needrepair'){
			$item->status = 'needrepair';
		}
		if($request->condition=='broken'){
			$item->status = 'broken';
		}
		
		$item->save();		
		
		if($cond != $request->condition){
			$his = new SubitemHistory;
			$his->sub_item_id = $item->id;
			$his->created_by = auth()->user()->id;
			$his->updated_by = auth()->user()->id;
			$his->notificationtext = auth()->user()->name.' updated condition from '.ucfirst($cond).' to '.ucfirst($request->condition);
			$his->save();
		}
		
		if($disposed != $request->disposed){
			$his = new SubitemHistory;
			$his->sub_item_id = $item->id;
			$his->created_by = auth()->user()->id;
			$his->updated_by = auth()->user()->id;
			$his->notificationtext = auth()->user()->name.' marked this item as disposed';
			$his->save();
		}
		
		if($is_missed != $request->is_missed){
			$his = new SubitemHistory;
			$his->sub_item_id = $item->id;
			$his->created_by = auth()->user()->id;
			$his->updated_by = auth()->user()->id;
			if($request->is_missed=='1'){
				$his->notificationtext = auth()->user()->name.' marked this item as missing';
			}
			else {
				$his->notificationtext = auth()->user()->name.' marked this item as found';
			}
			$his->save();
		}
		
		
		//...
		if(@$request->linkedsubitems){
			foreach ($request->linkedsubitems as $linkedItems) {
				$linkCheck = Linkedsubitem::where('linked_sub_item_id', $linkedItems)
					->where('sub_item_id', $item->id)->first();
				
				$linkStore = new Linkedsubitem;
				if(@$linkCheck){
					$linkStore->id = $linkCheck->id;
					$linkStore->exists = true;
				}			
				$linkStore->sub_item_id = $item->id;
				$linkStore->linked_sub_item_id = $linkedItems;
				$linkStore->created_by = auth()->user()->id;
				$linkStore->updated_by = auth()->user()->id;
				$linkStore->save();
			}
		}
		
		
		//...
		/* $quantity = $item->quantity;
		$count = SubitemBarcode::where('sub_item_id', $item->id)->count();
		
		$quantity = $quantity - $count;
		if($quantity>0){
			for($i=1; $i<=$quantity; $i++){
				$barcode = time() . $i . rand();
				
				//save barcode
				$storeBarcode = new SubitemBarcode;
				$storeBarcode->sub_item_id = $item->id;
				$storeBarcode->barcode_no = $barcode;
				$storeBarcode->barcode_url = $barcode . ".jpg";
                $storeBarcode->status = 'available';
				$storeBarcode->save();
			}
		} */
		
		$request->session()->flash('success', "Item Updated Successfully");
		return redirect()->route("subitems.show", $item->id);
	}
	
	
	
	///...
	public function export(){
	
		$filename = 'items_'.auth()->user()->id . date('Y_m_d_h_i');
		
		$headers = array(
			"Content-type" => "text/csv",
			"Content-Disposition" => "attachment; filename=".$filename.".csv",
			"Pragma" => "no-cache",
			"Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
			"Expires" => "0"
		);
		
		$columns = array('Barcode', 'Serial Number', 'Make', 'Model', 'Sub Category', 'Date of purchase', 'Warranty expiry period', 'Condition', 'Notes', 'Disposed', 'Cable Length', 'Pat Date', 'Pat Due Date', 'Is Missed', 'Status');
		
		$subitems = Subitem::where('organisation_id', '=', auth()->user()->organization_id)->get();
		
		if(auth()->user()->role=='superadmin' && auth()->user()->organization_id==''){
			$subitems = Subitem::get();
		}
		
		$callback = function() use ($subitems, $columns) {
			$file = fopen('php://output', 'w');
			fputcsv($file, $columns);
			
			foreach($subitems as $item){
				
				$datas = array();
				
				$datas['Barcode'] = @$item->barcode_no;
				$datas['Serial Number'] = @$item->serialnumber;
				$datas['Make'] = @$item->make;
				$datas['Model'] = @$item->model;
				$datas['Sub Category'] = subcategory($item->item_id);
				$datas['Date of purchase'] = @$item->date_of_purchase;
				$datas['Warranty expiry period'] = @$item->warranty_expiry_period;
				$datas['Condition'] = @$item->condition;
				$datas['Notes'] = @$item->notes;
				$datas['Disposed'] = @$item->disposed;
				$datas['Cable Length'] = @$item->cable_length;
				$datas['Pat Date'] = @$item->pat_date;
				$datas['Pat Due Date'] = @$item->pat_due_date;
				$datas['Is Missed'] = @$item->is_missed;
				$datas['Status'] = subItemStatus($item->status);			
				
				
				fputcsv($file, $datas);
			}
			fclose($file);
		};
		
		return response()->stream($callback, 200, $headers);
	}
	
	
	public function import(Request $request)
	{
		$request->validate([
			'file' => 'required|file',
		]);
		
		if ($request->file('file')) {
			
			$file = $request->file('file');
			
			$path = 'storage/uploads/csv/';
			if(!is_dir($path)) {
				mkdir($path, 0775, true);
				chown($path, exec('whoami'));
			}
			
			$filename = 'projects-'.md5(uniqid().time()).'.'.$file->getClientOriginalExtension();	
			// Upload file
			$file->move($path, $filename);
			
			// Import CSV to Database
			$filepath = ('public/uploads/csv/'.$filename);
			
			// Reading file
			$data = Storage::get($filepath);
			
			$csv = array_map(function($row) {
				$data = str_getcsv($row);
				
				//insert players
				$format = "Y-m-d";
				$path = ('storage/photos/');
				if(!is_dir($path)) {
					mkdir($path, 0775, true);
					chown($path, exec('whoami'));
				}
		
				if(@$data && $data[0] && $data[0]!='Barcode'){
					$barcode = uniqueNumber();
					
					$store = new Subitem;
					if($data[0]){
						$barcode = $data[0];
						$check = Subitem::where('barcode_no', $barcode)->first();
						if(@$check){
							$store->id = $check->id;
							$store->exists = true;
						}
						else {
							$barcode = uniqueNumber();
						}
					}
					else {
						$store->status = 'available';
					}
					
					
					$store->serialnumber = @$data[1];
					
					if(!$data[0]){
						$flag = true;
						$try = 1;
						while ($flag && $try <= 3):
							try {
								QrCode::size(200)
								->format('png')
								->generate($barcode, public_path('assets/barcodes/' . $barcode . '.png'));
								//Image migrated successfully
								$flag = false;
							} catch (\Exception $e) {
								//not throwing  error when exception occurs
							}
							$try++;
						endwhile;
					}
					
					if(@$data[4]){
						$items = SubCategories::where('name', 'like', '%'. $data[4] .'%')->first();
						//...
						if(@$items){
							$store->item_id = $items->id;
						}
					}
					
					$store->make = @$data[2];
					$store->model = @$data[3];
					
					if(@$data[5]){
						$store->date_of_purchase = @$data[5];
					}
					
					if(@$data[6]){
						$store->warranty_expiry_period = @$data[6];
					}
					
					$store->barcode_no = $barcode;
					$store->barcode_url = $barcode. '.png';
					$store->condition = @$data[7];
					$store->notes = @$data[8];
					$store->disposed = @$data[9];
					$store->created_by = auth()->user()->id;
					$store->updated_by = auth()->user()->id;
					$store->organisation_id = auth()->user()->organization_id;
					
					
					$store->cable_length = @$data[10];
					$store->pat_date = @$data[11];
					$store->pat_due_date = @$data[12];
					$store->is_missed = @$data[13];
					
					$store->quantity = '1';
					
					$store->save();
					
					//...
					$his = new SubitemHistory;
					$his->sub_item_id = $store->id;
					$his->created_by = auth()->user()->id;
					$his->updated_by = auth()->user()->id;
					$his->notificationtext = 'Created New Item';
					$his->save();
				
				}
			}, explode("\n", $data)); 
			
			$request->session()->flash('success', "Csv Uploaded");
			return redirect()->route("sub-items");
		}
		$request->session()->flash('error', "Csv File Missing");
		return redirect()->route("sub-items");
	}
	
	
	//...
	public function quickCheckout(Request $request, Subitem $item){
		if(@$item){
		
			return view('pages.subitems.quickCheckout', compact('item'));		
		}
		return redirect()->route("sub-items");
	}
	
	//...
	public function quickCheckoutStore(Request $request, Subitem $item){
		if(@$item){
			$request->validate([
				'title' =>'required',
				'user' =>'required',
				'pickup_date' =>'required|date',
				'expected_return_date' => 'required|date|after_or_equal:pickup_date',
			]);
			
			$store = new Project;
			$store->title = $request->title;
			$store->pickup_date = $request->pickup_date;
			$store->expected_return_date = $request->expected_return_date;
			$store->created_by = auth()->user()->id;
			$store->updated_by = auth()->user()->id;
			$store->organisation_id = auth()->user()->organization_id;
			$store->user = $request->user;
			$store->status = auth()->user()->role == 'user' ? '2' : '1';
			$store->project_status = 'reserved';
			$store->save();
			
			$phis = new ProjectHistory;
			$phis->project_id = $store->id;
			$phis->created_by = auth()->user()->id;
			$phis->updated_by = auth()->user()->id;
			$phis->notificationtext = 'Project Created - Quick Checkout';
			$phis->save();
			
			//...
			$perform = Project::find($store->id);
			//...
			Subitem::find($item->id)->update(['status'=>'reserved']);
			
			//..
			$perform->items()->create([
				'quantity' =>  '1',
				'item_id'  => $item->id,
				'user_id'  => auth()->user()->id,
				'project_id' => $perform->id,
				'status' => 'reserved'
			])->user()->associate(auth()->user()->id)->save();
			
			$his = new SubitemHistory;
			$his->sub_item_id = $item->id;
			$his->created_by = auth()->user()->id;
			$his->updated_by = auth()->user()->id;
			$his->notificationtext = 'Item Reserved';
			$his->save();
			
			$request->session()->flash('success', "Project Created - Quick Checkout");
			return redirect()->route("projects");
		}
		return redirect()->route("sub-items");
	}
	
	//...
	public function destroy(Request $request, Subitem $item)
	{
		$item->delete();
	
		$request->session()->flash('success', "Item Deleted");
		return redirect()->route('sub-items');
	}
	
}
