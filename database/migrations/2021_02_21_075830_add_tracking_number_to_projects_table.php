<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTrackingNumberToProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('projects', function (Blueprint $table) {
            $table->string('tracking_number')->nullable();
            $table->string('incoming_shipping_crate')->nullable();
            $table->string('outgoing_shipping_crate')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('projects', function (Blueprint $table) {
            $table->dropColumn('tracking_number');
            $table->dropColumn('incoming_shipping_crate');
            $table->dropColumn('outgoing_shipping_crate');
        });
    }
}
