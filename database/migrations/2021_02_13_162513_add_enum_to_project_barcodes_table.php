<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddEnumToProjectBarcodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('project_barcodes', function (Blueprint $table) {
			DB::statement("ALTER TABLE project_barcodes MODIFY status enum('kit_packing', 'on_location_kit_pack', 'warehouse_return') NOT NULL;");
			$table->string('tracking_number')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('project_barcodes', function (Blueprint $table) {
			DB::statement("ALTER TABLE project_barcodes MODIFY status enum('checkin', 'checkout') NOT NULL;");
			$table->dropColumn('tracking_number');
        });
    }
}
