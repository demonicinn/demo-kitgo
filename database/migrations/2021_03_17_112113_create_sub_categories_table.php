<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sub_categories', function (Blueprint $table) {
			$table->engine = 'InnoDB';
            $table->id();
			
            $table->unsignedBigInteger('category');
            $table->foreign('category')->references('id')->on('items')->onDelete('cascade');
			
			$table->string('name');
			$table->text('linked_id');
			
			$table->integer('created_by');
			$table->integer('updated_by');
			
			$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sub_categories');
    }
}
