<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTypesToSubitemHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('subitem_histories', function (Blueprint $table) {
            $table->enum('type', ['0', '1', '2'])->default('0');
            $table->string('linkto')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('subitem_histories', function (Blueprint $table) {
            $table->dropColumn('type');
            $table->dropColumn('linkto');
        });
    }
}
