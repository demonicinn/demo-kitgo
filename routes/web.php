<?php

use App\Http\Livewire\Frontpage;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\FrontController;


use App\Http\Controllers\DashboardController;
use App\Http\Controllers\UsersController;
use App\Http\Controllers\ItemsController;
use App\Http\Controllers\SubitemsController;
use App\Http\Controllers\ProjectsController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\SubCategoriesController;
/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

Route::get('/migrate-fresh', function() {
	Artisan::call('migrate:fresh');
	return "Data Cleaned";
});


Route::get('/cron', [FrontController::class, 'cron']);
#Route::get('/admin-list', [FrontController::class, 'getUser']);
#Route::get('/admin-pass', [FrontController::class, 'updatePassword']);

//'accessrole',
Route::group(['middleware' => [
        'auth:sanctum',
        'verified',
    ]], function () {

	Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard');


    Route::get('/pages', function () {
        return view('admin.pages');
    })->name('pages');

    Route::get('/navigation-menus', function () {
        return view('admin.navigation-menus');
    })->name('navigation-menus');

    Route::get('/users', function () {
        return view('admin.users');
    })->name('users');

    Route::get('/user-permissions', function () {
        return view('admin.user-permissions');
    })->name('user-permissions');

    

    Route::get('/projects', function () {
        return view('manager.projects');
    })->name('projects');

    

});



Route::group(['middleware' => [
        'auth',
        'verified'
    ]], function () {
	
	Route::get('/organizations', function () {
        return view('admin.organizations');
    })->name('organizations');
	
	
	
	//sub-categories
	Route::group(['prefix' => 'sub-categories'], function(){
		Route::get('/', [SubCategoriesController::class, 'index'])->name('subCategories');
		Route::get('/create', [SubCategoriesController::class, 'create'])->name('subCategories.create');
		Route::post('/store', [SubCategoriesController::class, 'store'])->name('subCategories.store');
		Route::get('/{category}/edit', [SubCategoriesController::class, 'edit'])->name('subCategories.edit');
		Route::patch('/{category}/update', [SubCategoriesController::class, 'update'])->name('subCategories.update');
		Route::post('/{category}/destroy', [SubCategoriesController::class, 'destroy'])->name('subCategories.destroy');
	});
	
	
	
	
	Route::post('/set-organization', [ProfileController::class, 'setOrganization'])->name('admin.organization');
	
	//profile
	Route::group(['prefix' => 'profile'], function(){
		//profile page
		Route::get('/', [ProfileController::class, 'index'])->name('profile');
		Route::post('/', [ProfileController::class, 'update'])->name('profile.update');

		//change-password
		Route::get('/password', [ProfileController::class, 'password'])->name('profile.password');
		Route::post('/password', [ProfileController::class, 'passwordUpdate'])->name('profile.password.update');
	});
	
	//sub-items
	Route::group(['prefix' => 'items'], function(){
		Route::get('/', function () {
			return view('manager.sub-items');
		})->name('sub-items');
		//...
		Route::get('/{item}/quick-checkout', [SubitemsController::class, 'quickCheckout'])->name('subitems.quickCheckout');
		Route::post('/{item}/quick-checkout', [SubitemsController::class, 'quickCheckoutStore'])->name('subitems.quickCheckout');
		Route::get('/create', [SubitemsController::class, 'create'])->name('subitems.create');
		Route::post('/store', [SubitemsController::class, 'store'])->name('subitems.store');	
		Route::get('/export', [SubitemsController::class, 'export'])->name('subitems.export');
		Route::get('/import/sample', [SubitemsController::class, 'importSample'])->name('subitems.importSample');
		Route::post('/import', [SubitemsController::class, 'import'])->name('subitems.import');
		Route::get('/{item}/edit', [SubitemsController::class, 'edit'])->name('subitems.edit');
		Route::patch('/{item}/update', [SubitemsController::class, 'update'])->name('subitems.update');	
		Route::post('/{item}/destroy', [SubitemsController::class, 'destroy'])->name('subitems.destroy');	
		Route::get('/{item}', [SubitemsController::class, 'show'])->name('subitems.show');
	});	
	
	//users
	Route::group(['prefix' => 'users'], function(){
		Route::get('/create', [UsersController::class, 'create'])->name('users.create');
		Route::post('/store', [UsersController::class, 'store'])->name('users.store');
		Route::get('/{user}/edit', [UsersController::class, 'edit'])->name('users.edit');
		Route::patch('/{user}/update', [UsersController::class, 'update'])->name('users.update');
		Route::post('/{user}/destroy', [UsersController::class, 'destroy'])->name('users.destroy');
		Route::get('/{user}', [UsersController::class, 'show'])->name('users.show');
	});	
	
	//items
	Route::group(['prefix' => 'categories'], function(){
		Route::get('/', function () {
			return view('manager.items');
		})->name('items');
		//...
		Route::get('/create', [ItemsController::class, 'create'])->name('categories.create');
		Route::post('/store', [ItemsController::class, 'store'])->name('categories.store');
		Route::get('/{item}/edit', [ItemsController::class, 'edit'])->name('categories.edit');
		Route::patch('/{item}/update', [ItemsController::class, 'update'])->name('categories.update');
	});
	
	//projects
	Route::group(['prefix' => 'projects'], function(){
		Route::post('/subitems', [ProjectsController::class, 'subitems'])->name('projects.subitems');
		Route::post('/subitemsLinked', [ProjectsController::class, 'subitemsLinked'])->name('projects.subitemsLinked');
		Route::post('/subitemsLinked/delete', [ProjectsController::class, 'subitemsLinkedDelete'])->name('projects.subitemsLinked.delete');
		
		Route::get('/create', [ProjectsController::class, 'create'])->name('projects.create');
		Route::post('/store', [ProjectsController::class, 'store'])->name('projects.store');
		Route::get('/export', [ProjectsController::class, 'export'])->name('projects.export');
		Route::get('/import/sample', [ProjectsController::class, 'importSample'])->name('projects.importSample');
		Route::post('/import', [ProjectsController::class, 'import'])->name('projects.import');
		
		Route::get('/{project}/edit', [ProjectsController::class, 'edit'])->name('projects.edit');
		Route::patch('/{project}/update', [ProjectsController::class, 'update'])->name('projects.update');
		Route::get('/{project}', [ProjectsController::class, 'show'])->name('projects.show');
		Route::get('/{project}/checkout', [ProjectsController::class, 'checkout'])->name('projects.checkout');
		Route::post('/{project}/destroy', [ProjectsController::class, 'destroy'])->name('projects.destroy');
		Route::get('/{project}/status/{status}', [ProjectsController::class, 'projectStatus'])->name('projects.projectStatus');
	});
	
});	

Route::get('/{urlslug}', Frontpage::class);
Route::get('/', function() {
    return redirect()->route('login');
});
